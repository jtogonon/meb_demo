<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'travel_agents';

    public static function getClientAgents($operator_id)
    {
        $travel_agents = Agent::select('name', 'agent_id')->where('operator_id', $operator_id)->get();

        $agentsArr = [];
        foreach ($travel_agents as $agent) {
            $agentsArr[$agent->agent_id] = $agent->name;
        }

        return $agentsArr;
    }
}