<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bank_account';

    /**
     * Fillable fields for model.
     *
     * @var array
     */
    protected $fillable = [
        'subscription_id',
        'routing_number',
        'account_number',
        'address1',
        'address2',
        'city',
        'state',
        'zip',
        'country',
        'phone_number'
    ];

    public static $rules = [
        'routing_number' => 'required|numeric',
        'account_number' => 'required|numeric',
        'city' => 'required|regex:/^[a-z][a-z ]*$/i',
        'state' => 'required|regex:/^[a-z][a-z ]*$/i',
        'zip' => 'required|numeric',
        'country' => 'required|regex:/^[a-z][a-z ]*$/i',
        'phone_number' => 'required'
    ];
}