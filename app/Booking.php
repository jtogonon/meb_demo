<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Booking extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bookings';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'status',
        'start_date',
        'end_date',
        'date',
        'adults_number',
        'children_number',
        'infants_number',
        'code',
        'type',
        'event_order',
        'purpose_id',
        'user_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'start_date',
        'end_date',
        'date',
    ];

    /**
     * The subtotal cost of booking.
     *
     * @return float|int
     */
    public function subtotal()
    {
        $subtotal = 0;
        foreach ($this->items as $item) {
            $itemTotal = $item->totalPrice();
            $subtotal += $itemTotal;
        }

        return round($subtotal, 2);
    }

    /**
     * The taxes of booking.
     *
     * @return float
     */
    public function taxes()
    {
        return 4.6;
    }

    /**
     * The commission of booking.
     *
     * @return int
     */
    public function commission()
    {
        return 2;
    }

    /**
     * The total cost of booking.
     */
    public function totalCost()
    {
        $commission = $this->commission();
        $subtotal = $this->subtotal();
        $taxes = $this->taxes();

        return round($subtotal + ($subtotal * ($taxes + $commission)) / 100, 2);
    }

    /**
 * Arriving date in human friendly format.
 *
 * @return bool|string
 */
    public function arriving()
    {
        if ($this->start_date) {
            $arriving = date('l, d F Y', strtotime($this->start_date));
        } else {
            $arriving = '';
        }

        return $arriving;
    }

    /**
     * Departing date in human friendly format.
     *
     * @return bool|string
     */
    public function departing()
    {
        if ($this->end_date) {
            $departing = date('l, d F Y', strtotime($this->end_date));
        } else {
            $departing = '';
        }

        return $departing;
    }

    /**
     * Get Bookings lead traveler.
     *
     * @return mixed
     */
    public function leadTraveler()
    {
        return $this->passengers()->where(['lead_traveler' => 1])->first();
    }

    /**
     * Get Lead travelers name.
     *
     * @return string
     */
    public function leadTravelerName()
    {
        $leadTraveler = $this->leadTraveler();

        return $leadTraveler->first_name . ' ' . $leadTraveler->last_name;
    }

    /**
     * Number of travelers.
     *
     * @return int|mixed
     */
    public function peopleNumber()
    {
        if($this->type_id == 'fit'){
            return 1;
        }

        return $this->adults_number + $this->children_number + $this->imfants_number;
    }

    /**
     * Set the start_date attribute.
     *
     * @param $date
     */
    public function setStartDateAttribute($date)
    {
        $this->attributes['start_date'] = Carbon::parse($date);
    }

    /**
     * Set the end_date attribute.
     *
     * @param $date
     */
    public function setEndDateAttribute($date)
    {
        $this->attributes['end_date'] = Carbon::parse($date);
    }

    /**
     * Set the date attribute.
     *
     * @param $date
     */
    public function setDateAttribute($date)
    {
        $this->attributes['date'] = Carbon::parse($date);
    }

    /**
     * Get the start_date attribute.
     *
     * @param $date
     * @return Carbon
     */
    public function getStartDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    /**
     * Get the end_date attribute.
     *
     * @param $date
     * @return Carbon
     */
    public function getEndDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    /**
     * Get the date attribute.
     *
     * @param $date
     * @return Carbon
     */
    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    /**
     * Booking belongs to a User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * One booking has many passengers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function passengers()
    {
        return $this->belongsToMany(Passenger::class);
    }

    /**
     * Booking items.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(BookingItem::class);
    }

    /**
     * Get previous booking_id and next booking_id.
     *
     * @return array
     */
    public function getPreviousAndNextBookingId()
    {
        $previous = 0;
        $next = 0;

        $previousBooking = self::where('id', '<', $this->id)
            ->where(['user_id' => Auth::user()->id])
            ->orderBy('id', 'desc')
            ->first();

        $nextBooking = self::where('id', '>', $this->id)
            ->where(['user_id' => Auth::user()->id])
            ->orderBy('id', 'asc')
            ->first();

        if(isset($previousBooking)){
            $previous = $previousBooking->id;
        }

        if(isset($nextBooking)){
            $next = $nextBooking->id;
        }

        return [
            'previous' => $previous,
            'next' => $next,
        ];
    }
}