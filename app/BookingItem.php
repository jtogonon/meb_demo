<?php namespace App;

use App\Repositories\HTMLtoOpenXML\HTMLtoOpenXML;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BookingItem extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'booking_items';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'product_code',
        'website',
        'zip',
        'adults_number',
        'children_number',
        'infants_number',
        'start_date',
        'end_date',
        'start_time',
        'notes',
        'end_time',
        'itinerary_id',
        'vacation_id',
        'category_id',
        'type_id',
        'type_id',
        'country_id',
        'price_id',
        'booking_id',
        'product_id',
        'user_id'
    ];

//    /**
//     * The attributes that should be mutated to dates.
//     *
//     * @var array
//     */
//    protected $dates = [
//        'start_date',
//        'end_date',
//        'start_time',
//        'end_time',
//    ];

    /**
     * Create booking item fro product info.
     *
     * @param Product $product
     * @param Booking $booking
     * @param int $vacation_id
     * @return bool|static
     */
    public static function createFromProduct(Product $product, Booking $booking, $vacation_id = null)
    {
        $bookingItem = self::where(['booking_id' => $booking->id, 'product_id' => $product->id])->get();

        if(!empty($bookingItem->all())){
            return false; //avoid duplication
        }

        $data = [
            'name' => $product->name,
            'description' => $product->description,
            'product_code' => $product->code,
            'website' => $product->website,
            'zip' => $product->zip,
            'adults_number' => $booking->adults_number,
            'children_number' => 0,
            'infants_number' => 0,
            'start_date' => Carbon::now(),
            'end_date' => Carbon::now()->addDay(),
            'start_time' => Carbon::now()->setTime(0,0,0),
            'end_time' => Carbon::now()->setTime(0,0,0)->addHours(2),
            'category_id' => $product->category_id,
            'type_id' => $product->type_id,
            'country_id' => $product->country_id,
            'booking_id' => $booking->id,
            'product_id' => $product->id,
            'vacation_id' => $vacation_id,
            'user_id' => Auth::user()->id
        ];

        $itemPrice = $product->price->replicate();
        $itemPrice->save();

        $itemItinerary = $product->itinerary->replicate();
        $itemItinerary->save();

        if (isset($booking->children_number)) {
            $data['children_number'] = $booking->children_number;
        }
        if (isset($booking->infants_number)) {
            $data['infants_number'] = $booking->infants_number;
        }
        $data['price_id'] = $itemPrice->id;
        $data['itinerary_id'] = $itemItinerary->id;

        return self::create($data);
    }

    /************************************OLD****************************************************/


    /**
     * Number of days of product.
     *
     * @return bool|float|int
     */
    public function quantity()
    {
        $days = get_days_between_dates($this->start_date, $this->end_date);

        return ($days > 0) ? $days : false;
    }

    /**
     * Total price of product.
     *
     * @return bool|float|int
     */
    public function totalPrice()
    {
        $quantity = $this->quantity();
        $perDay = $this->price->total_price;
        $price = $quantity * $perDay;
        $price = round($price, 2);

        return $price;
    }

    /**
     * Get Item section.
     *
     * @param int $section
     * @return mixed
     */
    public function getItemSection($section = 1)
    {
        return ProductItinerarySection::where([
            'section' => $section,
            'operator_id' => Auth::id(),
            'product_id' => $this->product->id
        ])->first();
    }

    /**
     * Get content from section.
     *
     * @param int $section
     * @return mixed
     */
    public function getSectionContent($section = 1)
    {
        if (empty($this->getItemSection($section))) {
            return '';
        }

        return $this->getItemSection($section)->content;
    }

    /**
     * Clean WYSIWYG string.
     *
     * @param $section_content
     * @return \App\Repositories\HTMLtoOpenXML\The|string
     */
    public function cleanRenderSection($section_content)
    {
        return HTMLtoOpenXML::getInstance()->fromHTML($section_content);
    }

    /**
     * To avoid duplicate items in a booking
     * single instance of a product only.
     *
     * @param $product_id
     * @param $booking_id
     * @return mixed
     */
    public static function checkProductAndBookingId($product_id, $booking_id)
    {
        $products = self::where('booking_id', $booking_id)
            ->where('product_id', $product_id)->get();

        return $products;
    }


    /**************************************************************/

    /**
     * Booking Item belongs to Product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Product belongs to Country.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Product belongs to Type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(ProductType::class);
    }

    /**
     * Product has one Itinerary.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function itinerary()
    {
        return $this->belongsTo(ItinerarySection::class);
    }

    /**
     * Product has one Price.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function price()
    {
        return $this->belongsTo(ProductFee::class);
    }

    /**
     * Product belongs to Category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }

    /**
     * Booking Item belongs to Booking.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }
}