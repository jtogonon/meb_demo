<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingItemDetail extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'booking_item_details';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Item start date.
     *
     * @return mixed|null
     */
    public function startDate()
    {
        switch ($this->type) {
            case 'accomodation':
                $startDate = $this->acco_checkin;
                break;
            case 'vacation':
                $startDate = $this->vac_start;
                break;
            case 'activity':
                $startDate = $this->act_date;
                break;
            case 'carrental':
                $startDate = $this->carrent_date_pickup;
                break;
            case 'flight':
                $startDate = $this->flight_from_date;
                break;
            case 'cruise':
                $startDate = $this->acco_checkin;
                break;
            default:
                $startDate = null;
        }

        return $startDate;
    }

    /**
     * Item end date.
     *
     * @return mixed|null
     */
    public function endDate()
    {
        switch ($this->type) {
            case 'accomodation':
                $endDate = $this->acco_checkout;
                break;
            case 'vacation':
                $endDate = $this->vac_end;
                break;
            case 'activity':
                $endDate = $this->act_date;
                break;
            case 'carrental':
                $endDate = $this->carrent_date_return;
                break;
            case 'flight':
                $endDate = $this->flight_to_date;
                break;
            case 'cruise':
                $endDate = $this->acco_checkout;
                break;
            default:
                $endDate = null;
        }

        return $endDate;
    }

    /**
     * Get items of a booking in details view
     *
     * @param $booking_id
     * @return mixed
     */
    public static function getItemsByBookingId($booking_id)
    {
        $products = self::where('booking_id', $booking_id)->get();

        return $products;
    }

    public static function checkBookingIdAndProductId($booking_id, $product_id, $booking_item_id)
    {
        $item = self::where('booking_id', $booking_id)
            ->where('booking_item_id', $booking_item_id)
            ->get();
        //should return only 1 row
        if (count($item) == 0) {
            return false;
        }

        return $item;
    }
}