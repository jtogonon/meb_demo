<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingTmp extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bookings_tmp';

    protected $primaryKey = 'id';

    public static function getProducts($session_id)
    {
        $products = self::where('session_id', $session_id)->get();

        return $products;
    }
}