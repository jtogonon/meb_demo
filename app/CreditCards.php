<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditCards extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'credit_cards';

    /**
     * Fillable fields for model.
     *
     * @var array
     */
    protected $fillable = [
        'subscription_id',
        'name_on_card',
        'card_number',
        'expiration',
        'card_code',
        'address1',
        'address2',
        'city',
        'state',
        'zip',
        'country',
        'phone_number'
    ];

    public static $rules = [
        'name_on_card' => 'required|regex:/^[a-z][a-z ]*$/i',
        'card_number' => 'required|numeric|min:16',
        'expiration' => 'required|date_format:m/Y',
        'card_code' => 'required',
        'city' => 'required|regex:/^[a-z][a-z ]*$/i',
        'state' => 'required|regex:/^[a-z][a-z ]*$/i',
        'zip' => 'required|numeric',
        'country' => 'required|regex:/^[a-z][a-z ]*$/i',
        'phone_number' => 'required'
    ];
}