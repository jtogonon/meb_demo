<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cruise extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cruises';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cruiseline',
        'supplier_id',
        'product_id',
    ];

    /**
     * Cruise belongs to Product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
