<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomField extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'custom_fields';

    public static function getFieldsByModule($user_id, $module, $tab, $window) //@TODO refactor this @Jakob
    {
        $fields = self::select('*')
            ->where('user_id', $user_id)
            ->where('module', $module)
            ->where('tab', $tab)
            ->orWhere('popup_window', $window)
            ->orderBy('name', 'ASC')
            ->get();

        return $fields;
    }

    /**
     * Custom field belongs to user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}