<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerReview extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customer_reviews';

    /**
     * Fillable fields for model.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company',
        'website',
        'rating',
        'comment',
        'created_at',
        'updated_at'
    ];
}