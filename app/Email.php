<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'emails';

    public static function getEmailsByOperatorId($operator_id)
    {
        $emails = self::select('*')
            ->where('operator_id', $operator_id)
            ->orderBy('created', 'DESC')
            ->get();

        return $emails;
    }

}