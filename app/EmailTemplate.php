<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'email_templates';

    public static function getEmailTemplatesByOperator($user_id)
    {
        $templates = self::where('user_id', $user_id)->get();

        return $templates;
    }
}