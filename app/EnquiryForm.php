<?php namespace App;

//use Zizaco\Entrust\EntrustRole;
use Illuminate\Database\Eloquent\Model;

class EnquiryForm extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'enquiries_form';

    /**
     * Fillable fields for model.
     *
     * @var array
     */
    protected $fillable = [
        'operator_id',
        'welcome_note',
        'name',
        'phone_number',
        'email',
        'address1',
        'address2',
        'city',
        'state',
        'country',
        'zip_code',
        'custom_field'
    ];
}