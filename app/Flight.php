<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'flights';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'airline',
        'arriving',
        'departing',
        'from',
        'to',
        'supplier_id',
        'product_id',
    ];

    /**
     * Flight belongs to Product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
