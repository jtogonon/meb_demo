<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hotels';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'room_category',
        'resort_fee',
        'supplier_id',
        'product_id',
    ];

    public function operator_id()
    {
        //@TODO get suppliers operator id @Jakob
    }

    /**
     * Hotel belongs to Product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
