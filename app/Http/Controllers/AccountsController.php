<?php namespace App\Http\Controllers;

use App\CreditCards;
use App\Bank;
use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

use Auth;
use Entrust;
use PayPal\Api\RedirectUrls;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Response, Asset, Html;
use App\OperatorSetting;
use Exception;
use App\Subscription;
use Illuminate\Support\Facades\Validator;
use PayPal\Api\Amount;
use PayPal\Api\CreditCard;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use PayPal\Common\ResultPrinter;
use PayPal\Api\BankAccount;

class AccountsController extends BaseController
{
    public $layout = 'layout';

    public $page_title = 'Account';

    public function __construct(Guard $auth)
    {
        //$this->middleware('guest');
        $this->middleware('auth');
        $this->auth = $auth;

        parent::__construct();

        $jsArray = [
            '/assets/jquery/dist/jquery.min.js',
            '/assets/jquery-ui/jquery-ui.min.js',
            '/assets/slimScroll/jquery.slimscroll.min.js',
            '/assets/bootstrap/dist/js/bootstrap.min.js',
            '/assets/metisMenu/dist/metisMenu.min.js',
            '/assets/iCheck/icheck.min.js',
            '/assets/peity/jquery.peity.min.js',
            '/assets/sweetalert/lib/sweet-alert.min.js',
            '/assets/sparkline/index.js',
            '/scripts/homer.js',

        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/fontawesome/css/font-awesome.css',
            '/assets/metisMenu/dist/metisMenu.css',
            '/assets/animate.css/animate.css',
            '/assets/bootstrap/dist/css/bootstrap.css',
            '/assets/sweetalert/lib/sweet-alert.css',
        ];
        Asset::add($cssArray, 'headerCss');
    }

    public function index()
    {
        $jsArray = [
            '/assets/datatables/media/js/jquery.dataTables.min.js',
            '/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',
            '/scripts/accounts.js',
            '/assets/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js'
        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css',
            '/assets/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css'
        ];
        Asset::add($cssArray, 'headerCss');

        //start here
        $data = [
            'page_title' => $this->page_title,
            'title_TopPanel' => 'Account',
            'TopPanel_submenu' => [
                'Administrator' => '/administrator/index',
                'Account Settings' => '/accounts/index'
            ]
        ];
        $data['date_format'] = OperatorSetting::$date_format;
        $data['currency_format'] = OperatorSetting::$currency_format;
        $data['timezone'] = OperatorSetting::$timezone;

        return view('accounts.index', $data);
    }

    public function update(Request $request)
    {
        $date_format = $request->input('date_format');
        $currency_format = $request->input('currency_format');
        $timezone = $request->input('timezone');
        $auth = $this->auth->user();

        $data['title'] = 'Saved';
        $data['success'] = 1;
        $data['text'] = 'Data has been saved';
        $data['type'] = 'success';

        if (!in_array($date_format, OperatorSetting::$date_format)) {
            $data['title'] = 'Error';
            $data['success'] = 0;
            $data['text'] = 'Please select valid date format';
            $data['type'] = 'warning';
        }

        if (!array_key_exists($currency_format, OperatorSetting::$currency_format)) {
            $data['title'] = 'Error';
            $data['success'] = 0;
            $data['text'] = 'Please select valid currency format';
            $data['type'] = 'warning';
        }

        if (!array_key_exists($timezone, OperatorSetting::$timezone)) {
            $data['title'] = 'Error';
            $data['success'] = 0;
            $data['text'] = 'Please select valid timezone';
            $data['type'] = 'warning';
        }

        $messages = [
            'legal_name' => 'The legal name must contain letters and space only.',
            'business_as' => 'The business as must contain letters and space only.',
            'contact_person' => 'The contact person as must contain letters and space only.',
        ];

        $validatorSub = Validator::make($request->all(), Subscription::$rules);

        if ($validatorSub->fails()) {
            $data['title'] = 'Error';
            $data['success'] = 0;
            $data['text'] = $validatorSub->errors()->all();;
            $data['type'] = 'warning';
        }

        $validator = Validator::make($request->all(), OperatorSetting::$rules, $messages);

        if ($validator->fails()) {
            $data['title'] = 'Error';
            $data['success'] = 0;
            $data['text'] = $validator->errors()->all();;
            $data['type'] = 'warning';
        }

        $insert = [];

        foreach ($request->all() as $key => $value) {
            $auth = $this->auth->user();
            $insert['operator_id'] = $auth->operator_id;
            $insert[$key] = $value;

        };

        if (!$validatorSub->fails()) {
            $subscription = Subscription::create($insert);
            if ($request->session()->has('card_number')) {
                CreditCards::create([
                    'subscription_id' => $subscription->id,
                    'name_on_card' => $request->session()->get('name_on_card'),
                    'card_number' => $request->session()->get('card_number'),
                    'expiration' => $request->session()->get('expiration'),
                    'card_code' => $request->session()->get('card_code'),
                    'address1' => $request->session()->has('address1') ? $request->session()->get('address1') : null,
                    'address2' => $request->session()->has('address2') ? $request->session()->get('address2') : null,
                    'city' => $request->session()->get('city'),
                    'state' => $request->session()->get('state'),
                    'zip' => $request->session()->get('zip'),
                    'country' => $request->session()->get('country'),
                    'phone_number' => $request->session()->get('phone_number'),
                ]);
            }

            if ($request->session()->has('routing_number')) {
                Bank::create([
                    'subscription_id' => $subscription->id,
                    'routing_number' => $request->session()->get('routing_number'),
                    'account_number' => $request->session()->get('account_number'),
                    'address1' => $request->session()->has('address1') ? $request->session()->get('address1') : null,
                    'address2' => $request->session()->has('address2') ? $request->session()->get('address2') : null,
                    'city' => $request->session()->get('city'),
                    'state' => $request->session()->get('state'),
                    'zip' => $request->session()->get('zip'),
                    'country' => $request->session()->get('country'),
                    'phone_number' => $request->session()->get('phone_number'),
                ]);
            }
        }

        if (!$validator->fails()) {
            OperatorSetting::where('operator_id', $auth->operator_id)->update([
                'business_add' => $request->get('business_add'),
                'business_as' => $request->get('business_as'),
                'company_email' => $request->get('company_email'),
                'paypal_email' => $request->get('paypal_email'),
                'company_phone' => $request->get('company_phone'),
                'contact_person' => $request->get('contact_person'),
                'legal_name' => $request->get('legal_name'),
                'date_format' => $date_format,
                'currency_format' => $currency_format,
                'timezone' => $timezone
            ]);
        }

        if (!$validatorSub->fails() && !$validator->fails() && $request->session()->has('card_number')) {

            $expiration = explode('/', $request->session()->get('expiration'));
            $name = $request->session()->get('name_on_card');
            $expName = explode(' ', $name);
            $firstname = '';
            $lastname = '';
            foreach ($expName as $key => $exp) {
                if ($key == count($expName) - 1) {
                    $lastname = $exp;
                } else {
                    $firstname .= $exp . ' ';
                }

            }

            $card = new CreditCard();
            $card->setType("visa");
            $card->setNumber($request->session()->get('card_number'));
            $card->setExpire_month($expiration[0]);
            $card->setExpire_year($expiration[1]);
            $card->setFirst_name($firstname);
            $card->setLast_name($lastname);

            $fi = new FundingInstrument();
            $fi->setCredit_card($card);

            $payer = new Payer();
            $payer->setPayment_method("credit_card");
            $payer->setFunding_instruments([$fi]);

            /*$item1 = new Item();
            $item1->setName('Ground Coffee 40 oz');
            $item1->setCurrency('USD');
            $item1->setQuantity(1);
            $item1->setPrice(4);

            $item2 = new Item();
            $item2->setName('Granola bars');
            $item2->setCurrency('USD');
            $item2->setQuantity(4);
            $item2->setPrice(4);

            $itemList = new ItemList();
            $itemList->setItems([$item1, $item2]);*/

            $amount = new Amount();
            $amount->setCurrency("USD");
            $amount->setTotal(20);

            $transaction = new Transaction();
            $transaction->setAmount($amount);
            //$transaction->setItem_list($itemList);
            $transaction->setDescription("Payment description");

            $payment = new Payment();
            $payment->setIntent("sale");
            $payment->setPayer($payer);
            $payment->setTransactions([$transaction]);

            //credential from paypal
            $authToken = new OAuthTokenCredential(
                'Ac_K-jScDFgC3lz8saxH8Lf2T54MzdihY-rEO7XwwvsZkZy6teuKv3IbVZZdHRd-XfWTTPbMAi1hP7FL',
                'EOnPxpx_iInfeDUiHLIZFf45M4zHjq1pxBfRxaQB2ZGvSjdW8GcsfReAaXmkvKzLlv4J_3Sh96jMbH95'
            );

            $apiContext = new ApiContext($authToken);
            try {
                $payment->create($apiContext);
            } catch (\PPConnectionException $e) {
                return Response::json($e->getData());
            }
            $data['payment'] = $payment->getId();

            $forgetKeys = ['name_on_card', 'card_number', 'expiration', 'card_code', 'city', 'state', 'zip', 'country', 'phone_number'
                , 'routing_number', 'account_number'];

            foreach ($forgetKeys as $forget) {
                $request->session()->forget($forget);
            }

            $request->session()->has('address1') ? $request->session()->forget('address1') : '';
            $request->session()->has('address2') ? $request->session()->forget('address2') : '';
        }

        if (!$validatorSub->fails() && !$validator->fails() && $request->session()->has('routing_number')) {
            $bankAccount = new BankAccount();
            $bankAccount->setAccountNumber("4417119669820331")
                ->setAccountNumberType("IBAN")
                ->setAccountType("SAVINGS")
                ->setAccountName("Ramraj")
                ->setCheckType("PERSONAL")
                ->setAuthType("WEB")
                ->setBankName("CITI")
                ->setCountryCode("US")
                ->setFirstName("Ramraj")
                ->setLastName("K")
                ->setBirthDate("1987-08-13")
                ->setExternalCustomerId(uniqid());

            $billingAddress = new \PayPal\Api\Address();
            $billingAddress->setLine1("52 N Main St");
            $billingAddress->setCity("Johnstown");
            $billingAddress->setState("OH");
            $billingAddress->setCountry_code("US");
            $billingAddress->setPostal_code("43210");
            $billingAddress->setPhone("408-334-8890");

            $bankAccount->setBillingAddress($billingAddress);

            try {
                $bankAccount->create($apiContext);
            } catch (\PPConnectionException $e) {
                return Response::json($e->getData());
            }
        }

        return Response::json($data);
    }

    public function addCard()
    {
        return view('accounts/_add_card');
    }

    public function addBankAccount()
    {
        return view('accounts/_add_bank_account');
    }

    public function choosePlan()
    {
        $testMode = true;

        $data = [
            'page_title' => $this->page_title,
            'title_TopPanel' => 'Account',
            'TopPanel_submenu' => [
                'Administrator' => '/administrator/index',
                'Account Settings' => '/accounts/index'
            ],
        ];

        if ($testMode):
            $data['paypalUrl'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
        else:
            $data['paypalUrl'] = 'https://www.paypal.com/cgi-bin/webscr';
        endif;

        $data['mebPaypalEmail'] = 'jetogonon@yahoo.com';


        return view('accounts/chooseplan', $data);
    }
}