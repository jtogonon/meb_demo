<?php

namespace App\Http\Controllers\Booking;

use App\User;
use App\Booking;
use App\Product;
use App\Country;
use App\Supplier;
use App\Passenger;
use App\BookingTmp;
use App\BookingItem;
use App\TripPurpose;
use App\ItineraryTemplate;

use App\Http\Requests;
use App\Http\Requests\BookingRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class BookingController extends Controller
{
    /**
     * Display a listing of the bookings.
     *
     * @return Response
     */
    public function index()
    {
        $bookings = Auth::user()->bookings;

        return view('booking.index', compact('bookings'));
    }

    /**
     * Show the form for creating a new booking.
     *
     * @return Response
     */
    public function create()
    {
        $lead_travelers = Passenger::leadTravelers();

        $lead_travelers_drop_down = ['' => '-Select-'];
        foreach ($lead_travelers as $lead_traveler) {
            $lead_travelers_drop_down[$lead_traveler->id] = $lead_traveler->first_name . ' ' . $lead_traveler->last_name;
        }

        $suppliers = Auth::user()->suppliers;

        $suppliers_drop_down = ['' => '-select-'];
        foreach ($suppliers as $supplier) {
            $suppliers_drop_down[$supplier->id] = $supplier->name;
        }

        //generate new session id
        if (!Session::getId()) {
            Session::regenerate();
        }

        $countries = Country::pluck('name', 'id'); //@TODO make helper for this @Jakob
        $trip_purposes = TripPurpose::pluck('purpose', 'id');
        $booking_items = [];
        $suppliers = $suppliers_drop_down;
        $lead_travelers = $lead_travelers_drop_down;

        return view('booking.create', compact(
            'countries',
            'lead_travelers',
            'suppliers',
            'trip_purposes',
            'booking_items'
        ));
    }

    /**
     * Store a newly created booking in storage.
     *
     * @param BookingRequest $request
     * @return Response
     */
    public function store(BookingRequest $request)
    {
        if ($request->input('booking_id')) {
            //edit booking
            $booking = Booking::find($request->input('id'));
            $booking->type = $request->input('type');
            $booking->code = $request->input('code');
        } else {
            $booking = $this->createBooking($request);
        }

        return Response::json(['data' => ['html' => 'saved', 'redirecturl' => '/booking/' . $booking->id]]);
    }

    /**
     * Display the specified booking.
     *
     * @param Booking $booking
     * @return Response
     * @internal param int $id
     *
     */
    public function show(Booking $booking)
    {
        $suppliers = Auth::user()->suppliers;

        $suppliers_drop_down = ['' => '-select-'];
        foreach ($suppliers as $supplier) {
            $suppliers_drop_down[$supplier->id] = $supplier->name;
        }

        $lead_travelers = Passenger::leadTravelers();

        $lead_travelers_drop_down = ['' => '-Select-'];
        foreach ($lead_travelers as $lead_traveler) {
            $lead_travelers_drop_down[$lead_traveler->id] = $lead_traveler->first_name . ' ' . $lead_traveler->last_name;
        }

        $previousAndNext = $booking->getPreviousAndNextBookingId();

        $next = $previousAndNext['next'];
        $previous = $previousAndNext['previous'];
        $suppliers = $suppliers_drop_down;
        $lead_travelers = $lead_travelers_drop_down;
        $trip_purposes = TripPurpose::pluck('purpose', 'id');
        $products = Auth::user()->products;
        $booking_items = $booking->items;
        $countries = Country::pluck('name', 'id'); //@TODO make helper for this @Jakob
        $itinerary_templates = [
            'template 1' => 1,
            'template 2' => 2,
            'template 3' => 3,
            'template 4' => 4
        ]; //@TODO take form Itinerary Model @Jakob

        return view('booking.show', compact(
            'booking',
            'suppliers',
            'lead_travelers',
            'trip_purposes',
            'products',
            'booking_items',
            'countries',
            'itinerary_templates',
            'next',
            'previous'
        ));
    }

    /**
     * Show the form for editing the specified booking.
     *
     * @param Booking $booking
     * @return Response
     * @internal param int $id
     *
     */
    public function edit(Booking $booking)
    {
        return view('booking.edit', compact('booking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Booking $booking
     * @param BookingRequest $request
     * @return Response
     * @internal param int $id
     *
     */
    public function update(Booking $booking, BookingRequest $request)
    {
        $booking->update($request->all());

        Session::flash('flash_message', 'Booking updated!');

        return redirect('booking');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Booking::destroy($id);

        Session::flash('flash_message', 'Booking deleted!');

        return redirect('booking');
    }

    /**
     * AJAX request: which loads list of recent bookings.
     *
     * @return mixed
     */
    public function getLatestBookings()
    {//@TODO find out why we need this and merge with getLatestBookingsSearch @Jakob
        $data['bookings'] = Auth::user()->bookings;

        $html = view('booking._latest_bookings')->with($data)->render();

        return Response::json(['data' => ['html' => $html]]);
    }

    /**
     * AJAX request: which loads list of recent bookings in the search tab.
     *
     * @return mixed
     */
    public function getLatestBookingsSearch()
    {
        $data['bookings'] = Auth::user()->bookings;

        $html = view('booking._latest_bookings_search_tab')->with($data)->render();

        return Response::json(['data' => ['html' => $html]]);
    }

    /**
     * AJAX request for products search tab in bookings/new page.
     *
     * @param null $category_id
     * @param $detailsView
     * @return mixed
     */
    public function searchProducts($category_id = null, $detailsView)
    {
        // @TODO delete this @Jakob
        if ($category_id == 0) {
            $data['products'] = Auth::user()->products;

        }else{
            $data['products'] = Product::getProductsByCategory($category_id);
        }

        $data['detailsView'] = $detailsView;

        $html = view('booking.search_products')->with($data)->render();

        return Response::json(['data' => ['html' => $html, 'redirecturl' => '/']]);
    }

    /**
     * AJAX request for adding products to booking.
     *
     * @param $product_id
     * @return mixed
     * @internal param null $urlDetails_view
     */
    public function addItemTmp($product_id)
    {
        $currentProduct = '';

        $session_id = Session::getId();

        $currentProducts = BookingTmp::getProducts($session_id);

        if (count($currentProducts) == 0) {
            $booking_temp = new BookingTmp;
            $booking_temp->session_id = $session_id;
            $booking_temp->product_ids = json_encode([$product_id]);
            $booking_temp->save();
        } else {
            $currentProduct = json_decode($currentProducts[0]->product_ids);
            if (!in_array($product_id, $currentProduct)) {
                array_push($currentProduct, $product_id);
            }

            $ubook = BookingTmp::find($currentProducts[0]->id);
            $ubook->product_ids = json_encode($currentProduct);
            $ubook->save();
        }

        $html = print_r($currentProduct, 1);

        return Response::json(['data' => ['html' => $html, 'product_id' => $product_id, 'redirecturl' => '/']]);
    }

    /**
     * AJAX request: Called everytime a product is added to a new booking.
     *
     * @return mixed
     */
    public function loadItemsInTableSession()
    { //@TODO take this data from session everytime user reloads page. @Jakob
        $html = '';
        $session_id = Session::getId();
        $currentProductsJson = BookingTmp::getProducts($session_id);

        if (strlen($currentProductsJson) > 3) {
            $currentProducts = json_decode($currentProductsJson[0]->product_ids);
            $table = '<table class="table table-striped">';
            $table .= '<thead><th>Category</th><th>Supplier</th><th>Country</th><th>Products & Service</th><th>Price</th><th>Remove</th></thead>';
            foreach ($currentProducts as $key => $item) {
                $product = Product::find($item);
                $table .= '<tr>
                <td>
                <input type="hidden" name="product_ids[]" value="'
                    . $product->id . '">'
                    . $product->category->name
                    . '</td><td>' . $product->supplier->name
                    . '</td><td>' . $product->country->name
                    . '</td><td>' . $product->name . '</td><td>'
                    . round($product->fee, 2)
                    . '</td><td><a href="'
                    . url('booking/remove_tmp_item/' . $product->id)
                    . '"><i class="pe-7s-close-circle"></i></a></td></tr>';
            }
            $table .= '</table>';
            $html .= $table;
        }

        return Response::json(['data' => ['html' => $html, 'redirecturl' => '/']]);
    }

    /**
     * Delete temp item from booking temp.
     *
     * @param $product_id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function removeTmpItem($product_id)
    {//@TODO make this AJAX call @Jakob
        $session_id = Session::getId();
        $currentProductsJson = BookingTmp::getProducts($session_id);
        $currentProducts = json_decode($currentProductsJson[0]->product_ids);

        $products = [];
        foreach ($currentProducts as $key => $item) {
            if ($product_id != $item) {
                $products[] = $item;
            }
        }
        $ubook = BookingTmp::find($currentProductsJson[0]->id);
        $ubook->product_ids = json_encode($products);
        $ubook->save();

        return redirect('booking/create');
    }

    /**
     * Save a new Booking.
     *
     * @param BookingRequest $request
     * @return mixed
     */
    private function createBooking(BookingRequest $request)
    {
        $booking = Auth::user()->bookings()->create($request->all());
        $this->syncLeadPassenger($booking, $request->input('lead_traveler_id'));
        if($request->input('product_ids')){
            $this->saveBookingItems($booking, $request);
            $this->destroyTempBooking();
        }

        return $booking;
    }

    /**
     * Create booking item from product,
     * and duplicate price and itinerary sections from Product.
     *
     * @param $booking
     * @param BookingRequest $request
     * @return string
     */
    private function saveBookingItems($booking, BookingRequest $request)
    {
        $item_ids = $request->input('product_ids');

        foreach ($item_ids as $product_id) {
            $this->addItem($booking, $product_id);
        }

    }

    /**
     * AJAX request: which adds item to a Booking on a booking details view.
     *
     * @param $booking
     * @param $product_id
     * @return mixed
     */
    public function addItem(Booking $booking, $product_id)
    {
        if ($product_id > 0) {
            $product = Product::find($product_id);
            BookingItem::createFromProduct($product, $booking);
        }

        return Response::json(['data' => ['html' => 'added', 'p_id' => $product_id, 'redirecturl' => '/']]);
    }

    /**
     * Sync up the list of passengers in the database.
     *
     * @param Booking $booking
     * @param $travelers
     */
    private function syncPassengers(Booking $booking, array $travelers)
    {
        $booking->passengers()->sync($travelers);
    }

    /**
     * Sync up the lead passenger in the database.
     *
     * @param $booking
     * @param $leadTraveler_id
     */
    private function syncLeadPassenger(Booking $booking, $leadTraveler_id)
    {
        $booking->passengers()->sync([$leadTraveler_id => ['lead_traveler' => 1]]);

    }

    /**
     * Destroy temp booking with added products.
     */
    private function destroyTempBooking()
    {
        BookingTmp::where('session_id', Session::getId())->delete();
    }

    /**
     * AJAX Request: which loads the items of a Booking on booking details page.
     *
     * @param $booking_id
     * @return mixed
     */
    public function loadItems($booking_id)
    {
        $html = '';
        $booking = Booking::find($booking_id);
        $items = $booking->items;

        if (!empty($items)) {
            $tbl = '<table class="table table-striped" id="tbl_booking_items">';
            $tbl .= '<thead>
                    <th>Category</th>
                    <th>Supplier</th>
                    <th>Country</th>
                    <th>Products & Service</th>
                    <th>Price</th><th>Remove</th>
                    </thead>';
            $commission = $booking->commission();

            $tax = $booking->taxes();
            $subtotal = $booking->subtotal();
            $total = $booking->totalCost();

            foreach ($items as $item) {
                $itemPrice = $item->totalPrice();
                $tbl .= '<tr data="' . $item->product_id . ':' . $item->id . '"" id="row-' . $item->product_id . '" class="tr-row products_list"><td><input type="hidden" name="product_ids[]" value="' . $item->id . '">' . $item->category->name . '</td><td>' . $item->product->supplier->name . '</td><td>' . $item->country->name . '</td>';

                $tbl .= '<td><a data="' . $item->product_id . ':' . $item->id . '" id="link-' . $item->product_id . '" href="" class="link_item_details" >' . $item->product->name . '</a></td><td>' . $itemPrice . '</td><td><a href="' . url('booking/item/remove/' . $item->booking_id . '/' . $item->id) . '" ><i class="pe-7s-close-circle"></i></a></td></tr>';
            }

            $tbl .= '<tr class="tr-row"><td></td><td></td><td></td><td><strong>Sub-total</strong></td><td><strong>' . $subtotal . '</strong></td><td></td></tr>';
            $tbl .= '<tr class="tr-row"><td></td><td></td><td></td><td><strong>Tax</strong></td><td><strong>' . round($subtotal * ($tax / 100), 2) . '(' . $tax . '%)</strong></td><td></td></tr>';
            $tbl .= '<tr class="tr-row"><td></td><td></td><td></td><td><strong>Commission</strong></td><td><strong>' . round($subtotal * ($commission / 100), 2) . '(' . $commission . '%)</strong></td><td></td></tr>';
            $tbl .= '<tr class="tr-row"><td></td><td></td><td></td><td><strong>User Defined</strong></td><td><input type="number" onKeyUp="if(this.value>1000000000){this.value=\'1000000000\';}else if(this.value<-1000000000){this.value=\'-1000000000\';}" min="-1000000000" max="1000000000" class="form-control user_defined"></td><td></td></tr>';
            $tbl .= '<tr class="tr-row"><td></td><td></td><td></td><td><strong>User Defined</strong></td><td><input type="number" onKeyUp="if(this.value>1000000000){this.value=\'1000000000\';}else if(this.value<-1000000000){this.value=\'-1000000000\';}" min="-1000000000" max="1000000000" class="form-control user_defined"></td><td></td></tr>';
            $tbl .= '<tr class="tr-row"><td></td><td></td><td></td><td><h3><strong>Total Cost Of Booking</strong></h3></td><td id="total_cost"><h3><strong>' . $total . '</strong></h3></td><td></td></tr>';
            $tbl .= '</table>';
            $html .= $tbl;
        }

        return Response::json(['data' => ['html' => $html, 'redirecturl' => '/']]);
    }

    /**
     * Remove item from booking.
     *
     * @param $booking_id
     * @param BookingItem $item
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function removeItem($booking_id, BookingItem $item)
    {
        if ($item->booking_id == $booking_id) {
            $item->delete();
        }

        return redirect('booking/' . $booking_id);
    }

    /**
     * Remove all temp products from booking.
     */
    public function removeTmpItems()
    {
        $session_id = Session::getId();
        $tmpBooking = BookingTmp::where('session_id', $session_id)->first();
        $tmpBooking->delete(); //@TODO delete items from bookings items table @Jakob
    }

    /*--------------------------------------------OLD-----------------------------------------*/

    /**
     * AJAX request: which saves order of events when arranged
     *
     * @param Booking $booking
     * @param Request $request
     */
    public function orderEvents(Booking $booking, Request $request)
    {
        dd($request->all());
        $booking->event_order = $request->input('order_events');
        $booking->save();
        echo 'saved';
    }

    public function generate_itinerary(ItineraryWordRepository $itineraryWordRepository, $booking_id, $template_id)
    {
        return $itineraryWordRepository->generate_itinerary($template_id);
    }

    public function generate_invoice(Invoice $invoice, $booking_id, $template_id)
    {
        $invoice->generateInvoice($template_id);
    }

    public function download($type, $booking_id, $ext)
    {
        switch ($ext) {
            case "docx":
                $contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                break;

            case "pdf":
                $contentType = "application/pdf";
                break;
        }
        if ($type == '1') {
            $booking = Booking::find($booking_id);
            $filename = $booking->filename . '.' . $ext;
            $pathToFile = 'documents/results/' . $filename;
            $headers = [
                'Content-Type' => 'application/pdf',
            ];
        } elseif ($type == '2') {
            $filename = (new Invoice($booking_id))->getOutputFileName() . '.' . $ext;
            $pathToFile = 'documents/results/' . $filename;
            $headers = [
                'Content-Type' => $contentType,
            ];
        }

        return response()->download($pathToFile, $filename, $headers);
    }
}
