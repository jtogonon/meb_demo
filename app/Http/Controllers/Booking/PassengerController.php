<?php

namespace App\Http\Controllers\Booking;

use App\Booking;
use App\Country;
use App\Http\Controllers\BaseController;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Passenger;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Roumen\Asset\Asset;
use Session;
use Entrust;


class PassengerController extends BaseController
{

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;

        parent::__construct();

        $jsArray = [
            '/assets/jquery/dist/jquery.min.js',
            '/assets/jquery-ui/jquery-ui.min.js',
            '/assets/slimScroll/jquery.slimscroll.min.js',
            '/assets/bootstrap/dist/js/bootstrap.min.js',
            '/assets/metisMenu/dist/metisMenu.min.js',
            '/assets/iCheck/icheck.min.js',
            '/assets/peity/jquery.peity.min.js',
            '/assets/sweetalert/lib/sweet-alert.min.js',
            '/assets/sparkline/index.js',
            '/scripts/homer.js',

        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/fontawesome/css/font-awesome.css',
            '/assets/metisMenu/dist/metisMenu.css',
            '/assets/animate.css/animate.css',
            '/assets/bootstrap/dist/css/bootstrap.css',
            '/assets/sweetalert/lib/sweet-alert.css',
        ];
        Asset::add($cssArray, 'headerCss');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        if (!Entrust::hasRole('SuperAdmin')) { //@TODO create middleware @Jakob
            return redirect('home');
        }

        $jsArray = ['/assets/select2-3.5.2/select2.min.js', '/assets/steps/jquery.steps.min.js', '/assets/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js',
            '/assets/datatables/jquery.DataTable.1.10.9.min.js',
            '/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js', '/assets/toastr/build/toastr.min.js',
            '/scripts/customers.js',
        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css',
        ];
        Asset::add($cssArray, 'headerCss');

        $data = ['page_title' => $this->page_title,
            'title_TopPanel' => 'Customers',
            'show_TopPanel' => true,
            'TopPanel_button' => '<a id="link_popup_newcustomer" class="btn btn-default btn-sm" href="/customers/add">Add Customer</a>',
        ];

        $lead_travelers = Passenger::leadTravelers();

        $data['lead_travelers'] = $lead_travelers;

        return view('users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('passenger.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['first_name' => 'required', 'last_name' => 'required', ]);

        Passenger::create($request->all());

        Session::flash('flash_message', 'Passenger added!');

        return redirect('passenger');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $passenger = Passenger::findOrFail($id);

        return view('passenger.show', compact('passenger'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $passenger = Passenger::findOrFail($id);

        return view('passenger.edit', compact('passenger'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['first_name' => 'required', 'last_name' => 'required', ]);

        $passenger = Passenger::findOrFail($id);
        $passenger->update($request->all());

        Session::flash('flash_message', 'Passenger updated!');

        return redirect('passenger');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Passenger::destroy($id);

        Session::flash('flash_message', 'Passenger deleted!');

        return redirect('passenger');
    }

    /**
     * AJAX request: for popup. Get bookings all products.
     *
     * @param Booking $booking
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function bookingsPassengers(Booking $booking)
    {
        $data['booking'] = $booking;

        //get countries
        $data['countries'] = Country::pluck('name', 'id');
        $data['lead_traveler_id'] = $booking->leadTraveler()->id;

        $passengers = $booking->passengers;

        if (count($passengers) == 0) {
            $data['has_passengers'] = false;
        } else {
            $data['has_passengers'] = true;
            $data['passengers'] = $passengers;
        }

        return view('booking._view_passengers', $data);
    }

    /**
     * AJAX request: which will add row,
     * group of elements to add passenger
     * for booking shown in popup
     *
     * @param $num
     * @return View
     */
    public function addRowPassenger($num)
    {
        $countries = Country::pluck('name', 'id');

        return view('booking._view_passengers_addrow', compact('num', 'countries'));
    }

    /**
     * AJAX request: which saves data from popup
     *
     * @param Request $request
     * @return mixed
     */
    public function savePassengers(Request $request)
    {
        foreach ($request->input('passenger') as $passenger => $key) {
            if (isset($_POST['user_id'][$passenger])) {
                $passenger_id = $_POST['user_id'][$passenger];
                $user = User::find($passenger_id);
            } else {
                $user = new User;
            }
            $user->user_id = Auth::user()->id;
            $user->prefix = $_POST['prefix_' . $passenger];
            $user->first_name = $_POST['first_name_' . $passenger];
            $user->middle_name = $_POST['middle_' . $passenger];
            $user->last_name = $_POST['last_name_' . $passenger];
            $user->suffix = $_POST['suffix_' . $passenger];
            $user->lead_customer = $_POST['lead_customer'];
            $user->date_of_birth = Booking::formatDate($_POST['dob_' . $passenger], 1);
            $user->gender = $_POST['gender_' . $passenger];
            $user->save();

            //save address
            if (isset($_POST['user_id'][$passenger])) {
                $user_id = $_POST['user_id'][$passenger];

                $address = Address::find($user->address->id);
                $address->country_id = $_POST['country_' . $passenger];
                $address->save();
            } else {
                $address = new Address;
                $address->user_id = $user->id;

                $address->country_id = $_POST['country_' . $passenger];
                $address->save();

                //determine if adult
                $birthDate = explode("/", $_POST['dob_' . $passenger]);
                //get age from date or birthdate
                $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
                    ? ((date("Y") - $birthDate[2]) - 1)
                    : (date("Y") - $birthDate[2]));
                $isAdult = ($age > 12) ? '1' : '0';
            }

            if (isset($_POST['user_id'][$passenger])) {
            } else {
                //save passenger
                $pasngr = new Passenger;
                $pasngr->booking_id = $_POST['booking_id'];
                $pasngr->user_id = $user->id;
                $pasngr->adult = $isAdult;
                $pasngr->save();
            }
        }
        $html = $_POST['prefix_' . $passenger];

        return Response::json(['data' => ['html' => $html]]);
    }

}
