<?php namespace App\Http\Controllers;

use App\Http\Requests\BookingRequest;
use App\ProductFee;
use App\Repositories\DocumentWriter\Invoice;
use App\Repositories\DocumentWriter\ItineraryWordRepository;
use App\Repositories\FileMakerRepository;

use PDF;
use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use App\User, Response, Asset, Html;

use App\Booking;
use App\BookingItem, App\BookingPassenger;
use App\Role, App\BookingTmp;
use App\Country, App\Nationality, App\Agent, App\Address;
use App\Supplier, App\Product, App\ProductType;
use App\TripPurpose, App\ProductPromo, App\ItineraryTemplate;
use App\BookingItemDetail;
use Illuminate\Support\Facades\View;

class BookingsController extends BaseController
{
    public $layout = 'layout';

    public $page_title = 'Dashboard';

    public function __construct(Guard $auth)
    {
        $this->middleware('auth');
        $this->auth = $auth;

        parent::__construct();

        $jsArray = [
            '/assets/jquery/dist/jquery.min.js',
            '/assets/jquery-ui/jquery-ui.min.js',
            '/assets/slimScroll/jquery.slimscroll.min.js',
            '/assets/bootstrap/dist/js/bootstrap.min.js',
            '/assets/metisMenu/dist/metisMenu.min.js',
            '/assets/iCheck/icheck.min.js',
            '/assets/peity/jquery.peity.min.js',
            '/assets/sweetalert/lib/sweet-alert.min.js',
            '/assets/sparkline/index.js',
            '/scripts/homer.js',
        ];
        Asset::add($jsArray, 'footer');

        $cssArray = [
            '/assets/fontawesome/css/font-awesome.css',
            '/assets/metisMenu/dist/metisMenu.css',
            '/assets/animate.css/animate.css',
            '/assets/bootstrap/dist/css/bootstrap.css',
            '/assets/sweetalert/lib/sweet-alert.css',
        ];
        Asset::add($cssArray, 'headerCss');

        $this->title_TopPanel = 'Bookings';

        $this->TopPanel_submenu = ['Dashboard' => '/', 'Customers' => '/customers', 'Products' => '/products/index'];
    }

    public function index()
    {
        $jsArray = [
            '/assets/datatables/media/js/jquery.dataTables.min.js',
            '/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js'
        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css',
        ];
        Asset::add($cssArray, 'headerCss');

        //start here
        $auth = $this->auth->user();
        $data = [
            'page_title' => $this->page_title,
            'title_TopPanel' => 'Bookings',
            'TopPanel_button' => '<a class="btn btn-default btn-sm" href="/bookings/new">Add Booking</a>',
            'TopPanel_submenu' => $this->TopPanel_submenu
        ];

        $bookings = Booking::where('user_id', Auth::user()->id)->get();

        $data['bookings'] = $bookings;

        return view('bookings/index', $data);
    }

    public function create($supplier_id = 0)
    {
        $users = User::getCustomersByOperatorId(Auth::user()->id);
        $customers = ['' => '-Select-'];
        foreach ($users as $customer => $customer) {
            $customers[$customer->id] = $customer['first_name'] . ' ' . $customer['last_name'];
        }

        $suppliers = Supplier::getSuppliersByOperatorId(Auth::user()->id);

        $sups = ['' => '-select-'];
        foreach ($suppliers as $supplier => $s) {
            $sups[$s['id']] = $s['supplier_name'];
        }

        $data['supplier_id'] = $supplier_id;
        $data['customers'] = $customers;
        $data['suppliers'] = $sups;
        //get countries
        $data['countries'] = Country::lists('name', 'id');
        //purpose of trip
        $data['trip_purposes'] = TripPurpose::lists('purpose', 'id');
        $data['agents'] = [];
        $data['booking_items'] = [];
        $data['show_TopPanel'] = false;
        $data['page_title'] = $this->page_title;

        //generate new session id
        $session_id = Session::getId();
        if (!$session_id) {
            Session::regenerate();
        }

        return view('bookings.tabs', $data);
    }

    /**
     * Save new booking.
     *
     * @param BookingRequest|Request $request
     * @return mixed
     */
    public function store(Request $request) // BookingRequest
    {
        if ($request->input('booking_id')) {
            $booking = Booking::find($request->input('booking_id'));
            $status = $request->input('status');
            $booking->booking_type = $request->input('booking_type');
            $booking->group_code = $request->input('group_code');
        } else {
            $booking = new Booking;
            $status = 'quote';
        }

        if ($request->input('hidden_user_id')) {
            $user_id = $request->input('hidden_user_id');
        } else {
            $user_id = $request->input('user_id');
        }

        $booking->user_id = $user_id;
        $booking->booking_name = $request->input('booking_name');
        $booking->status = $status;
        $booking->start_date = Booking::formatDate($request->input('start_date'), 1);
        $booking->end_date = Booking::formatDate($request->input('end_date'), 1);
        $booking->booking_date = Booking::formatDate($request->input('booking_date'), 1);
        $booking->tour_people_num = $request->input('travelers_count');
        $booking->tour_code = $request->input('tour_code');
        $booking->purpose_id = $request->input('purpose_id');
        $booking->save();

        if (!$request->input('booking_id')) {
            //save booking items
            $item_ids = $request->input('product_ids');
            foreach ($item_ids as $id) {
                if ($id > 0) {
                    $item = new BookingItem;
                    $item->booking_id = $booking->booking_id;
                    $item->product_id = $id;
                    $item->user_id = $request->input('user_id');
                    $item->fee = Product::getFee($id);
                    $item->save();
                }
            }
        }

        return Response::json(['data' => ['html' => 'saved', 'redirecturl' => '/bookings/details/' . $booking->booking_id]]);
    }

    public function details($booking_id)
    {
        $auth = $this->auth->user();

        $jsArray = ['/assets/select2-3.5.2/select2.min.js', '/scripts/bookings-new.js', '/scripts/bookings.js', '/assets/steps/jquery.steps.min.js', '/assets/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js',
            '/assets/datatables/jquery.DataTable.1.10.9.min.js',
            '/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js', '/assets/toastr/build/toastr.min.js',
            '/assets/summernote/dist/summernote.min.js',
            '/assets/nestable/jquery.nestable.js'
        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/select2-3.5.2/select2.css', '/assets/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css', '/assets/toastr/build/toastr.min.css',
            '/assets/summernote/dist/summernote.css',

        ];
        Asset::add($cssArray, 'headerCss');

        $data = [];

        /*booking details start*/
        $bookings = Booking::findBooking($booking_id, Auth::user()->id);
        if (count($bookings) == 0) {
            return redirect('bookings/new');
        }
        $booking = $bookings[0];
        $booking->start_date = Booking::formatDate($booking->start_date, 3);
        $booking->end_date = Booking::formatDate($booking->end_date, 3);
        $booking->booking_date = Booking::formatDate($booking->booking_date, 2);

        /*booking details end*/

        $data['booking'] = $booking;
        $data['supplier_id'] = 0;

        $suppliers = Supplier::getSuppliersByOperatorId(Auth::user()->id);
        $sups = ['' => '-select-'];
        foreach ($suppliers as $supplier => $s) {
            $sups[$s['id']] = $s['supplier_name'];
        }
        $data['suppliers'] = $sups;

        //purpose of trip
        $data['trip_purposes'] = TripPurpose::lists('purpose', 'id');

        $users = User::getCustomersByOperatorId(Auth::user()->id);
        $customers = ['' => '-Select-'];
        foreach ($users as $customer => $c) {
            $customers[$c->id] = $c['first_name'] . ' ' . $c['last_name'];
        }

        $data['customers'] = $customers;

        $prods = Product::getProductsByOperatorId(Auth::user()->id);
        $data['products'] = $prods;

        $data['booking_items'] = $booking->items;


        $data['customer'] = User::find($booking->user_id);
        $totalPassengers = BookingPassenger::countPassengers($booking->booking_id);

        $data['pax_total'] = ($totalPassengers['adults'] + $totalPassengers['children']);

        //get countries
        $data['countries'] = Country::lists('name', 'id');

        $data['itinerary_templates'] = ItineraryTemplate::lists('name', 'id');

        $data['agents'] = [];

        $data['show_TopPanel'] = false;
        $data['page_title'] = $this->page_title;

        $nextPrevs = Booking::getNexPrev($booking->booking_id, Auth::user()->id);
        if (count($nextPrevs) == 1) {
            if (isset($nextPrevs[0]->booking_id) && $booking_id < $nextPrevs[0]->booking_id) {
                $data['prev'] = 0;
                $data['next'] = $nextPrevs[0]->booking_id;
            }
            if (isset($nextPrevs[0]->booking_id) && $booking_id > $nextPrevs[0]->booking_id) {
                $data['prev'] = $nextPrevs[0]->booking_id;
                $data['next'] = 0;
            }
        }

        if (count($nextPrevs) == 2) {
            $data['prev'] = (isset($nextPrevs[0]->booking_id)) ? $nextPrevs[0]->booking_id : 0;
            $data['next'] = (isset($nextPrevs[1]->booking_id)) ? $nextPrevs[1]->booking_id : 0;
        }

        return view('bookings/details', $data);
    }

    public function add_item_tmp($product_id, $urlDetails_view = null)
    {
        $itemsArr = [];
        $html = '';
        $curProd = '';

        $session_id = Session::getId();

        $curProducts = BookingTmp::getProducts($session_id);
        //print_r($curProducts);

        if (count($curProducts) == 0) {
            $book = new BookingTmp;
            $book->session_id = $session_id;
            $book->product_ids = json_encode([$product_id]);
            $book->save();
            $itemsArr = [$product_id];

        } else {
            $curProd = json_decode($curProducts[0]->product_ids);
            if (!in_array($product_id, $curProd)) {
                array_push($curProd, $product_id);
            }

            $ubook = BookingTmp::find($curProducts[0]->id);
            $ubook->product_ids = json_encode($curProd);
            $ubook->save();
            $itemsArr = $curProd;

        }

        $html = print_r($curProd, 1);

        return Response::json(['data' => ['html' => $html, 'p_id' => $product_id, 'redirecturl' => '/']]);
    }

    /**
     * AJAX request: Called everytime a product is added to a new booking.
     *
     * @return mixed
     */
    public function load_items_inTableSession()
    {
        $html = '';
        $session_id = Session::getId();
        $curProducts = BookingTmp::getProducts($session_id);

        if (strlen($curProducts) > 3) {
            $curProds = json_decode($curProducts[0]->product_ids);
            $tbl = '<table class="table table-striped">';
            $tbl .= '<thead><th>Category</th><th>Supplier</th><th>Country</th><th>Products & Service</th><th>Price</th><th>Remove</th></thead>';
            foreach ($curProds as $key => $item) {
                $p = Product::find($item);
                $tbl .= '<tr><td><input type="hidden" name="product_ids[]" value="' . $p->id . '">' . $p->category->category_name . '</td><td>' . $p->supplier->supplier_name . '</td><td>' . $p->country->name . '</td><td>' . $p->product_name . '</td><td>' . round($p->fee, 2) . '</td><td><a href="/bookings/remove_tmp_item/' . $p->id . '"><i class="pe-7s-close-circle"></i></a></td></tr>';
            }
            $tbl .= '</table>';
            $html .= $tbl;
        }

        return Response::json(['data' => ['html' => $html, 'redirecturl' => '/']]);
    }

    public function clear_items()
    {
        //Session::forget('booking_items');
        $session_id = Session::getId();
        $tmp = BookingTmp::where('session_id', $session_id)->get();
        $tmp->delete();
    }

    public function remove_tmp_item($product_id)
    {
        $session_id = Session::getId();
        $curProducts = BookingTmp::getProducts($session_id);
        $curProds = json_decode($curProducts[0]->product_ids);

        $newArr = [];
        foreach ($curProds as $key => $item) {
            if ($product_id != $item) {
                $newArr[] = $item;
            }
        }
        $ubook = BookingTmp::find($curProducts[0]->id);
        $ubook->product_ids = json_encode($newArr);
        $ubook->save();

        return redirect('bookings/new');
    }

    public function remove_item($booking_id, $item_id)
    {
        $item = BookingItem::find($item_id);
        if ($item->booking_id == $booking_id) {
            $item->delete();
        }

        return redirect('bookings/details/' . $booking_id);
    }

    /**
     * AJAX request for products search tab in bookings/new page.
     *
     * @param null $category_id
     * @param $detailsView
     * @return mixed
     */
    public function searchProducts($category_id = null, $detailsView)
    {
        $operator_id = Auth::user();

        // @TODO delete this @Jakob
        if ($category_id == 0) {
            $category_id = null;
        }

        $data['products'] = Product::getProducts($operator_id, $category_id);

        $data['detailsView'] = $detailsView;

        $html = view('bookings.search_products')->with($data)->render();

        return Response::json(['data' => ['html' => $html, 'redirecturl' => '/']]);
    }

    /**
     * AJAX Request: which loads the items of a Booking on booking details page.
     *
     * @param $booking_id
     * @return mixed
     */
    public function load_items($booking_id)
    {
        $html = '';
        $items = BookingItem::getItemsByBookingId($booking_id);
        $booking = Booking::find($booking_id);

        if (!empty($items)) {
            $tbl = '<table class="table table-striped" id="tbl_booking_items">';
            $tbl .= '<thead><th>Category</th><th>Supplier</th><th>Country</th><th>Products & Service</th><th>Price</th><th>Remove</th></thead>';
            $commission = $booking->commission();
            $tax = $booking->taxes();
            $subtotal = $booking->subtotal();
            $total = $booking->totalCost();
            foreach ($items as $i) {
                $p = Product::find($i->product_id);
                $itemPrice = $i->totalPrice();
                $tbl .= '<tr data="' . $i->product_id . ':' . $i->booking_item_id . '"" id="row-' . $i->product_id . '" class="tr-row products_list"><td><input type="hidden" name="product_ids[]" value="' . $i->booking_item_id . '">' . $p->category->category_name . '</td><td>' . $p->supplier->supplier_name . '</td><td>' . $p->country->name . '</td>';

                $tbl .= '<td><a data="' . $i->product_id . ':' . $i->booking_item_id . '" id="link-' . $i->product_id . '" href="" class="link_item_details" >' . $p->product_name . '</a></td><td>' . $itemPrice . '</td><td><a href="/bookings/remove_item/' . $i->booking_id . '/' . $i->booking_item_id . '" ><i class="pe-7s-close-circle"></i></a></td></tr>';
            }
            $tbl .= '<tr class="tr-row"><td></td><td></td><td></td><td><strong>Sub-total</strong></td><td><strong>' . $subtotal . '</strong></td><td></td></tr>';
            $tbl .= '<tr class="tr-row"><td></td><td></td><td></td><td><strong>Tax</strong></td><td><strong>' . round($subtotal * ($tax / 100), 2) . '(' . $tax . '%)</strong></td><td></td></tr>';
            $tbl .= '<tr class="tr-row"><td></td><td></td><td></td><td><strong>Commission</strong></td><td><strong>' . round($subtotal * ($commission / 100), 2) . '(' . $commission . '%)</strong></td><td></td></tr>';
            $tbl .= '<tr class="tr-row"><td></td><td></td><td></td><td><strong>User Defined</strong></td><td><input type="number" onKeyUp="if(this.value>1000000000){this.value=\'1000000000\';}else if(this.value<-1000000000){this.value=\'-1000000000\';}" min="-1000000000" max="1000000000" class="form-control user_defined"></td><td></td></tr>';
            $tbl .= '<tr class="tr-row"><td></td><td></td><td></td><td><strong>User Defined</strong></td><td><input type="number" onKeyUp="if(this.value>1000000000){this.value=\'1000000000\';}else if(this.value<-1000000000){this.value=\'-1000000000\';}" min="-1000000000" max="1000000000" class="form-control user_defined"></td><td></td></tr>';
            $tbl .= '<tr class="tr-row"><td></td><td></td><td></td><td><h3><strong>Total Cost Of Booking</strong></h3></td><td id="total_cost"><h3><strong>' . $total . '</strong></h3></td><td></td></tr>';
            $tbl .= '</table>';
            $html .= $tbl;
        }

        return Response::json(['data' => ['html' => $html, 'redirecturl' => '/']]);
    }

    /**
     * AJAX request: which adds item to a Booking on a booking details view.
     *
     * @param $product_id
     * @param $booking_id
     * @return mixed
     */
    public function add_item($product_id, $booking_id)
    {
        $booking = Booking::find($booking_id);
        $p = Product::find($product_id);
        $item = new BookingItem;
        $item->booking_id = $booking_id;
        $item->product_id = $product_id;
        $item->user_id = $booking->user_id;
        $item->fee = $p->fee;
        if ($item->save()) {
            $html = 'ok';
        } else {
            $html = 'fail';
        }

        return Response::json(['data' => ['html' => $html, 'p_id' => $product_id, 'redirecturl' => '/']]);
    }

    /**
     * AJAX request: show in popup
     *
     * @param $booking_id
     * @return View
     */
    public function view_passengers($booking_id)
    {
        $data['booking_id'] = $booking_id;
        $data['booking'] = Booking::find($booking_id);
        //get countries
        $data['countries'] = Country::lists('name', 'id');

        $passengers = BookingPassenger::getPassengers($booking_id);
        if (count($passengers) == 0) {
            $data['has_passengers'] = false;
        } else {
            $data['has_passengers'] = true;
            $data['passengers'] = $passengers;
        }

        return view('bookings/_view_passengers', $data);
    }

    /**
     * AJAX request: which will add row,
     * group of elements to add passenger
     * for booking shown in popup
     *
     * @param $num
     * @return View
     */
    public function addRowPassenger($num)
    {
        //get countries
        $data['countries'] = Country::lists('name', 'id');
        $data['num'] = $num;

        return view('bookings/_view_passengers_addrow', $data);
    }

    /**
     * AJAX request: which saves data from popup
     *
     * @return mixed
     */
    public function save_passengers()
    {
        $auth = $this->auth->user();
        $html = '';
        foreach ($_POST['passenger'] as $pas => $key) {
            if (isset($_POST['user_id'][$pas])) {
                $user_id = $_POST['user_id'][$pas];
                $user = User::find($user_id);

            } else {
                $user = new User;
            }
            $user->user_id = Auth::user()->id;
            $user->prefix = $_POST['prefix_' . $pas];
            $user->first_name = $_POST['first_name_' . $pas];
            $user->middle_name = $_POST['middle_' . $pas];
            $user->last_name = $_POST['last_name_' . $pas];
            $user->suffix = $_POST['suffix_' . $pas];
            $user->lead_customer = $_POST['lead_customer'];
            $user->date_of_birth = Booking::formatDate($_POST['dob_' . $pas], 1);
            $user->gender = $_POST['gender_' . $pas];
            $user->save();

            //save address
            if (isset($_POST['user_id'][$pas])) {
                $user_id = $_POST['user_id'][$pas];

                $address = Address::find($user->address->id);
                $address->country_id = $_POST['country_' . $pas];
                $address->save();
            } else {
                $address = new Address;
                $address->user_id = $user->id;

                $address->country_id = $_POST['country_' . $pas];
                $address->save();

                //determine if adult
                $birthDate = explode("/", $_POST['dob_' . $pas]);
                //get age from date or birthdate
                $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
                    ? ((date("Y") - $birthDate[2]) - 1)
                    : (date("Y") - $birthDate[2]));
                $isAdult = ($age > 12) ? '1' : '0';
            }

            if (isset($_POST['user_id'][$pas])) {
            } else {
                //save passenger
                $pasngr = new BookingPassenger;
                $pasngr->booking_id = $_POST['booking_id'];
                $pasngr->user_id = $user->id;
                $pasngr->adult = $isAdult;
                $pasngr->save();
            }
        }
        $html = $_POST['prefix_' . $pas];

        return Response::json(['data' => ['html' => $html]]);
    }

    /**
     * AJAX request: which loads list of recent bookings.
     *
     * @return mixed
     */
    public function getLatestBookings()
    {
        $auth = $this->auth->user();
        $data['bookings'] = Booking::getLatestBookingsByOperatorId(Auth::user()->id);
        $html = view('bookings/_latest_bookings')->with($data)->render();

        return Response::json(['data' => ['html' => $html]]);
    }

    /**
     * AJAX request: which loads list of recent bookings in the search tab.
     *
     * @return mixed
     */
    public function getLatestBookingsSearch()
    {
        $bookings = Booking::getLatestBookingsByOperatorId(Auth::user()->id);

        foreach ($bookings as $booking) {
            $start_date = Booking::formatDate($booking->start_date, 3);
            $end_date = Booking::formatDate($booking->end_date, 3);
            $booking->start_date = $start_date;
            $booking->end_date = $end_date;
        }

        $data['bookings'] = $bookings;

        $html = view('bookings/_latest_bookings_searchtab')->with($data)->render();

        return Response::json(['data' => ['html' => $html]]);
    }

    /**
     * AJAX request: which submit product item details for a booking from popup.
     *
     * @param Request $req
     * @return mixed
     */
    public function save_product_details(Request $req)
    {
        $auth = $this->auth->user();
        if ($req->ajax()) {
            $item = BookingItemDetail::checkBookingIdAndProductId($_POST['booking_id'], $_POST['product_id'], $_POST['booking_item_id']);
            if (!$item) {
                //save new item details
                $detail = new BookingItemDetail;
            } else {
                $detail = BookingItemDetail::find($item[0]->id);
            }
            $detail->user_id = Auth::user()->id;
            $detail->booking_id = $_POST['booking_id'];
            $detail->booking_item_id = $_POST['booking_item_id'];
            $detail->product_id = $_POST['product_id'];
            $detail->type = $req->input('bookingtype');

            if ($req->input('bookingtype') == 'accomodation') {
                $detail->acco_room_category = $req->input('room_category_id');
                $detail->acco_hotel = $req->input('acco_hotel');
                $detail->acco_checkin = Booking::formatDate($req->input('acco_checkin'), 1);
                $detail->acco_checkout = Booking::formatDate($req->input('acco_checkout'), 1);
                $detail->acco_rooms = $req->input('acco_rooms');
                $detail->acco_num_adults = $req->input('acco_num_adults');
                $detail->acco_num_children = $req->input('acco_num_children');
                $detail->acco_airline = $req->input('acco_airline');
                $detail->acco_flight = $req->input('acco_flight');
                $detail->acco_depart = Booking::formatDate($req->input('acco_depart'), 1);
                $detail->acco_arrival = Booking::formatDate($req->input('acco_arrival'), 1);
                $detail->acco_origin = $req->input('acco_origin');
                $detail->acco_destination = $req->input('acco_destination');
                $detail->notes = $req->input('notes');
                $detail->save();
            }

            if ($req->input('bookingtype') == 'flight') {
                $detail->flight_from_city = $req->input('flight_from_city');
                $detail->flight_from_date = Booking::formatDate($req->input('flight_from_date'), 1);
                $detail->flight_to_city = $req->input('flight_to_city');
                $detail->flight_to_date = Booking::formatDate($req->input('flight_to_date'), 1);
                $detail->flight_num_passengers = $req->input('flight_num_passengers');
                $detail->notes = $req->input('notes');
                $detail->save();
            }

            if ($req->input('bookingtype') == 'carrental') {
                $detail->carrent_location_pickup = $req->input('carrent_location_pickup');
                $detail->carrent_date_pickup = Booking::formatDate($req->input('carrent_date_pickup'), 1);
                $detail->carrent_time_pickup = $req->input('carrent_time_pickup');
                $detail->carrent_location_return = $req->input('carrent_location_return');
                $detail->carrent_date_return = Booking::formatDate($req->input('carrent_date_return'), 1);
                $detail->carrent_time_return = $req->input('carrent_time_return');
                $detail->carrent_vehicleclass = $req->input('carrent_vehicleclass');
                $detail->carrent_age = $req->input('carrent_age');
                $detail->notes = $req->input('notes');
                $detail->save();
            }

            if ($req->input('bookingtype') == 'activity') {
                $detail->act_activityname = $req->input('act_activityname');
                $detail->act_date = Booking::formatDate($req->input('act_date'), 1);
                $detail->act_name = $req->input('act_name');
                $detail->act_num = $req->input('act_num');
                $detail->act_hotelresort = $req->input('act_hotelresort');
                $detail->notes = $req->input('notes');
                $detail->save();
            }

            $html = 'ok';

            return Response::json(['data' => ['html' => $html]]);
        }
    }

    /**
     * AJAX request: which saves order of events when arranged
     *
     * @param Request $request
     */
    public function save_order_of_events(Request $request)
    {
        if ($request->input('booking_id')) {
            $booking = Booking::find($request->input('booking_id'));
            $booking->iti_template = $request->input('iti_template');
            $booking->order_events = $request->input('order_events');
            $booking->save();
        }
        echo 'saved';
    }

    public function generate_itinerary(ItineraryWordRepository $itineraryWordRepository, $booking_id, $template_id)
    {
        return $itineraryWordRepository->generate_itinerary($template_id);
    }

    public function generate_invoice(Invoice $invoice, $booking_id, $template_id)
    {
        $invoice->generateInvoice($template_id);
    }

    public function download($type, $booking_id, $ext)
    {
        switch ($ext) {
            case "docx":
                $contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                break;

            case "pdf":
                $contentType = "application/pdf";
                break;
        }
        if ($type == '1') {
            $booking = Booking::find($booking_id);
            $filename = $booking->filename . '.' . $ext;
            $pathToFile = 'documents/results/' . $filename;
            $headers = [
                'Content-Type' => 'application/pdf',
            ];
        } elseif ($type == '2') {
            $filename = (new Invoice($booking_id))->getOutputFileName() . '.' . $ext;
            $pathToFile = 'documents/results/' . $filename;
            $headers = [
                'Content-Type' => $contentType,
            ];
        }

        return response()->download($pathToFile, $filename, $headers);

    }

    public static function safely_show($obj)
    {
        if (empty($obj)) {
            return '';
        }

        if (is_null($obj)) {
            return '';
        }

        if (!is_object($obj)) {
            return '';
        }

        return $obj;
    }

    /**
     * Create PDF file from html.
     *
     * @param $html
     * @param $name
     * @return bool
     */
    private function createPDF($html, $name)
    {
        return PDF::loadHTML($html)->setPaper('a4')->save($name);
    }

    /**
     * Create Doc file from html.
     *
     * @param FileMakerRepository $fileMakerRepository
     * @param $html
     * @param $name
     */
    private function createWord(FileMakerRepository $fileMakerRepository, $html, $name)
    {
        $fileMakerRepository->CreateDOC($html, $name);
    }
}