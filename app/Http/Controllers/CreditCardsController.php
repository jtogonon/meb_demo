<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

use Auth;
use Entrust;
use Illuminate\Support\Facades\Session;
use Response, Asset, Html;
use \App\CreditCards;
use Illuminate\Support\Facades\Validator;

class CreditCardsController extends BaseController
{
    public function check(Request $request)
    {
        $validator = Validator::make($request->all(), CreditCards::$rules);
        $data = [];

        if ($validator->fails()) {
            $data['title'] = 'Error';
            $data['success'] = 0;
            $data['text'] = $validator->errors()->all();;
            $data['type'] = 'warning';
        } else {
            $expiration = explode('/', $request->get('expiration'));
            if ($expiration[1] < date('Y')) {
                $data['title'] = 'Error';
                $data['success'] = 0;
                $data['msg'] = "Expiration year can't be from the past.";
                $data['type'] = 'warning';
            }

            if ($expiration[0] > 12) {
                $data['title'] = 'Error';
                $data['success'] = 0;
                $data['msg'] = "Invalid month.";
                $data['type'] = 'warning';
            }

            foreach ($request->all() as $key => $value) {
                Session::set($key, $value);
            }
        }

        return Response::json($data);
    }
}