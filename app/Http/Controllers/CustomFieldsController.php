<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

use Auth, Mail;
use Entrust, Illuminate\Support\Facades\Session;
use App\User, Response, Asset, Html;

use App\Booking;
use App\CustomField;

class CustomFieldsController extends BaseController
{
    public $layout = 'layout';

    public $page_title = 'Dashboard';

    public function __construct(Guard $auth)
    {
        //$this->middleware('guest');
        $this->middleware('auth');
        $this->auth = $auth;

        parent::__construct();

        $jsArray = [
            '/assets/jquery/dist/jquery.min.js',
            '/assets/jquery-ui/jquery-ui.min.js',
            '/assets/slimScroll/jquery.slimscroll.min.js',
            '/assets/bootstrap/dist/js/bootstrap.min.js',
            '/assets/metisMenu/dist/metisMenu.min.js',
            '/assets/iCheck/icheck.min.js',
            '/assets/peity/jquery.peity.min.js',
            '/assets/sweetalert/lib/sweet-alert.min.js',
            '/assets/sparkline/index.js',
            '/scripts/homer.js',

        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/fontawesome/css/font-awesome.css',
            '/assets/metisMenu/dist/metisMenu.css',
            '/assets/animate.css/animate.css',
            '/assets/bootstrap/dist/css/bootstrap.css',
            '/assets/sweetalert/lib/sweet-alert.css',
        ];
        Asset::add($cssArray, 'headerCss');

        $this->title_TopPanel = 'Bookings';

        $this->TopPanel_submenu = ['Dashboard' => '/', 'Customers' => '/customers', 'Products' => '/products/index'];
    }

    public function customfields()
    {
        $data = [];

        $data['page_title'] = 'Extra Fields';
        $data['navbartitle'] = 'Extra Fields';

        $jsArray = [
            '/scripts/admin-extrafields.js',
        ];
        Asset::add($jsArray, 'footer');

        $auth = $this->auth->user();

        return view('administrator/customfields', $data);
    }

    /**
     * AJAX request:
     *
     * @param Request $req
     * @return mixed
     */
    public function getcustomfields(Request $req)
    {
        $auth = $this->auth->user();

        $module = $req->input('choose_module');
        $tab = $req->input('choose_tab');
        $window = $req->input('choose_window');

        $fields = CustomField::getFieldsByModule($auth->operator_id, $module, $tab, $window);

        $html = '';

        if (count($fields) > 0) {
            $html .= $this->get_fields_tablelist($fields);
        }

        return Response::json(['data' => ['html' => $html]]);
    }

    /**
     * AJAX request:
     *
     * @param Request $req
     * @return mixed
     */
    public function save_customfields(Request $req)
    {
        $auth = $this->auth->user();

        $module = $req->input('choose_module');
        $tab = $req->input('choose_tab');
        $window = $req->input('choose_window');

        //save new field
        $cField = new CustomField;
        $cField->operator_id = $auth->operator_id;
        $cField->module = $req->input('choose_module');
        $cField->tab = $req->input('choose_tab');
        $cField->popupwindow = $req->input('choose_window');
        $cField->name = $req->input('field_name');
        $cField->field_type = $req->input('field_type');
        $cField->field_options = $req->input('field_options');
        $cField->default_value = $req->input('field_default');
        $cField->field_length = $req->input('field_length');
        $cField->active = 1;
        $cField->save();

        $fields = CustomField::getFieldsByModule($auth->operator_id, $module, $tab, $window);

        $html = '<p>Tab: ' . $tab . ':' . $window . '</p>';
        if (count($fields) > 0) {
            $html .= $this->get_fields_tablelist($fields);
        }

        return Response::json(['data' => ['html' => $html]]);
    }

    public function get_fields_tablelist($fields)
    {
        $html = '<table class="table table-striped">';
        $html .= '<tr><th>Name</th><th>Type</th> <th>Options</th><th></th></tr>';
        foreach ($fields as $fld){
            $html .= '<tr><td>' . $fld->name . '</td><td>' . $fld->field_type . '</td><td>' . $fld->field_options
                . '</td><td><a href="/administrator/delete_customfield/' . $fld->id . '" data="' . $fld->id
                . '" class="btn_deletecustomfld btn btn-xs btn-danger btn-circle">x</a></td></tr>';
        }
        $html .= '</table>';

        return $html;
    }
}