<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use App\User, Response, Asset, Html;
use App\ItineraryTemplate;

class DocumentsTemplateController extends BaseController
{
    public $layout = 'layout';

    public $page_title = 'Documents';

    public function __construct(Guard $auth)
    {
        //$this->middleware('guest');
        $this->middleware('auth');
        $this->auth = $auth;

        parent::__construct();

        $jsArray = [
            '/assets/jquery/dist/jquery.min.js',
            '/assets/jquery-ui/jquery-ui.min.js',
            '/assets/slimScroll/jquery.slimscroll.min.js',
            '/assets/bootstrap/dist/js/bootstrap.min.js',
            '/assets/metisMenu/dist/metisMenu.min.js',
            '/assets/iCheck/icheck.min.js',
            '/assets/peity/jquery.peity.min.js',
            '/assets/sweetalert/lib/sweet-alert.min.js',
            '/assets/sparkline/index.js',
            '/scripts/homer.js',
            '/scripts/emailtemplates.js',

        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/fontawesome/css/font-awesome.css',
            '/assets/metisMenu/dist/metisMenu.css',
            '/assets/animate.css/animate.css',
            '/assets/bootstrap/dist/css/bootstrap.css',
            '/assets/sweetalert/lib/sweet-alert.css',
        ];
        Asset::add($cssArray, 'headerCss');

        $this->title_TopPanel = 'Documents';

        $this->TopPanel_submenu = ['Dashboard' => '/', 'Customers' => '/customers', 'Products' => '/products/index'];
    }

    public function index()
    {
        $jsArray = [
            '/assets/datatables/media/js/jquery.dataTables.min.js',
            '/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js'
        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css',
        ];
        Asset::add($cssArray, 'headerCss');

        //start here
        $auth = $this->auth->user();
        $data = [
            'page_title' => $this->page_title,
            'title_TopPanel' => 'Document Templates',
            'TopPanel_button' => '<a class="btn btn-default btn-sm" href="/documenttemplate/new">Add Document Template</a>',
            'TopPanel_submenu' => $this->TopPanel_submenu
        ];

        $templates = ItineraryTemplate::getTemplatesByOperator($auth->operator_id);
        $data['templates'] = $templates;

        return view('documenttemplate.index', $data);
    }

    public function delete($id)
    {
        $hasRecord = ItineraryTemplate::find($id);
        if ($hasRecord){
            ItineraryTemplate::find($id)->delete();
        }

        return redirect('/documenttemplate/index');
    }
}