<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use App\User, Response, Asset, Html;

use App\Booking;
use App\BookingItem;
use App\Country;
use App\Email, App\EmailTemplate;

class EmailTemplatesController extends BaseController
{
    public $layout = 'layout';

    public $page_title = 'Dashboard';

    public function __construct(Guard $auth)
    {
        //$this->middleware('guest');
        $this->middleware('auth');
        $this->auth = $auth;

        parent::__construct();

        $jsArray = [
            '/assets/jquery/dist/jquery.min.js',
            '/assets/jquery-ui/jquery-ui.min.js',
            '/assets/slimScroll/jquery.slimscroll.min.js',
            '/assets/bootstrap/dist/js/bootstrap.min.js',
            '/assets/metisMenu/dist/metisMenu.min.js',
            '/assets/iCheck/icheck.min.js',
            '/assets/peity/jquery.peity.min.js',
            '/assets/sweetalert/lib/sweet-alert.min.js',
            '/assets/sparkline/index.js',
            '/scripts/homer.js',
            '/scripts/emailtemplates.js',

        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/fontawesome/css/font-awesome.css',
            '/assets/metisMenu/dist/metisMenu.css',
            '/assets/animate.css/animate.css',
            '/assets/bootstrap/dist/css/bootstrap.css',
            '/assets/sweetalert/lib/sweet-alert.css',
        ];
        Asset::add($cssArray, 'headerCss');

        $this->title_TopPanel = 'Bookings';

        $this->TopPanel_submenu = ['Dashboard' => '/', 'Customers' => '/customers', 'Products' => '/products/index'];
    }

    public function index()
    {
        $jsArray = [
            '/assets/datatables/media/js/jquery.dataTables.min.js',
            '/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js'
        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css',
        ];
        Asset::add($cssArray, 'headerCss');

        //start here
        $auth = $this->auth->user();
        $data = [
            'page_title' => $this->page_title,
            'title_TopPanel' => 'Email Templates',
            'TopPanel_button' => '<a class="btn btn-default btn-sm" href="/emailtemplates/new">Add Email Template</a>',
            'TopPanel_submenu' => $this->TopPanel_submenu
        ];

        $templates = EmailTemplate::getEmailTemplatesByOperator($auth->operator_id);
        $data['templates'] = $templates;

        return view('emailtemplates/index', $data);
    }

    /**
     * Save new template.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save(Request $request)
    {
        if ($request->input('id')){
            $etpl = EmailTemplate::find($request->input('id'));
        } else {
            $etpl = new EmailTemplate;
        }

        $etpl->operator_id = $this->auth->user()->operator_id;
        $etpl->short_name = $request->input('short_name');
        $etpl->subject = $request->input('subject');
        $etpl->html_body = $request->input('html_body');
        $etpl->text_body = $request->input('text_body');
        //$etpl->email_system_template_id = $request->input('email_system_template_id');
        $etpl->save();

        \Session::flash('flash_message', 'New Email Template has been created.');

        return redirect('emailtemplates/index');
    }

    public function view($etpl_id)
    {
        $jsArray = ['/assets/summernote/dist/summernote.min.js'];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/summernote/dist/summernote.css'];
        Asset::add($cssArray, 'headerCss');

        $template = EmailTemplate::find($etpl_id);

        $data = [
            'page_title' => $this->page_title,
            'title_TopPanel' => 'Email Templates',
        ];
        $data['emailVars'] = ['fullname', 'address', 'city', 'country'];
        $data['template'] = $template;

        return view('emailtemplates/view', $data);
    }

    public function create($supplier_id = 0)
    {
        $jsArray = ['/scripts/bookings.js', '/assets/steps/jquery.steps.min.js', '/assets/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js',
            '/assets/select2-3.5.2/select2.min.js',
            '/assets/datatables/jquery.DataTable.1.10.9.min.js',
            '/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',
            '/assets/toastr/build/toastr.min.js'];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css',
            '/assets/select2-3.5.2/select2.css',
            '/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css',
            '/assets/toastr/build/toastr.min.css'];
        Asset::add($cssArray, 'headerCss');


        $auth = $this->auth->user();
        $data = [];
        $data['emailVars'] = ['fullname', 'address', 'city', 'country'];
        $data['show_TopPanel'] = false;
        $data['page_title'] = $this->page_title;

        return view('emailtemplates/add', $data);
    }

    /**
     * AJAX request: used in popup.
     *
     * @param $booking_id
     * @return \Illuminate\View\View
     */
    public function popup($booking_id)
    {
        $jsArray = [
            '/assets/summernote/dist/summernote.min.js',
        ];
        Asset::add($jsArray, 'footer');

        $cssArray =
            ['/assets/summernote/dist/summernote.css',
                '/assets/summernote/dist/summernote-bs3.css'];
        Asset::add($cssArray, 'headerCss');

        $data['booking_id'] = $booking_id;

        return view('emailtemplates/popup', $data);
    }
}