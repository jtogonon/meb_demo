<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

use Auth, Mail;
use Entrust, Illuminate\Support\Facades\Session;
use App\User, Response, Asset, Html;

use App\Booking;
use App\BookingItem;
use App\Country;
use App\Email, App\EmailTemplate;

class EmailsController extends BaseController
{
    public $layout = 'layout';

    public $page_title = 'Dashboard';

    public function __construct(Guard $auth)
    {
        //$this->middleware('guest');
        $this->middleware('auth');
        $this->auth = $auth;

        parent::__construct();

        $jsArray = [
            '/assets/jquery/dist/jquery.min.js',
            '/assets/jquery-ui/jquery-ui.min.js',
            '/assets/slimScroll/jquery.slimscroll.min.js',
            '/assets/bootstrap/dist/js/bootstrap.min.js',
            '/assets/metisMenu/dist/metisMenu.min.js',
            '/assets/iCheck/icheck.min.js',
            '/assets/peity/jquery.peity.min.js',
            '/assets/sweetalert/lib/sweet-alert.min.js',
            '/assets/sparkline/index.js',
            '/scripts/homer.js',

        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/fontawesome/css/font-awesome.css',
            '/assets/metisMenu/dist/metisMenu.css',
            '/assets/animate.css/animate.css',
            '/assets/bootstrap/dist/css/bootstrap.css',
            '/assets/sweetalert/lib/sweet-alert.css',
        ];
        Asset::add($cssArray, 'headerCss');

        $this->title_TopPanel = 'Bookings';

        $this->TopPanel_submenu = ['Dashboard' => '/', 'Customers' => '/customers', 'Products' => '/products/index'];
    }

    /**
     * AJAX request: used in popup.
     *
     * @param $booking_id
     * @return \Illuminate\View\View
     */
    public function popup($booking_id)
    {
        $jsArray = [
            '/assets/summernote/dist/summernote.min.js',
        ];
        Asset::add($jsArray, 'footer');

        $cssArray =
            ['/assets/summernote/dist/summernote.css',
                '/assets/summernote/dist/summernote-bs3.css'];
        Asset::add($cssArray, 'headerCss');

        $auth = $this->auth->user();

        $etpls = EmailTemplate::getEmailTemplatesByOperator($auth->id);

        $templates = ['0' => '-Select Template-'];
        foreach ($etpls as $tpl) {
            $templates[$tpl->id] = $tpl->short_name;
        }

        $data['templates'] = $templates;

        $users = User::getCustomersByOperatorId($auth->id);
        $contacts = [];
        foreach ($users as $u) {
            $contacts[$u->id] = $u->first_name . ' ' . $u->last_name;
        }

        $data['email_to'] = $contacts;

        //get email settings
        $settings = \DB::table('operator_settings')
            ->select('*')
            ->where('user_id', $auth->id)
            ->get();
        if(count($settings)){
            $setting = $settings[0];
            //$data['email_from'] = $setting->email_from_name .' <'.$setting->email_from.'>';
            $data['email_from'] = $setting->email_from;
        }

        $data['booking_id'] = $booking_id;

        return view('emails/popup', $data);
    }

    public function getTemplateContent($tpl_id)
    {
        $etpl = EmailTemplate::find($tpl_id);

        $html = $etpl->html_body;
        $text = $etpl->text_body;
        $subject = $etpl->subject;

        return Response::json(['data' => ['html' => $html, 'text' => $text, 'subject' => $subject]]);
    }

    public function getContacts()
    {
        $data = [];

        return view('emails/getcontacts', $data);
    }

    public function send(Request $req)
    {
        $auth = $this->auth->user();
        $error_msg = '';
        $error_title = '';
        $error = false;

        $settings = \DB::table('operator_settings')
            ->select('*')
            ->where('operator_id', $auth->operator_id)
            ->get();
        $setting = $settings[0];

        $data['subject'] = $req->input('email_subject');
        $data['from_email'] = $req->input('email_from');
        $data['from_name'] = $setting->email_from_name;

        //$variables = $this->get_variables($data);

        $data['email_body'] = $req->input('email_body');
        $tos = $req->input('email_to');
        if (count($tos) > 0) {
            $toArray = [];
            foreach ($tos as $e => $id) {
                $email = User::getEmailsId($id);
                $toArray[$email] = $email;
            }
            $data['to_emails'] = $toArray;
        } else {
            $error = true;
            $error_title = 'Missing Recipient!';
            $error_msg = 'Please select a Recipient';
        }
        if ($data['email_body'] == '                ') {
            $error = true;
            $error_title = 'There is no Message!';
            $error_msg = 'Please write your message before sending this email.';
        }

        $status = '';

        if (!$error) { //no error, then send it!
            $status = Mail::send('email.blank', ['msg' => $data['email_body']], function ($message) use ($data) {
                $message->from($data['from_email'], $data['from_name']);
                $message->to(array_values($data['to_emails']));
                $message->replyTo($data['from_email'], $data['from_name']);
                $message->setContentType("text/html");
                $message->addPart($data['email_body'], 'text/html');
                $message->subject($data['subject']);
            });
        }

        $html = $status;

        return Response::json(['data' => ['html' => $html, 'error' => $error, 'error_title' => $error_title, 'error_msg' => $error_msg]]);
    }
}