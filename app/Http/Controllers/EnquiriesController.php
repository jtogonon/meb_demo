<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

use Auth;
use Entrust;
use Illuminate\Support\Facades\Validator;
use Response, Asset, Html;

class EnquiriesController extends BaseController
{
    public $page_title = 'Enquiries';

    public function __construct(Guard $auth)
    {
        //$this->middleware('guest');
        $this->middleware('auth');
        $this->auth = $auth;

        parent::__construct();

        $jsArray = [
            '/assets/jquery/dist/jquery.min.js',
            '/assets/jquery-ui/jquery-ui.min.js',
            '/assets/slimScroll/jquery.slimscroll.min.js',
            '/assets/bootstrap/dist/js/bootstrap.min.js',
            '/assets/metisMenu/dist/metisMenu.min.js',
            '/assets/iCheck/icheck.min.js',
            '/assets/peity/jquery.peity.min.js',
            '/assets/sweetalert/lib/sweet-alert.min.js',
            '/assets/sparkline/index.js',
            '/scripts/homer.js',
            '/scripts/inquiry.js'
        ];

        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/fontawesome/css/font-awesome.css',
            '/assets/metisMenu/dist/metisMenu.css',
            '/assets/animate.css/animate.css',
            '/assets/bootstrap/dist/css/bootstrap.css',
            '/assets/sweetalert/lib/sweet-alert.css',
            '/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css',
        ];
        Asset::add($cssArray, 'headerCss');
    }

    public function index()
    {
        $data = ['page_title' => $this->page_title, 'show_TopPanel' => false,];

        return view('enquiries.index', $data);
    }

    public function createform(Request $request)
    {
        $allRequest = $request->all();
        $data['success'] = 1;
        $data['msg'] = 'Data have been saved.';
        $i = 0;
        $len = count($allRequest);
        foreach ($allRequest as $key => $req) {
            $string = $key;

            if (str_contains($key, '_') == true) {
                $exp = explode('_', $key);
                $string = $exp[0] . ' ' . $exp[1];
            }

            if ($req == '') {
                $data['success'] = 0;
                $data['msg'] = 'The ' . $string . ' field is required.';
                break;
            } else {

                if ($string == 'name' || $string == 'city' || $string == 'country') {
                    $fields = [$string => $req];
                    $rules = ['name' => 'regex:/^[a-z][a-z ]*$/i', 'city' => 'regex:/^[a-z][a-z ]*$/i', 'country' => 'regex:/^[a-z][a-z ]*$/i'];
                    $valid = Validator::make($fields, $rules);
                    if ($valid->fails() == true) {
                        $data['success'] = 0;
                        $data['msg'] = 'The ' . $string . ' field must contain letters and spaces only.';
                        break;
                    }
                }

                if ($key == 'zip_code') {
                    $fields = [$key => $req];
                    $rules = ['zip_code' => 'numeric'];
                    $valid = Validator::make($fields, $rules);
                    if ($valid->fails() == true) {
                        $data['success'] = 0;
                        $data['msg'] = 'The ' . $string . ' field must contain numbers only.';
                        break;
                    }
                }

                if ($string == 'email') {
                    $fields = [$key => $req];
                    $rules = ['email' => 'email'];
                    $valid = Validator::make($fields, $rules);
                    if ($valid->fails() == true) {
                        $data['success'] = 0;
                        $data['msg'] = 'The ' . $string . ' field must be a valid email.';
                        break;
                    }
                }

                //check if last item in the loop
                if ($i == $len - 1) {
                    $insert = [];

                    foreach ($request->all() as $key => $data) {
                        $auth = $this->auth->user();
                        $insert['operator_id'] = $auth->operator_id;
                        $insert[$key] = $data;
                    };

                    \App\EnquiryForm::create($insert);
                }
                $i++;

            }
        }

        return Response::json($data);
    }
}