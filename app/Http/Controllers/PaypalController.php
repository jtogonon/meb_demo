<?php namespace App\Http\Controllers;

use PayPal\Rest\ApiContext;

class PaypalController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index()
    {
        return view('welcome');
    }

    public function test()
    {
        $sdkConfig = [
            "mode" => "sandbox"
        ];

        $cred = new \PayPal\Auth\OAuthTokenCredential("AQkquBDf1zctJOWGKWUEtKXm6qVhueUEMvXO_-MCI4DQQ4-LWvkDLIN2fGsd", "EL1tVxAjhT7cJimnz5-Nsx9k2reTKSVfErNQF-CmrwJgxRtylkGTKlU4RvrX");
        $apiContext = new ApiContext($cred, 'Request' . time());
        //$apiContext->setConfig($sdkConfig);

        $card = new \PayPal\Api\CreditCard();
        $card->setType("visa");
        $card->setNumber("4446283280247004");
        $card->setExpire_month("11");
        $card->setExpire_year("2018");
        $card->setFirst_name("Joe");
        $card->setLast_name("Shopper");

        $fundingInstrument = new \PayPal\Api\FundingInstrument();
        $fundingInstrument->setCredit_card($card);

        $payer = new \PayPal\Api\Payer();
        $payer->setPayment_method("credit_card");
        $payer->setFunding_instruments([$fundingInstrument]);

        $amount = new \PayPal\Api\Amount();
        $amount->setCurrency("USD");
        $amount->setTotal("12");

        $transaction = new \PayPal\Api\Transaction();
        $transaction->setAmount($amount);
        $transaction->setDescription("creating a direct payment with credit card");

        $payment = new \PayPal\Api\Payment();
        $payment->setIntent("sale");
        $payment->setPayer($payer);
        $payment->setTransactions([$transaction]);

        $payment->create($apiContext);

        var_dump($payment);

        return view('welcome');
    }

}
