<?php

namespace App\Http\Controllers\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Car;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class CarController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $car = Car::paginate(15);

        return view('car.index', compact('car'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('car.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['car_rental' => 'required', 'supplier_id' => 'required', 'product_id' => 'required', ]);

        Car::create($request->all());

        Session::flash('flash_message', 'Car added!');

        return redirect('car');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $car = Car::findOrFail($id);

        return view('car.show', compact('car'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $car = Car::findOrFail($id);

        return view('car.edit', compact('car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['car_rental' => 'required', 'supplier_id' => 'required', 'product_id' => 'required', ]);

        $car = Car::findOrFail($id);
        $car->update($request->all());

        Session::flash('flash_message', 'Car updated!');

        return redirect('car');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Car::destroy($id);

        Session::flash('flash_message', 'Car deleted!');

        return redirect('car');
    }

}
