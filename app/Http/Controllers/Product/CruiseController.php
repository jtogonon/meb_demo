<?php

namespace App\Http\Controllers\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cruise;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class CruiseController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $cruise = Cruise::paginate(15);

        return view('cruise.index', compact('cruise'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('cruise.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['cruiseline' => 'required', 'supplier_id' => 'required', 'product_id' => 'required', ]);

        Cruise::create($request->all());

        Session::flash('flash_message', 'Cruise added!');

        return redirect('cruise');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cruise = Cruise::findOrFail($id);

        return view('cruise.show', compact('cruise'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cruise = Cruise::findOrFail($id);

        return view('cruise.edit', compact('cruise'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['cruiseline' => 'required', 'supplier_id' => 'required', 'product_id' => 'required', ]);

        $cruise = Cruise::findOrFail($id);
        $cruise->update($request->all());

        Session::flash('flash_message', 'Cruise updated!');

        return redirect('cruise');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Cruise::destroy($id);

        Session::flash('flash_message', 'Cruise deleted!');

        return redirect('cruise');
    }

}
