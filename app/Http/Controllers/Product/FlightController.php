<?php

namespace App\Http\Controllers\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Flight;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class FlightController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $flight = Flight::paginate(15);

        return view('flight.index', compact('flight'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('flight.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['airline' => 'required', 'supplier_id' => 'required', 'product_id' => 'required', ]);

        Flight::create($request->all());

        Session::flash('flash_message', 'Flight added!');

        return redirect('flight');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $flight = Flight::findOrFail($id);

        return view('flight.show', compact('flight'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $flight = Flight::findOrFail($id);

        return view('flight.edit', compact('flight'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['airline' => 'required', 'supplier_id' => 'required', 'product_id' => 'required', ]);

        $flight = Flight::findOrFail($id);
        $flight->update($request->all());

        Session::flash('flash_message', 'Flight updated!');

        return redirect('flight');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Flight::destroy($id);

        Session::flash('flash_message', 'Flight deleted!');

        return redirect('flight');
    }

}
