<?php

namespace App\Http\Controllers\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Hotel;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class HotelController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $hotel = Hotel::paginate(15);

        return view('hotel.index', compact('hotel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('hotel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'room_category' => 'required', 'supplier_id' => 'required', 'product_id' => 'required', ]);

        Hotel::create($request->all());

        Session::flash('flash_message', 'Hotel added!');

        return redirect('hotel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $hotel = Hotel::findOrFail($id);

        return view('hotel.show', compact('hotel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $hotel = Hotel::findOrFail($id);

        return view('hotel.edit', compact('hotel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', 'room_category' => 'required', 'supplier_id' => 'required', 'product_id' => 'required', ]);

        $hotel = Hotel::findOrFail($id);
        $hotel->update($request->all());

        Session::flash('flash_message', 'Hotel updated!');

        return redirect('hotel');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Hotel::destroy($id);

        Session::flash('flash_message', 'Hotel deleted!');

        return redirect('hotel');
    }

}
