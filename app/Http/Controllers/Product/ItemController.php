<?php

namespace App\Http\Controllers\Product;

use App\BookingItem;
use App\VehicleClass;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created item in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Check category of item and calls according method.
     *
     * @param BookingItem $item
     * @return \Illuminate\Http\Response
     */
    public function show(BookingItem $item)
    {
        switch ($item->category_id) {
            case 1: //Hotel
                $module = 'hotel';
                break;
            case 2: //Vacation Package
                $module = 'vacation';
                break;
            case 3: //Activities
                $module = 'activity';
                break;
            case 4: //Cars
                $module = 'car';
                $data['vehicleClass'] = VehicleClass::pluck('name', 'id');
                $data['ageArray'] = [
                    '17-20' => '17-20 Yrs Old',
                    '21-30' => '21-30 Yrs Old',
                    '31-50' => '31-50 Yrs Old'
                ];
                break;
            case 5: //Flight
                $module = 'flight';
                break;
            case 6: //Cruises
                $module = 'cruise';
                break;
            default:
                return false;
        }

        $fields = Auth::user()->customFields->where('module', $module);

        $custom_fields = [];
        foreach ($fields as $field) {
            $custom_fields[$field->id] = [
                'name' => 'customfld_' . $field->id,
                'label' => $field->name,
                'type' => $field->field_type,
                'options' => $field->field_options,
                'value' => $field->default_value
            ];
        }

        $data['item'] = $item;
        $data['custom_fields'] = $custom_fields;
        $data['array_of_numbers'] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

        return view('product._details_' . $module, $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->ajax()) {
            $item = BookingItemDetail::checkBookingIdAndProductId($_POST['booking_id'], $_POST['product_id'], $_POST['booking_item_id']);
            if (!$item) {
                //save new item details
                $detail = new BookingItemDetail;
            } else {
                $detail = BookingItemDetail::find($item[0]->id);
            }
            $detail->user_id = Auth::user()->id;
            $detail->booking_id = $_POST['booking_id'];
            $detail->booking_item_id = $_POST['booking_item_id'];
            $detail->product_id = $_POST['product_id'];
            $detail->type = $request->input('bookingtype');

            if ($request->input('bookingtype') == 'accomodation') {
                $detail->acco_room_category = $request->input('room_category_id');
                $detail->acco_hotel = $request->input('acco_hotel');
                $detail->acco_checkin = Booking::formatDate($request->input('acco_checkin'), 1);
                $detail->acco_checkout = Booking::formatDate($request->input('acco_checkout'), 1);
                $detail->acco_rooms = $request->input('acco_rooms');
                $detail->acco_num_adults = $request->input('acco_num_adults');
                $detail->acco_num_children = $request->input('acco_num_children');
                $detail->acco_airline = $request->input('acco_airline');
                $detail->acco_flight = $request->input('acco_flight');
                $detail->acco_depart = Booking::formatDate($request->input('acco_depart'), 1);
                $detail->acco_arrival = Booking::formatDate($request->input('acco_arrival'), 1);
                $detail->acco_origin = $request->input('acco_origin');
                $detail->acco_destination = $request->input('acco_destination');
                $detail->notes = $request->input('notes');
                $detail->save();
            }

            if ($request->input('bookingtype') == 'flight') {
                $detail->flight_from_city = $request->input('flight_from_city');
                $detail->flight_from_date = Booking::formatDate($request->input('flight_from_date'), 1);
                $detail->flight_to_city = $request->input('flight_to_city');
                $detail->flight_to_date = Booking::formatDate($request->input('flight_to_date'), 1);
                $detail->flight_num_passengers = $request->input('flight_num_passengers');
                $detail->notes = $request->input('notes');
                $detail->save();
            }

            if ($request->input('bookingtype') == 'carrental') {
                $detail->carrent_location_pickup = $request->input('carrent_location_pickup');
                $detail->carrent_date_pickup = Booking::formatDate($request->input('carrent_date_pickup'), 1);
                $detail->carrent_time_pickup = $request->input('carrent_time_pickup');
                $detail->carrent_location_return = $request->input('carrent_location_return');
                $detail->carrent_date_return = Booking::formatDate($request->input('carrent_date_return'), 1);
                $detail->carrent_time_return = $request->input('carrent_time_return');
                $detail->carrent_vehicleclass = $request->input('carrent_vehicleclass');
                $detail->carrent_age = $request->input('carrent_age');
                $detail->notes = $request->input('notes');
                $detail->save();
            }

            if ($request->input('bookingtype') == 'activity') {
                $detail->act_activityname = $request->input('act_activityname');
                $detail->act_date = Booking::formatDate($request->input('act_date'), 1);
                $detail->act_name = $request->input('act_name');
                $detail->act_num = $request->input('act_num');
                $detail->act_hotelresort = $request->input('act_hotelresort');
                $detail->notes = $request->input('notes');
                $detail->save();
            }

            $html = 'ok';

            return Response::json(['data' => ['html' => $html]]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
