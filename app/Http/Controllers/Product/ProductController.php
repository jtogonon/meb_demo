<?php

namespace App\Http\Controllers\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\ProductRequest;
use App\Product;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the product.
     *
     * @return Response
     */
    public function index()
    {
        $products = Auth::user()->products;

        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new product.
     *
     * @return Response
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created product in storage.
     *
     * @param ProductRequest $request
     * @return Response
     */
    public function store(ProductRequest $request)
    {
        Product::create($request->all());

        Session::flash('flash_message', 'Product added!');

        return redirect('product');
    }

    /**
     * Display the specified product.
     *
     * @param Product $product
     * @return Response
     * @internal param int $id
     *
     */
    public function show(Product $product)
    {
        return view('product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified product.
     *
     * @param Product $product
     * @return Response
     * @internal param int $id
     *
     */
    public function edit(Product $product)
    {
        return view('product.edit', compact('product'));
    }

    /**
     * Update the specified product in storage.
     *
     * @param Product $product
     * @param ProductRequest $request
     * @return Response
     * @internal param int $id
     *
     */
    public function update(Product $product, ProductRequest $request)
    {
        $product->update($request->all());

        Session::flash('flash_message', 'Product updated!');

        return redirect('product');
    }

    /**
     * Remove the specified product from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Product::destroy($id);

        return Response::json(['data' => ['html' => 'Product deleted!']]);
    }

    /*---------------------AJAX------------------------*/

    /**
     * AJAX request: used in Booking -> new.
     *
     * @return mixed
     */
    public function get_all_products()
    {
        $data['products'] = Auth::user()->products;
        $html = view('products/all_products')->with($data)->render();

        return Response::json(['data' => ['html' => $html, 'redirecturl' => '/']]);
    }

}
