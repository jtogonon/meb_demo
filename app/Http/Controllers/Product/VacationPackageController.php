<?php

namespace App\Http\Controllers\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\VacationPackage;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class VacationPackageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $vacationpackage = VacationPackage::paginate(15);

        return view('vacationpackage.index', compact('vacationpackage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('vacationpackage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['supplier_id' => 'required', 'product_id' => 'required', ]);

        VacationPackage::create($request->all());

        Session::flash('flash_message', 'VacationPackage added!');

        return redirect('vacationpackage');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $vacationpackage = VacationPackage::findOrFail($id);

        return view('vacationpackage.show', compact('vacationpackage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $vacationpackage = VacationPackage::findOrFail($id);

        return view('vacationpackage.edit', compact('vacationpackage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['supplier_id' => 'required', 'product_id' => 'required', ]);

        $vacationpackage = VacationPackage::findOrFail($id);
        $vacationpackage->update($request->all());

        Session::flash('flash_message', 'VacationPackage updated!');

        return redirect('vacationpackage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        VacationPackage::destroy($id);

        Session::flash('flash_message', 'VacationPackage deleted!');

        return redirect('vacationpackage');
    }

}
