<?php namespace App\Http\Controllers;

use Auth;
use Entrust;
use Response, Asset, Html;

use App\Booking;
use App\Product, App\BookingItemDetail;
use App\Country, App\Agent, App\CarClass, App\CustomField;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class ProductsController extends BaseController
{
    public $layout = 'layout';

    public $page_title = 'Products';

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;

        parent::__construct();

        $jsArray = [
            '/assets/jquery/dist/jquery.min.js',
            '/assets/jquery-ui/jquery-ui.min.js',
            '/assets/slimScroll/jquery.slimscroll.min.js',
            '/assets/bootstrap/dist/js/bootstrap.min.js',
            '/assets/metisMenu/dist/metisMenu.min.js',
            '/assets/iCheck/icheck.min.js',
            '/assets/peity/jquery.peity.min.js',
            '/assets/sweetalert/lib/sweet-alert.min.js',
            '/assets/sparkline/index.js',
            '/scripts/homer.js',

        ];
        Asset::add($jsArray, 'footer');

        $cssArray = [
            '/assets/fontawesome/css/font-awesome.css',
            '/assets/metisMenu/dist/metisMenu.css',
            '/assets/animate.css/animate.css',
            '/assets/bootstrap/dist/css/bootstrap.css',
            '/assets/sweetalert/lib/sweet-alert.css',
        ];
        Asset::add($cssArray, 'headerCss');

        $this->title_TopPanel = 'Bookings';

        $this->TopPanel_submenu = ['Dashboard' => '/', 'Customers' => '/customers', 'Products' => '/products/index'];
    }

    public function index()
    {
        $jsArray = [
            '/assets/datatables/media/js/jquery.dataTables.min.js',
            '/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js'
        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css',
        ];
        Asset::add($cssArray, 'headerCss');


        //start here
        $auth = $this->auth->user();
        $data = [
            'page_title' => $this->page_title,
            'title_TopPanel' => 'Products',
            'TopPanel_button' => '<a class="btn btn-default btn-sm" href="/products/new">Add Product</a>',
            'TopPanel_submenu' => $this->TopPanel_submenu
        ];

        $products = Product::where('operator_id', $auth->operator_id)->get();
        $data['products'] = $products;

        return view('products/index', $data);
    }

    public function create()
    {
        $data = [];

        return view('products/create', $data);
    }

    public function save(Request $request)
    {
        if ($request->isMethod('post')) {
            $prod = new Product;
            $prod->operator_id = $this->auth->user()->operator_id;
            $prod->product_name = $request->input('product_name');
            $prod->product_code = $request->input('product_code');
            $prod->supplier_ids = '';
            $prod->product_url = '';
            $prod->tour_country_ids = '';
            $prod->postal_address = $request->input('postal_address');
            $prod->tour_type = $request->input('tour_type');
            $prod->tour_leader = $request->input('tour_leader');
            $prod->grade = $request->input('grade');
            $prod->accomodation = $request->input('accomodation');
            $prod->suitable_for = json_encode($request->input('suitable_for'));
            $prod->language_ids = '';
            $prod->save();
        }

        \Session::flash('flash_message', 'New product has been created.');

        return redirect('products/index');
    }

    /**
     * AJAX request: used in Booking -> new.
     *
     * @return mixed
     */
    public function get_all_products()
    {
        $auth = $this->auth->user();

        //get all products of this operator
        $data['products'] = Product::getProductsByOperatorId($auth->operator_id);
        $html = view('products/all_products')->with($data)->render();
        return Response::json(['data' => ['html' => $html, 'redirecturl' => '/']]);
    }

    /**
     * AJAX request:
     *
     * @param $product_id
     * @param $booking_id
     * @param $booking_item_id
     * @return \Illuminate\View\View
     */
    public function details($product_id, $booking_id, $booking_item_id)
    {
        $auth = $this->auth->user();

        $data['product_id'] = $product_id;

        //check product category to use view details
        $product = Product::find($product_id);
        $data['product'] = $product;
        $data['booking_id'] = $booking_id;
        $data['booking_item_id'] = $booking_item_id;
        $item = BookingItemDetail::checkBookingIdAndProductId($booking_id, $product_id, $booking_item_id);
        if ($item) {
            $detail = BookingItemDetail::find($item[0]->id);

            //accomodation
            $detail->acco_checkin = Booking::formatDate($detail->acco_checkin, 2);
            $detail->acco_checkout = Booking::formatDate($detail->acco_checkout, 2);
            $detail->acco_depart = Booking::formatDate($detail->acco_depart, 2);
            $detail->acco_arrival = Booking::formatDate($detail->acco_arrival, 2);

            //flight
            $detail->flight_from_date = Booking::formatDate($detail->flight_from_date, 2);
            $detail->flight_to_date = Booking::formatDate($detail->flight_to_date, 2);

            //carrental
            $detail->carrent_date_pickup = Booking::formatDate($detail->carrent_date_pickup, 2);
            $detail->carrent_date_return = Booking::formatDate($detail->carrent_date_return, 2);

            //activity
            $detail->act_date = Booking::formatDate($detail->act_date, 2);

            //cruise

            $data['detail'] = $detail;
        } else {
            //no entry yet in booking_item_details
            //pull data from product tables
        }
        $cat = $product->category->id;
        switch ($product->category->id) {
            case 1:
                $module = 'accomodation';
                break;
            case 2:
                $module = 'vacation';
                break;
            case 3:
                $module = 'activity';
                break;
            case 4:
                $module = 'carrental';
                break;
            case 5:
                $module = 'flight';
                break;
            case 6:
                $module = 'cruise';
                break;

        }

        $fields = CustomField::getFieldsByOperatorId($auth->operator_id, $module);
        $custom_fields = [];
        foreach ($fields as $fld) {
            $custom_fields[$fld->id] = [
                'name' => 'customfld_' . $fld->id,
                'label' => $fld->name,
                'type' => $fld->field_type,
                'options' => $fld->field_options,
                'value' => $fld->default_value
            ];
        }

        $data['custom_fields'] = $custom_fields;

        switch ($product->category_id) {
            case 1: //Accomodations
                $view = 'details_accomodation';
                $data['roomsArr'] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                $data['adultArr'] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                $data['childrenArr'] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                break;
            case 2: //Destinations
                $view = 'details_vacation';
                break;
            case 3: //Activities
                $view = 'details_activity';
                break;
            case 4: //Cars
                $view = 'details_car';
                $data['vehicleClass'] = CarClass::lists('class_name', 'id');
                $data['ageArray'] = ['17-20' => '17-20 Yrs Old', '21-30' => '21-30 Yrs Old', '31-50' => '31-50 Yrs Old'];
                break;
            case 5: //Flight
            default:
                $view = 'details_flight';
                break;
            case 6: //Cruises
                $view = 'details_cruise';
                $data['roomsArr'] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                break;
        }

        return view('products/_' . $view, $data);
    }

    public function delete($product_id)
    {
        $product = Product::find($product_id);
        $product->delete();
        $html = 'ok';

        return Response::json(['data' => ['html' => $html]]);
    }
}