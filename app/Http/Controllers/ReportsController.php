<?php namespace App\Http\Controllers;

use Auth;
use Entrust;
use Asset, Html, Response;


use App\Supplier;
use App\Product;
use App\Country, App\Agent;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class ReportsController extends BaseController
{
    public $layout = 'layout';

    public $page_title = 'Products';

    public function __construct(Guard $auth)
    {
        //$this->middleware('guest');
        $this->auth = $auth;

        parent::__construct();

        $jsArray = [
            '/assets/jquery/dist/jquery.min.js',
            '/assets/jquery-ui/jquery-ui.min.js',
            '/assets/slimScroll/jquery.slimscroll.min.js',
            '/assets/bootstrap/dist/js/bootstrap.min.js',
            '/assets/metisMenu/dist/metisMenu.min.js',
            '/assets/iCheck/icheck.min.js',
            '/assets/peity/jquery.peity.min.js',
            '/assets/sweetalert/lib/sweet-alert.min.js',
            '/assets/sparkline/index.js',
            '/scripts/homer.js',

        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/fontawesome/css/font-awesome.css',
            '/assets/metisMenu/dist/metisMenu.css',
            '/assets/animate.css/animate.css',
            '/assets/bootstrap/dist/css/bootstrap.css',
            '/assets/sweetalert/lib/sweet-alert.css',
        ];
        Asset::add($cssArray, 'headerCss');

        $this->title_TopPanel = 'Bookings';

        $this->TopPanel_submenu = ['Dashboard' => '/', 'Customers' => '/customers', 'Products' => '/products/index'];

        view()->share('TopPanel_button', '<a class="btn btn-default btn-sm" href="/suppliers/new">Add Supplier</a>');


    }

    public function index($product_type = null)
    {
        $data = [];
        if (!Entrust::hasRole('SuperAdmin')) {
            return redirect('home');
        }

        $jsArray = [
            '/assets/datatables/media/js/jquery.dataTables.min.js',
            '/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js'
        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css',
        ];
        Asset::add($cssArray, 'headerCss');

        //start here
        $auth = $this->auth->user();
        $data = [
            'page_title' => $this->page_title,
            'title_TopPanel' => 'Suppliers',
            'TopPanel_submenu' => $this->TopPanel_submenu
        ];

        return view('reports/index', $data);
    }
}