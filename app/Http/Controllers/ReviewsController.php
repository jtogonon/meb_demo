<?php namespace App\Http\Controllers;

use Auth;
use Entrust;
use Asset, Html, Response;

class ReviewsController extends BaseController
{
    public function index()
    {
        if (!Entrust::hasRole('SuperAdmin')) {
            return redirect('home');
        }

        $jsArray = [
            '/assets/jquery-flot/jquery.flot.js',
            '/assets/jquery-flot/jquery.flot.resize.js',
            '/assets/jquery-flot/jquery.flot.pie.js',
            '/assets/flot.curvedlines/curvedLines.js',
            '/assets/jquery.flot.spline/index.js',
        ];
        Asset::add($jsArray, 'footer');

        $data = ['page_title' => $this->page_title,
            'show_TopPanel' => false,
        ];

        return view('reviews.index', $data);
    }
}