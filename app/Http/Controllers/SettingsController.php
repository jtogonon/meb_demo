<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use App\User, Response, Asset, Html;

use App\CustomField;
use App\Email, App\OperatorSetting;

class SettingsController extends BaseController
{
    public $layout = 'layout';

    public $page_title = 'Dashboard';

    public function __construct(Guard $auth)
    {
        //$this->middleware('guest');
        $this->middleware('auth');
        $this->auth = $auth;

        parent::__construct();

        $jsArray = [
            '/assets/jquery/dist/jquery.min.js',
            '/assets/jquery-ui/jquery-ui.min.js',
            '/assets/slimScroll/jquery.slimscroll.min.js',
            '/assets/bootstrap/dist/js/bootstrap.min.js',
            '/assets/metisMenu/dist/metisMenu.min.js',
            '/assets/iCheck/icheck.min.js',
            '/assets/peity/jquery.peity.min.js',
            '/assets/sweetalert/lib/sweet-alert.min.js',
            '/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',
            '/assets/datatables/jquery.DataTable.1.10.9.min.js',
            '/assets/toastr/build/toastr.min.js',
            '/assets/sparkline/index.js',

            '/scripts/homer.js',
        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/fontawesome/css/font-awesome.css',
            '/assets/metisMenu/dist/metisMenu.css',
            '/assets/animate.css/animate.css',
            '/assets/bootstrap/dist/css/bootstrap.css',
            '/assets/sweetalert/lib/sweet-alert.css',
            '/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css',
        ];
        Asset::add($cssArray, 'headerCss');

        $this->title_TopPanel = 'Settings';

        $this->TopPanel_submenu = ['Dashboard' => '/', 'Customers' => '/customers', 'Products' => '/products/index'];
    }

    public function customfields()
    {
        $data = [];
        $jsArray = ['/scripts/settings_customfields.js'];
        Asset::add($jsArray, 'footer');

        $auth = $this->auth->user();

        $settings = \DB::table('operator_settings')
            ->select('*')
            ->where('operator_id', $auth->operator_id)
            ->get();

        $data['show_TopPanel'] = false;
        $data['page_title'] = $this->page_title;

        $data['setting'] = $settings[0];

        $data['field_types'] = [
            'text' => 'Text',
            'select' => 'Select',
            'textarea' => 'TextArea',
            'checkbox' => 'Checkbox'
        ];

        return view('settings/customfields', $data);
    }

    public function getcustomfields($module)
    {
        $auth = $this->auth->user();

        $data = [];
        $data['module'] = $module;
        $data['customFields'] = CustomField::getFieldsByOperatorId($auth->operator_id, $module);

        return view('settings/_customfields_table', $data);
    }

    public function saveCustomFields(Request $req)
    {
        $auth = $this->auth->user();
        if ($req->ajax()){
            if ($req->input('id')){
                $cField = CustomField::find($req->input('id'));
            } else {
                $cField = new CustomField;
            }

            $cField->operator_id = $auth->operator_id;
            $cField->module = $req->input('module');
            $cField->name = $req->input('name');
            $cField->field_type = $req->input('field_type');
            $cField->field_options = $req->input('field_options');
            $cField->default_value = $req->input('default_value');
            $cField->active = $req->input('active');
            $cField->save();
        }
        $html = 'ok';

        return Response::json(['data' => ['html' => $html]]);
    }

    public function deleteCustomFields($id)
    {
        $cField = CustomField::find($id);
        $cField->delete();
        $html = $id;

        return Response::json(['data' => ['html' => $html]]);
    }

    public function emails()
    {
        $auth = $this->auth->user();

        $settings = \DB::table('operator_settings')
            ->select('*')
            ->where('operator_id', $auth->operator_id)
            ->get();

        $data['show_TopPanel'] = false;
        $data['page_title'] = $this->page_title;


        $data['setting'] = $settings[0];

        return view('settings/emails', $data);
    }

    public function save(Request $request)
    {
        if ($request->input('id')){
            $setting = OperatorSetting::find($request->input('id'));
            $setting->email_from = $request->input('email_from');
            $setting->email_from_name = $request->input('email_from_name');
            $setting->save();
        }

        \Session::flash('flash_message', 'Settings changes have been created.');

        return redirect('settings/emails');
    }
}