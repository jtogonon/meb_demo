<?php

namespace App\Http\Controllers;

use App\Booking;
use App\CarClass;
use App\Country;
use App\CustomField;
use App\Product;
use App\ProductFee;
use App\ProductItinerarySection;
use App\ProductType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AjaxSupplierController extends Controller
{
    public function getSupplierProducts($category_id, $supplier_id)
    {
        $data = [];

        if ($category_id == 0) {
            $category_id = null;
        }
        if ($supplier_id == 0) {
            $supplier_id = null;
        }
        $data['supplier_id'] = $supplier_id;

        switch ($category_id) {
            case '1':
                $productCategory = 'hotels';
                break;
            case '2':
                $productCategory = 'vacation';
                break;
            case '3':
                $productCategory = 'activity';
                break;
            case '4':
                $productCategory = 'carrental';
                break;
            case '5':
                $productCategory = 'flight';
                break;
            case '6':
                $productCategory = 'cruise';
                break;
        }

//        $data['products'] = Product::getProducts($auth->operator_id, $category_id);
        $data['products'] = Auth::user()->products->all();

        dd($data['products']);
        $html = view('suppliers/_search_products_' . $productCategory)->with($data)->render();

        return Response::json(['data' => ['html' => $html, 'redirecturl' => '/']]);
    }

    public function add_product($category_id, $supplier_id)
    {
        $data = [];
        $auth = $this->auth->user();
        if ($category_id == 0) {
            $category_id = null;
        }

        $data['product_id'] = 0;

        switch ($category_id) {
            case '1':
                $productCat = 'accomodation';
                $window = 'product-accomodation';
                break;
            case '2':
                $productCat = 'vacation';
                $window = 'product-vacation';
                break;
            case '3':
                $productCat = 'activity';
                $window = 'product-activity';
                break;
            case '4':
                $productCat = 'carrental';
                $window = 'product-carrental';

                break;
            case '5':
                $productCat = 'flight';
                $window = 'product-flight';
                break;
            case '6':
                $productCat = 'cruise';
                $window = 'product-cruise';
                break;
        }

        //get countries
        $data['countries'] = Country::lists('name', 'id');

        //get countries
        $data['carclass'] = CarClass::lists('class_name', 'id');

        //get countries
        $data['productypes'] = ProductType::lists('type_name', 'id');
        $data['supplier_id'] = $supplier_id;

        $data['customfields'] = CustomField::getFieldsByModule($auth->operator_id, 'suppliers', '', $window);

        return view('suppliers/_product_' . $productCat, $data);
    }

    public function save_product_details(Request $req)
    {
        $auth = $this->auth->user();

        if ($req->input('product_id')) {
            $product = Product::find($req->input('product_id'));
            $product->operator_id = $auth->operator_id;
        } else {
            $product = new Product;
            $product->operator_id = $auth->operator_id;
        }

        if ($req->input('category_id') == '1') { //accomodation
            $product->product_name = $req->input('product_name');
            $product->product_code = $req->input('product_code');
        }

        if ($req->input('category_id') == '2') { //vacation
            $product->product_type_id = $req->input('product_type_id');
            $product->product_name = $req->input('product_name');
        }

        if ($req->input('category_id') == '3') { //activity
            $product->product_name = $req->input('product_name');
        }

        if ($req->input('category_id') == '4') { //carrental
            $product->product_name = $req->input('product_name');
            $product->car_class = $req->input('car_class');
        }

        if ($req->input('category_id') == '5') { //flight
            $product->flight_departing = Booking::formatDate($req->input('flight_departing'), 1);
            $product->flight_arriving = Booking::formatDate($req->input('flight_arriving'), 1);
            $product->product_name = $req->input('product_name');
        }

        if ($req->input('category_id') == '6') { //cruise
            $product->product_name = $req->input('product_name');
        }

        $product->description = $req->input('description');
        $product->commision = $req->input('commision');

        $product->country_id = $req->input('country_id');
        $product->category_id = $req->input('category_id');
        $product->supplier_id = $req->input('supplier_id');
        $product->description = $req->input('description');

        $product->save();

        //save product fees
        $fees_json = $req->input('fee');

        //exit();
        $exists = ProductFee::checkProduct($product->id, $auth->operator_id);
        //$html = dd($fees_json);

        if (count($exists) > 0) {
            //update
        } else {
            //insert
            $productFee = new ProductFee;
            $productFee->operator_id = $auth->operator_id;
            $productFee->product_id = $product->id;
            $productFee->fees = json_encode($fees_json);
            $productFee->save();
        }

        //save itinerary section
        $sectionNum = $req->input('section_select');
        $content = $req->input('section_content');
        $sectionSaved = ProductItinerarySection::saveSectionContent($auth->operator_id, $product->id, $sectionNum, $content);

        $html = 'ok';

        return Response::json(['data' => ['html' => $html]]);
    }

    /**
     * AJAX request: which displays product details for editing a single product.
     *
     * @param $product_id
     * @param $category_id
     * @return \Illuminate\View\View
     */
    public function get_product_details($product_id, $category_id)
    {
        $auth = $this->auth->user();

        $data['product_id'] = $product_id;
        $product = Product::find($product_id);
        $productfees = ProductFee::getFeesByProductId($product->id);

        switch ($category_id) {
            case '1':
                $productCat = 'accomodation';
                $window = 'product-accomodation';
                break;
            case '2':
                $productCat = 'vacation';
                $window = 'product-vacation';
                break;
            case '3':
                $productCat = 'activity';
                $window = 'product-activity';
                break;
            case '4':
                $productCat = 'carrental';
                $window = 'product-carrental';
                break;
            case '5':
                $productCat = 'flight';
                $window = 'product-flight';

                $product->flight_departing = Booking::formatDate($product->flight_departing, 2);
                $product->flight_arriving = Booking::formatDate($product->flight_arriving, 2);
                break;
            case '6':
                $productCat = 'cruise';
                $window = 'product-cruise';
                break;
        }

        $data['product'] = $product;
        $data['productfees'] = $productfees;
        $data['supplier_id'] = $product->supplier_id;

        //get countries
        $data['countries'] = Country::lists('name', 'id');

        //get countries
        $data['carclass'] = CarClass::lists('class_name', 'id');

        //get countries
        $data['productypes'] = ProductType::lists('type_name', 'id');
        //$data['supplier_id'] = $supplier_id;

        $data['customfields'] = CustomField::getFieldsByModule($auth->operator_id, 'suppliers', '', $window);

        $data['itinerary_sections'] = [
            1 => 'Section 1',
            2 => 'Section 2',
            3 => 'Section 3',
            4 => 'Section 4',
            5 => 'Section 5'
        ];

        //by default show section 1 of itinerary
        $section1 = ProductItinerarySection::getSection1($auth->operator_id, $product_id);

        $data['section1'] = ($section1 != false) ? $section1->content : '';

        return view('suppliers/_product_' . $productCat, $data);
    }

    public function get_section_content($product_id, $section)
    {
        $auth = $this->auth->user();

        $content = ProductItinerarySection::getSectionContent($auth->operator_id, $product_id, $section);
        return Response::json(['data' => ['content' => $content]]);
    }

    public function testpaypal()
    {

    }
}
