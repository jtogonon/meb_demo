<?php

namespace App\Http\Controllers\Supplier;

use App\Country;
use App\CustomField;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\SupplierRequest;
use App\ItinerarySection;
use App\Product;
use App\ProductFee;
use App\ProductType;
use App\Supplier;
use App\VehicleClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Session;
use Entrust;

class SupplierController extends Controller
{
    /**
     * Display a listing of the suppliers.
     *
     * @param null $product_type
     * @return Response
     */
    public function index($product_type = null)
    {
        if (!Entrust::hasRole('SuperAdmin')) { //@TODO create middleware @Jakob
            return redirect('home');
        }
        if ($product_type) {
            $suppliers = Supplier::join('supplier_product_types', 'suppliers.id', '=', 'supplier_product_types.supplier_id')
                ->where('supplier_product_types.product_type_id', $product_type)
                ->where('suppliers.user_id', Auth::user()->id)
                ->get();
        } else {
            $suppliers = Auth::user()->suppliers;
        }

        return view('supplier.index', compact('product_type', 'suppliers'));
    }

    /**
     * Show the form for creating a new supplier.
     *
     * @return Response
     */
    public function create()
    {
        $countries = Country::pluck('name', 'id'); //@TODO add Cities and States @Jakob

        return view('supplier.create', compact('countries'));
    }

    /**
     * Store a newly created supplier in storage.
     *
     * @param SupplierRequest $request
     * @return Response
     * @internal param $SupplierRequest
     */
    public function store(SupplierRequest $request)
    {
        $this->createSupplier($request);

        Session::flash('flash_message', 'Supplier added!');

        return redirect('supplier');
    }

    /**
     * Display the specified supplier.
     *
     * @param Supplier $supplier
     * @return Response
     * @internal param int $id
     *
     */
    public function show(Supplier $supplier)
    {
        return view('supplier.show', compact('supplier'));
    }

    /**
     * Show the form for editing the specified supplier.
     *
     * @param Supplier $supplier
     * @return Response
     * @internal param int $id
     *
     */
    public function edit(Supplier $supplier)
    {
        $countries = Country::pluck('name', 'id');
        $products = $supplier->products->all();

        return view('supplier.edit', compact('supplier', 'products', 'countries'));
    }

    /**
     * Update the specified supplier in storage.
     *
     * @param Supplier $supplier
     * @param SupplierRequest $request
     * @return Response
     * @internal param int $id
     */
    public function update(Supplier $supplier, SupplierRequest $request)
    {
        dd($request);

        $supplier->update($request->all());

        Session::flash('flash_message', 'Supplier updated!');

        return redirect('supplier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Supplier::destroy($id);

        Session::flash('flash_message', 'Supplier deleted!');

        return redirect('supplier');
    }

    /**
     * Save a new supplier.
     *
     * @param SupplierRequest $request
     * @return mixed
     */
    public function createSupplier(SupplierRequest $request)
    {
        $supplier = Auth::user()->suppliers()->create($request->all());

        return $supplier;
    }

    /**
     * AJAX request for getting all users products.
     *
     * @param $category_id
     * @param $supplier_id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function getSupplierProducts($category_id, $supplier_id)
    {
        $data = [];

        if ($category_id == 0) {
            $category_id = null;
        }

        if ($supplier_id == 0) {
            $supplier_id = null;
        }

        switch ($category_id) {
            case '1':
                $productCategory = 'hotel';
                break;
            case '2':
                $productCategory = 'vacation';
                break;
            case '3':
                $productCategory = 'activity';
                break;
            case '4':
                $productCategory = 'car_rental';
                break;
            case '5':
                $productCategory = 'flight';
                break;
            case '6':
                $productCategory = 'cruise';
                break;
            default:
                return false;
        }

        $data['supplier_id'] = $supplier_id;
        $data['products'] = Product::getProductsByCategory($category_id);
        $html = view('supplier._search_products_' . $productCategory)->with($data)->render();

        return Response::json(['data' => ['html' => $html, 'redirecturl' => '/']]);
    }

    public function createProduct($category_id, $supplier_id)
    {
        if ($category_id == 0) {
            $category_id = null;
        }

        $data['product_id'] = 0;

        switch ($category_id) { //@TODO extract to private method @Jakob
            case '1':
                $productCategory = 'hotel';
                $window = 'product-hotel';
                break;
            case '2':
                $productCategory = 'vacation';
                $window = 'product-vacation';
                break;
            case '3':
                $productCategory = 'activity';
                $window = 'product-activity';
                break;
            case '4':
                $productCategory = 'car';
                $window = 'product-car';

                break;
            case '5':
                $productCategory = 'flight';
                $window = 'product-flight';
                break;
            case '6':
                $productCategory = 'cruise';
                $window = 'product-cruise';
                break;
            default:
                return false;
        }

        $data['countries'] = Country::pluck('name', 'id');
        $data['vehicle_classes'] = VehicleClass::pluck('name', 'id');
        $data['product_types'] = ProductType::pluck('name', 'id');
        $data['supplier_id'] = $supplier_id;
        $data['itinerary_sections'] = [
            1 => 'section1',
            2 => 'section2',
            3 => 'section3',
            4 => 'section4',
            5 => 'section5',
            6 => 'section6'
        ];

        $data['section1'] = '';

        $data['custom_fields'] = Auth::user()
            ->customFields()
            ->where(['module' => 'supplier', 'popup_window' => $window])
            ->get();

        return view('supplier._product_' . $productCategory, $data);
    }

    /**
     * AJAX request: which displays product details for editing a single product.
     *
     * @param Product $product
     * @param $category_id
     * @return \Illuminate\View\View
     */
    public function showProduct(Product $product, $category_id)
    {
        $data['product_id'] = $product->id;
        $product_fees = $product->price;

        switch ($category_id) {
            case '1':
                $productCategory = 'hotel';
                $window = 'product-hotel';
                break;
            case '2':
                $productCategory = 'vacation';
                $window = 'product-vacation';
                break;
            case '3':
                $productCategory = 'activity';
                $window = 'product-activity';
                break;
            case '4':
                $productCategory = 'car';
                $window = 'product-car';
                break;
            case '5':
                $productCategory = 'flight';
                $window = 'product-flight';
                break;
            case '6':
                $productCategory = 'cruise';
                $window = 'product-cruise';
                break;
            default:
                return false;
        }

        $data['product'] = $product;
        $data['product_fees'] = $product_fees;
        $data['supplier_id'] = $product->supplier_id;
        $data['countries'] = Country::pluck('name', 'id');
        $data['vehicle_classes'] = VehicleClass::pluck('name', 'id');
        $data['product_types'] = ProductType::pluck('name', 'id');

        $data['custom_fields'] = Auth::user()
            ->customFields()
            ->where(['module' => 'supplier', 'popup_window' => $window])
            ->get();

        $data['itinerary_sections'] = [
            1 => 'Section 1',
            2 => 'Section 2',
            3 => 'Section 3',
            4 => 'Section 4',
            5 => 'Section 5',
            6 => 'section6'
        ];

        //by default show section 1 of itinerary
        $data['section1'] = $product->itinerary->section1;

        return view('supplier._product_' . $productCategory, $data);
    }

}
