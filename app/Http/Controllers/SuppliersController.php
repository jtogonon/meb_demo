<?php namespace App\Http\Controllers;

use Auth;
use Entrust;
use Asset, Html, Response;


use App\Supplier, App\CarClass;
use App\Product, App\Booking;
use App\Country, App\Agent, App\ProductType;
use App\ProductFee, App\CustomField, App\ProductItinerarySection;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SuppliersController extends BaseController
{
    public $layout = 'layout';

    public $page_title = 'Suppliers';

    public function __construct(Guard $auth)
    {
        //$this->middleware('guest');
        $this->auth = $auth;

        parent::__construct();

        $jsArray = [
            '/assets/jquery/dist/jquery.min.js',
            '/assets/jquery-ui/jquery-ui.min.js',
            '/assets/slimScroll/jquery.slimscroll.min.js',
            '/assets/bootstrap/dist/js/bootstrap.min.js',
            '/assets/metisMenu/dist/metisMenu.min.js',
            '/assets/iCheck/icheck.min.js',
            '/assets/peity/jquery.peity.min.js',
            '/assets/sweetalert/lib/sweet-alert.min.js',
            '/assets/sparkline/index.js',
            '/scripts/homer.js',

        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/fontawesome/css/font-awesome.css',
            '/assets/metisMenu/dist/metisMenu.css',
            '/assets/animate.css/animate.css',
            '/assets/bootstrap/dist/css/bootstrap.css',
            '/assets/sweetalert/lib/sweet-alert.css',
        ];
        Asset::add($cssArray, 'headerCss');

        $this->title_TopPanel = 'Bookings';

        $this->TopPanel_submenu = ['Dashboard' => '/', 'Customers' => '/customers', 'Products' => '/products/index'];

        view()->share('TopPanel_button', '<a class="btn btn-default btn-sm" href="/suppliers/new">Add Supplier</a>');
    }

    /**
     * Show all suppliers.
     *
     * @param null $product_type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index($product_type = null)
    {
        if (!Entrust::hasRole('SuperAdmin')) { //@TODO create middleware @Jakob
            return redirect('home');
        }
        //start here
        $data = [
            'page_title' => $this->page_title,
            'title_TopPanel' => 'Suppliers',
            'TopPanel_submenu' => $this->TopPanel_submenu
        ];

        //\DB::enableQueryLog();
        if ($product_type) {
            $suppliers = Supplier::join('supplier_product_types', 'suppliers.id', '=', 'supplier_product_types.supplier_id')
                ->where('supplier_product_types.product_type_id', $product_type)
                ->where('suppliers.user_id', Auth::user()->id)
                ->get();
            //dd($suppliers);//to log after toSQL instead of get
        } else {
            //default query
            $suppliers = Supplier::where('user_id', Auth::user()->id)->get();
        }

        $data['suppliers'] = $suppliers;
        $data['product_type'] = $product_type;

        return view('suppliers121/index', $data);
    }

    /**
     * Create new supplier
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data['show_TopPanel'] = false;
        $data['page_title'] = $this->page_title;
        $data['tab_panel_title'] = '&nbsp;';
        $data['supplier'] = null;

        //get countries
        $data['countries'] = Country::lists('name', 'id');

        return view('suppliers121.create', $data);
    }

    /**
     * Save a supplier.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save(Request $request)
    {
        $supplier = new Supplier;
        $supplier->operator_id = $this->auth->user()->operator_id;
        $supplier->supplier_name = $request->input('supplier_name');
        $supplier->supplier_code = $request->input('supplier_code');
        $supplier->website_url = $request->input('website_url');
        $supplier->tax_scheme = '';
        $supplier->logo = '';
        $supplier->contact_name = $request->input('contact_name');
        $supplier->phone = $request->input('phone');
        $supplier->fax = $request->input('fax');
        $supplier->email_sales = $request->input('email_sales');
        $supplier->email_invoice = $request->input('email_invoice');
        $supplier->street = $request->input('street');
        $supplier->city = $request->input('city');
        $supplier->country_id = $request->input('country_id');
        $supplier->company_desc = $request->input('company_desc');
        $supplier->save();

        Session::flash('flash_message', 'New supplier has been created.');

        return redirect('suppliers/index');
    }

    /**
     * AJAX request: for pulling products for products dropdown
     *
     * @param $supplier_id
     * @return mixed
     */
    public function get_products($supplier_id)
    {
        $prods = Product::getProductsBySupplierId($supplier_id);
        $products = [];
        if (isset($prods)) {
            foreach ($prods as $prod => $p) {
                $products[] = ['id' => $p['id'], 'name' => $p['product_name']];
            }
        }

        return Response::json(['data' => ['products' => $products]]);
    }

    public function view($supplier_id)
    {
        $jsArray = [
            '/assets/select2-3.5.2/select2.min.js',
            '/assets/steps/jquery.steps.min.js',
            '/assets/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js',
            '/assets/datatables/jquery.DataTable.1.10.9.min.js',
            '/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',
            '/assets/toastr/build/toastr.min.js',
            '/scripts/suppliers.js', '/assets/summernote/dist/summernote.min.js'
        ];
        Asset::add($jsArray, 'footer');

        $cssArray = [
            '/assets/select2-3.5.2/select2.css',
            '/assets/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css',
            '/assets/toastr/build/toastr.min.css',
            '/assets/summernote/dist/summernote.css',
        ];
        Asset::add($cssArray, 'headerCss');


        $data['show_TopPanel'] = false;
        $data['page_title'] = $this->page_title;
        $data['supplier_id'] = $supplier_id;
        $supplier = Supplier::find($supplier_id);
        $data['supplier'] = $supplier;

        $products = Product::getProductsBySupplierId($supplier_id);
        $data['products'] = $products;

        //get countries
        $data['countries'] = Country::lists('name', 'id');

        $data['tab_panel_title'] = $supplier->supplier_name;

        return view('suppliers/view', $data);
    }

    public function searchSupplierProducts($category_id, $supplier_id)
    {
        $data = [];
        $auth = $this->auth->user();
        if ($category_id == 0) {
            $category_id = null;
        }
        if ($supplier_id == 0) {
            $supplier_id = null;
        }
        $data['supplier_id'] = $supplier_id;

        switch ($category_id) {
            case '1':
                $productCat = 'accomodation';
                break;
            case '2':
                $productCat = 'vacation';
                break;
            case '3':
                $productCat = 'activity';
                break;
            case '4':
                $productCat = 'carrental';
                break;
            case '5':
                $productCat = 'flight';
                break;
            case '6':
                $productCat = 'cruise';
                break;
        }

        $data['products'] = Product::getProducts($auth->operator_id, $category_id);

        $html = view('suppliers/_search_products_' . $productCat)->with($data)->render();

        return Response::json(['data' => ['html' => $html, 'redirecturl' => '/']]);
    }

    public function add_product($category_id, $supplier_id)
    {
        $data = [];
        $auth = $this->auth->user();
        if ($category_id == 0) {
            $category_id = null;
        }

        $data['product_id'] = 0;

        switch ($category_id) {
            case '1':
                $productCat = 'accomodation';
                $window = 'product-accomodation';
                break;
            case '2':
                $productCat = 'vacation';
                $window = 'product-vacation';
                break;
            case '3':
                $productCat = 'activity';
                $window = 'product-activity';
                break;
            case '4':
                $productCat = 'carrental';
                $window = 'product-carrental';

                break;
            case '5':
                $productCat = 'flight';
                $window = 'product-flight';
                break;
            case '6':
                $productCat = 'cruise';
                $window = 'product-cruise';
                break;
        }

        //get countries
        $data['countries'] = Country::lists('name', 'id');

        //get countries
        $data['carclass'] = CarClass::lists('class_name', 'id');

        //get countries
        $data['productypes'] = ProductType::lists('type_name', 'id');
        $data['supplier_id'] = $supplier_id;

        $data['customfields'] = CustomField::getFieldsByModule($auth->operator_id, 'suppliers', '', $window);

        return view('suppliers/_product_' . $productCat, $data);
    }

    public function save_product_details(Request $req)
    {
        $auth = $this->auth->user();

        if ($req->input('product_id')) {
            $product = Product::find($req->input('product_id'));
            $product->operator_id = $auth->operator_id;
        } else {
            $product = new Product;
            $product->operator_id = $auth->operator_id;
        }

        if ($req->input('category_id') == '1') { //accomodation
            $product->product_name = $req->input('product_name');
            $product->product_code = $req->input('product_code');
        }

        if ($req->input('category_id') == '2') { //vacation
            $product->product_type_id = $req->input('product_type_id');
            $product->product_name = $req->input('product_name');
        }

        if ($req->input('category_id') == '3') { //activity
            $product->product_name = $req->input('product_name');
        }

        if ($req->input('category_id') == '4') { //carrental
            $product->product_name = $req->input('product_name');
            $product->car_class = $req->input('car_class');
        }

        if ($req->input('category_id') == '5') { //flight
            $product->flight_departing = Booking::formatDate($req->input('flight_departing'), 1);
            $product->flight_arriving = Booking::formatDate($req->input('flight_arriving'), 1);
            $product->product_name = $req->input('product_name');
        }

        if ($req->input('category_id') == '6') { //cruise
            $product->product_name = $req->input('product_name');
        }

        $product->description = $req->input('description');
        $product->commision = $req->input('commision');

        $product->country_id = $req->input('country_id');
        $product->category_id = $req->input('category_id');
        $product->supplier_id = $req->input('supplier_id');
        $product->description = $req->input('description');

        $product->save();

        //save product fees
        $fees_json = $req->input('fee');

        //exit();
        $exists = ProductFee::checkProduct($product->id, $auth->operator_id);
        //$html = dd($fees_json);

        if (count($exists) > 0) {
            //update
        } else {
            //insert
            $productFee = new ProductFee;
            $productFee->operator_id = $auth->operator_id;
            $productFee->product_id = $product->id;
            $productFee->fees = json_encode($fees_json);
            $productFee->save();
        }

        //save itinerary section
        $sectionNum = $req->input('section_select');
        $content = $req->input('section_content');
        $sectionSaved = ProductItinerarySection::saveSectionContent($auth->operator_id, $product->id, $sectionNum, $content);

        $html = 'ok';

        return Response::json(['data' => ['html' => $html]]);
    }

    /**
     * AJAX request: which displays product details for editing a single product.
     *
     * @param $product_id
     * @param $category_id
     * @return \Illuminate\View\View
     */
    public function get_product_details($product_id, $category_id)
    {
        $auth = $this->auth->user();

        $data['product_id'] = $product_id;
        $product = Product::find($product_id);
        $productfees = ProductFee::getFeesByProductId($product->id);

        switch ($category_id) {
            case '1':
                $productCat = 'accomodation';
                $window = 'product-accomodation';
                break;
            case '2':
                $productCat = 'vacation';
                $window = 'product-vacation';
                break;
            case '3':
                $productCat = 'activity';
                $window = 'product-activity';
                break;
            case '4':
                $productCat = 'carrental';
                $window = 'product-carrental';
                break;
            case '5':
                $productCat = 'flight';
                $window = 'product-flight';

                $product->flight_departing = Booking::formatDate($product->flight_departing, 2);
                $product->flight_arriving = Booking::formatDate($product->flight_arriving, 2);
                break;
            case '6':
                $productCat = 'cruise';
                $window = 'product-cruise';
                break;
        }

        $data['product'] = $product;
        $data['productfees'] = $productfees;
        $data['supplier_id'] = $product->supplier_id;

        //get countries
        $data['countries'] = Country::lists('name', 'id');

        //get countries
        $data['carclass'] = CarClass::lists('class_name', 'id');

        //get countries
        $data['productypes'] = ProductType::lists('type_name', 'id');
        //$data['supplier_id'] = $supplier_id;

        $data['customfields'] = CustomField::getFieldsByModule($auth->operator_id, 'suppliers', '', $window);

        $data['itinerary_sections'] = [
            1 => 'Section 1',
            2 => 'Section 2',
            3 => 'Section 3',
            4 => 'Section 4',
            5 => 'Section 5'
        ];

        //by default show section 1 of itinerary
        $section1 = ProductItinerarySection::getSection1($auth->operator_id, $product_id);

        $data['section1'] = ($section1 != false) ? $section1->content : '';

        return view('suppliers/_product_' . $productCat, $data);
    }

    public function get_section_content($product_id, $section)
    {
        $auth = $this->auth->user();

        $content = ProductItinerarySection::getSectionContent($auth->operator_id, $product_id, $section);
        return Response::json(['data' => ['content' => $content]]);
    }

    public function testpaypal()
    {

    }
}