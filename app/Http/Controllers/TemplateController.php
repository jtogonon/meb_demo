<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ItineraryTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = [
            'itineraryTemplates' => ItineraryTemplate::all(),
            'page_title' => 'Templates',
            'title_TopPanel' => 'Templates',
            'TopPanel_button' => '<a class="btn btn-default btn-sm" href="template/create">Create Template</a>',
            'TopPanel_submenu' => [
                'Dashboard' => '/',
                'Customers' => '/customers',
                'Products' => '/products/index'
            ],
            'show_TopPanel' => true
        ];

        return view('bookings.templates.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data = [
            'itineraryTemplates' => ItineraryTemplate::all(),
            'page_title' => 'Templates',
            'title_TopPanel' => 'Templates',
            'TopPanel_button' => '',
            'TopPanel_submenu' => [
                'Dashboard' => '/',
                'Customers' => '/customers',
                'Products' => '/products/index'
            ],
            'show_TopPanel' => true
        ];

        return view('bookings.templates.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'layout' => 'required']);

        ItineraryTemplate::create($request->all());

        Session::flash('flash_message', 'Template created!');

        return redirect('bookings/itinerary/template');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
