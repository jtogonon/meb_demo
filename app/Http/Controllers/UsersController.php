<?php namespace App\Http\Controllers;

use App\Passenger;
use Html;
use Asset;
use Entrust;
use Response;

use App\User;
use App\Booking;
use App\Country;
use App\Address;
use App\Roleuser;
use App\Nationality;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsersController extends BaseController
{
    public $layout = 'layout';

    public $page_title = 'Dashboard';

    /**
     * Create a new controller instance.
     *
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        //$this->middleware('guest');
        $this->auth = $auth;

        parent::__construct();

        $jsArray = [
            '/assets/jquery/dist/jquery.min.js',
            '/assets/jquery-ui/jquery-ui.min.js',
            '/assets/slimScroll/jquery.slimscroll.min.js',
            '/assets/bootstrap/dist/js/bootstrap.min.js',
            '/assets/metisMenu/dist/metisMenu.min.js',
            '/assets/iCheck/icheck.min.js',
            '/assets/peity/jquery.peity.min.js',
            '/assets/sweetalert/lib/sweet-alert.min.js',
            '/assets/sparkline/index.js',
            '/scripts/homer.js',

        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/fontawesome/css/font-awesome.css',
            '/assets/metisMenu/dist/metisMenu.css',
            '/assets/animate.css/animate.css',
            '/assets/bootstrap/dist/css/bootstrap.css',
            '/assets/sweetalert/lib/sweet-alert.css',
        ];
        Asset::add($cssArray, 'headerCss');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index()
    {
        if (!Entrust::hasRole('SuperAdmin')) {
            return redirect('home');
        }

        $jsArray = ['/assets/select2-3.5.2/select2.min.js', '/assets/steps/jquery.steps.min.js', '/assets/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js',
            '/assets/datatables/jquery.DataTable.1.10.9.min.js',
            '/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js', '/assets/toastr/build/toastr.min.js',
            '/scripts/customers.js',
        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css',
        ];
        Asset::add($cssArray, 'headerCss');

        $data = ['page_title' => $this->page_title,
            'title_TopPanel' => 'Customers',
            'show_TopPanel' => true,
            'TopPanel_button' => '<a id="link_popup_newcustomer" class="btn btn-default btn-sm" href="/customers/add">Add Customer</a>',
        ];

        $auth = $this->auth->user();

        $customers = User::getCustomersByOperatorId($auth->operator_id);
        $data['customers'] = $customers;

        return view('users/index', $data);
    }

    public function dashboard()
    {
        if (!Entrust::hasRole('SuperAdmin')) {
            return redirect('home');
        }

        $jsArray = [
            '/assets/jquery-flot/jquery.flot.js',
            '/assets/jquery-flot/jquery.flot.resize.js',
            '/assets/jquery-flot/jquery.flot.pie.js',
            '/assets/flot.curvedlines/curvedLines.js',
            '/assets/jquery.flot.spline/index.js',
        ];
        Asset::add($jsArray, 'footer');

        $data = [
            'page_title' => $this->page_title,
            'show_TopPanel' => false,
        ];

        return view('users/dashboard', $data);
    }

    public function save(Request $request)
    {
        if ($request->isMethod('post')) {
            $user = new User;
            $user->operator_id = $this->auth->user()->operator_id;
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->email = $request->input('email');
            $user->role_id = 3; //customer
            $user->save();

            //insert address
            $address = new Address;
            $address->operator_id = $this->auth->user()->operator_id;
            $address->user_id = $user->id;
            $address->country_id = $request->input('country_id');
            $address->street = $request->input('address');
            $address->city = $request->input('city');
            $address->state = $request->input('state');
            $address->postal = $request->input('postal');
            $address->save();

            //insert user

            \Session::flash('flash_message', 'New customer has been created.');
        }

        return redirect('customers');
    }

    /**
     * AJAX request: showing details in a popup.
     *
     * @param $user_id
     * @return \Illuminate\View\View
     */
    public function details($user_id)
    {
        //get countries
        $data['countries'] = Country::pluck('name', 'id');

        //nationalities
        $data['nationalities'] = Nationality::pluck('name', 'id');

        $passenger = Passenger::first($user_id);
        $passenger->date_of_birth = Booking::formatDate($passenger->date_of_birth, 2);
        $data['customer'] = $passenger;

        if ($passenger->country_id == 226) {
            //country is USA, show default, states is dropdown
            $data['show_states_default'] = true;
            $data['us_states'] = Address::USA_States();
        } else {
            $data['show_states_default'] = false;
            $data['us_states'] = Address::USA_States();
        }

        $custom_fields = [
            1 => [
                'name' => 'custom_1',
                'label' => 'Custom 1',
                'value' => 'Custom value',
            ],
            2 => [
                'name' => 'custom_2',
                'label' => 'Custom 2',
                'value' => 'Custom 2 value',
            ]

        ];
        $data['custom_fields'] = $custom_fields;

        return view('users._details_customer', $data);
    }

    public function add()
    {
        //get countries
        $data['countries'] = Country::pluck('name', 'id');

        //nationalities
        $data['nationalities'] = Nationality::pluck('name', 'id');
        $data['show_states_default'] = false;
        $data['us_states'] = Address::USA_States();

        $custom_fields = [
            1 => [
                'name' => 'custom_1',
                'label' => 'Custom 1',
                'value' => '',
            ],
            2 => [
                'name' => 'custom_2',
                'label' => 'Custom 2',
                'value' => '',
            ]
        ];
        $data['custom_fields'] = $custom_fields;

        return view('users/_details_customer', $data);
    }

    /**
     * AJAX request post data to update customer/user.
     *
     * @param Request $request
     * @return mixed
     */
    public function savedetails(Request $request)
    {
        $auth = $this->auth->user();

        if ($request->input('user_id')) {
            $user = User::find($request->input('user_id'));
        } else {
            $user = new User;
            $user->operator_id = $auth->operator_id;
        }

        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->date_of_birth = Booking::formatDate(urldecode($request->input('dob')), 1);
        $user->gender = $request->input('gender');
        $user->nationality_id = $request->input('nationality');
        $user->phone_home = $request->input('phone_home');
        $user->phone_mobile = $request->input('phone_mobile');
        $user->notes = $request->input('notes');
        $user->save();

        if ($request->input('user_id')) {
            //update address
            $address = Address::where('user_id', $request->input('user_id'))
                ->get();
            $adrs = Address::find($address[0]->id);
        } else {
            $adrs = new Address;
        }

        $adrs->street = $request->input('address');
        $adrs->city = $request->input('city');
        $adrs->state = ($request->input('state2') != '') ? $request->input('state2') : $request->input('state');
        $adrs->postal = $request->input('zip');
        $adrs->country_id = $request->input('country_id');
        $adrs->save();

        if (!$request->input('user_id')) {
            //create new user role
            $userRole = new Roleuser;
            $userRole->user_id = $user->id;
            $userRole->role_id = 3;
            $userRole->save();
        }

        return Response::json(['data' => ['html' => 'saved', 'redirecturl' => '/']]);
    }

    public function save_new_customer($newName)
    {
        $lastname = '';
        $firstname = '';
        $pcs = explode(' ', $newName);
        if (isset($pcs[2])) {
            $lastname = $pcs[2];
            $firstname = $pcs[0] . ' ' . $pcs[1];
        } else {
            $lastname = $pcs[1];
            $firstname = $pcs[0];
        }
        $user = new User;
        $user->operator_id = $this->auth->user()->operator_id;
        $user->firstname = ucfirst($firstname);
        $user->lastname = ucfirst($lastname);
        $user->email = '';
        //$user->role_id = 3; //customer
        $user->save();

        //insert address
        $address = new Address;
        $address->operator_id = $this->auth->user()->operator_id;
        $address->user_id = $user->id;
        $address->country_id = '';
        $address->street = '';
        $address->city = '';
        $address->state = '';
        $address->postal = '';
        $address->save();

        $userRole = new Roleuser;
        $userRole->user_id = $user->id;
        $userRole->role_id = 3;
        $userRole->save();

        //$html['name']  = urldecode($newName);
        //$html['user_id']  = $user->id;

        return Response::json(['data' => ['user_id' => $user->id, 'name' => urldecode($newName)]]);
    }

    public function manage()
    {
        $jsArray = [
            '/assets/datatables/media/js/jquery.dataTables.min.js',
            '/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js'
        ];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css',
        ];
        Asset::add($cssArray, 'headerCss');

        //start here
        $auth = $this->auth->user();
        if (!$auth) {
            return redirect('/login');
        }
        $data = ['page_title' => $this->page_title,
            'title_TopPanel' => 'Manage Users',
            'TopPanel_button' => '<a class="btn btn-default btn-sm" href="/users/create">Add User</a>',
            'TopPanel_submenu' => ['Administrator' => '/administrator/index', 'Manage Users' => '/users/manage']
        ];

        $users = \DB::table('users')
            ->select('users.*', 'roles.name as role_name')
            ->leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
            ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
            ->get();
        $data['users'] = $users;

        return view('users.manage', $data);
    }

    public function removeUser($id)
    {
        $user = User::find($id);
        $data['success'] = 1;
        if (!$user) {
            $data['success'] = 0;
        } else {
            $user->delete();
            $data['id'] = $id;
        }

        return Response::json($data);
    }

    public function create()
    {
        $jsArray = ['/assets/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js'];
        Asset::add($jsArray, 'footer');

        $cssArray = ['/assets/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css'];
        Asset::add($cssArray, 'headerCss');

        $data = [
            'page_title' => 'Create User',
            'title_TopPanel' => 'Create User',
            'TopPanel_submenu' => [
                'Administrator' => '/administrator/index',
                'Manage Users' => '/users/manage',
                'Add User' => '/users/create'
            ]
        ];

        $data['nationality'] = \App\Nationality::pluck('name', 'id');

        return view('users.create', $data);
    }

    public function store(Request $request)
    {
        $messages = [
            'first_name' => 'The first name must contain letters and space only.',
            'last_name' => 'The last name must contain letters and space only.',
        ];
        $validator = Validator::make($request->all(), User::$rules, $messages);

        if (!$validator->fails()) {
            $insert = [];

            foreach ($request->all() as $key => $data) {
                if ($key == 'password') {
                    $insert[$key] = Hash::make($data);
                } else {
                    $auth = $this->auth->user();
                    $insert['operator_id'] = $auth->operator_id;
                    $insert[$key] = $data;
                }
            };
            $user = User::create($insert);
            $user->attachRole(5);
            \Session::flash('success_flash', 'User successfully created.');
            return redirect('users/create');
        }

        return redirect('users/create')
            ->withErrors($validator)
            ->withInput();
    }

    public static function convertToHoursMins($time, $format = '%02d:%02d:%02d')
    {
        if ($time < 1) {
            return $time;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        $seconds = ($minutes / 60);
        return sprintf($format, $hours, $minutes, $seconds);
    }

    /**
     *
     *
     * @param $customer_id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function customerBookings($customer_id)
    {
        $data['customer'] = User::find($customer_id);
        $data['bookings'] = User::find(3)->bookings; //User::find($customer_id)->bookings;

        $html = view('users._bookings_customer')->with($data)->render();

        return Response::json(['data' => ['html' => $html]]);
    }
}
