<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::group(['middleware' => ['auth']], function () {

        /*-------------------------------------BOOKING MODULE---------------------------------------------------------*/

        Route::group(['prefix' => 'booking'], function () {

            Route::post('order_events/{booking}', 'Booking\BookingController@orderEvents');

            /*-----------------------------------BOOKING ITEM---------------------------------------------------------*/
            Route::get('search_products/{category_id?}/{details_view?}', 'Booking\BookingController@searchProducts');
            Route::get('latest_bookings', 'Booking\BookingController@getLatestBookings');
            Route::get('latest_bookings_search', 'Booking\BookingController@getLatestBookingsSearch');

            //@TODO move this to ItemController @Jakob
            Route::get('item/add/tmp/{product_id?}', 'Booking\BookingController@addItemTmp');
            Route::get('item/add/{booking}/{product_id}', 'Booking\BookingController@addItem');
            Route::get('load_items_in_table_session', 'Booking\BookingController@loadItemsInTableSession');
            Route::get('remove_tmp_item/{item_id?}', 'Booking\BookingController@removeTmpItem');
            Route::get('load_items/{_id?}', 'Booking\BookingController@loadItems');
            Route::get('item/remove/{booking}/{product_id}', 'Booking\BookingController@removeItem');
            Route::get('items/clear', 'Booking\BookingController@removeTmpItems');

            Route::resource('item', 'Product\ItemController'); //update not done
            /*--------------------------------------------------------------------------------------------------------*/

            /*-----------------------------------BOOKING DOCUMENT-----------------------------------------------------*/
//            Route::get('download/{type?}/{booking_id?}/{ext?}', 'Booking\BookingController@download');
//            Route::get('generate_itinerary/{booking_id?}/{template_id?}', 'Booking\BookingController@generate_itinerary');
//            Route::get('generate_invoice/{booking_id?}/{template_id?}', 'Booking\BookingController@generate_invoice');
            /*--------------------------------------------------------------------------------------------------------*/

            /*-----------------------------------BOOKING PASSENGER----------------------------------------------------*/
            Route::get('passengers/{booking}', 'Booking\PassengerController@bookingsPassengers');
            Route::get('passenger/add/row/{num?}', 'Booking\PassengerController@addRowPassenger');
            Route::post('passengers/save/{booking?}', 'Booking\PassengersController@savePassengers');
            /*--------------------------------------------------------------------------------------------------------*/
        });

        /*
         * RESTful CRUD for Booking Module.
         */
        Route::resource('booking', 'Booking\BookingController');

        /*------------------------------------SUPPLIER MODULE---------------------------------------------------------*/

        /*
         * AJAX Requests for Supplier Module.
         */
        Route::group(['prefix' => 'supplier'], function () {
            Route::get('products/{category_id?}/{supplier_id?}', 'Supplier\SupplierController@getSupplierProducts');
            Route::get('product/create/{category_id?}/{supplier_id?}', 'Supplier\SupplierController@createProduct');
            Route::get('product/{product}/{category_id?}', 'Supplier\SupplierController@showProduct');
            Route::post('product/save', 'SuppliersController@save_product_details');


            //@TODO find this methods if needed move this to ItemController or ProductController @Jakob
            Route::get('get_products/{supplier_id?}', 'SuppliersController@get_products');
            Route::get('get_section_content/{product_id?}/{section?}', 'SuppliersController@get_section_content');
        });

        /*
         * RESTful CRUD for Supplier Module.
         */
        Route::resource('supplier', 'Supplier\SupplierController');

        /*-------------------------------------PRODUCT MODULE---------------------------------------------------------*/

        /*
         * AJAX Requests for Product Module.
         */
        Route::group(['prefix' => 'product'], function () {
            //@TODO find this methods @Jakob
            Route::get('get_all_products', 'ProductsController@get_all_products');
            Route::get('delete/{product_id?}', 'ProductsController@delete');
        });

        /*
         * RESTful CRUD for Product Module.
         */
        Route::resource('product', 'Product\ProductController');
    });

    /*---------------------------------------USER MODULE----------------------------------------------------------*/

    //USERS
    Route::get('customers', 'Booking\PassengerController@index');
    Route::get('customers/index', 'Booking\PassengerController@index');
    Route::get('customers/add', 'UsersController@add');
    Route::get('customers/details/{user_id?}', 'UsersController@details');
    Route::get('customers/viewdetails/{user_id?}', 'UsersController@viewdetails');
    Route::get('customers/savedetails', 'UsersController@savedetails');
    Route::get('customers/save_new_customer/{newterm?}', 'UsersController@save_new_customer');
    Route::get('customers/bookings/{user_id?}', 'UsersController@customerBookings');

    Route::model('user', 'User');
    Route::post('users/save', 'UsersController@save');
    Route::get('users/manage', 'UsersController@manage');
    Route::delete('user/delete/{id}', 'UsersController@removeUser');
    Route::get('users/create', 'UsersController@create');
    Route::post('users/store', 'UsersController@store');

    /*------------------------------------------------------------------------------------------------------------*/

    Route::get('home', 'HomeController@index');
    Route::get('home/start', 'HomeController@start');

    //Dashboard
    Route::get('dashboard', 'UsersController@dashboard');

    //EMAILS
    Route::get('emails/popup/{booking_id?}/{module?}', 'EmailsController@popup');
    Route::get('emails/gettemplatecontent/{id?}', 'EmailsController@getTemplateContent');
    Route::get('emails/getcontacts', 'EmailsController@getContacts');
    Route::post('emails/send', 'EmailsController@send');

    //EMAILTEMPLATES
    Route::get('emailtemplates/index', 'EmailTemplatesController@index');
    Route::get('emailtemplates/new', 'EmailTemplatesController@create');
    Route::get('emailtemplates/view/{id?}', 'EmailTemplatesController@view');
    Route::post('emailtemplates/save', 'EmailTemplatesController@save');


    //SETTINGS
    Route::get('settings/emails', 'SettingsController@emails');
    Route::post('settings/save', 'SettingsController@save');
    Route::get('settings/customfields', 'SettingsController@customfields');
    Route::get('settings/getcustomfields/{module?}', 'SettingsController@getcustomfields');
    Route::post('settings/savecustomfields', 'SettingsController@saveCustomFields');
    Route::get('settings/deletecustomfield/{id?}', 'SettingsController@deleteCustomFields');

    //REPORTS
    Route::get('reports/index', 'ReportsController@index');

    //ADMINISTRATOR
    Route::get('administrator', 'AdministratorController@index');
    Route::get('administrator/extrafields', 'CustomFieldsController@customfields');
    Route::post('administrator/get_extrafields', 'CustomFieldsController@getcustomfields');
    Route::post('administrator/save_extrafields', 'CustomFieldsController@save_customfields');

    //DOCUMENTS
    Route::get('documenttemplate/index', 'DocumentsTemplateController@index');
    Route::get('documenttemplate/delete/{id}', 'DocumentsTemplateController@delete');

    //ACCOUNTS
    Route::get('accounts/index', 'AccountsController@index');
    Route::put('accounts/update', 'AccountsController@update');
    Route::get('accounts/addCard', 'AccountsController@addCard');
    Route::get('accounts/addBankAccount', 'AccountsController@addBankAccount');
    Route::get('accounts/choose_plan', 'AccountsController@choosePlan');

    //PAYPAL
    Route::get('paypal/test', 'PaypalController@test');

    //CREDIT CARD
    Route::get('card/check', 'CreditCardsController@check');

    //BANK ACCOUNT
    Route::get('bank/check', 'BankAccountController@check');

    Route::get('reviews', 'ReviewsController@index');

    //ENQUIRIES
    Route::get('enquiries/index', 'EnquiriesController@index');
    Route::post('enquiries/createform', 'EnquiriesController@createform');

    Route::auth();
});
