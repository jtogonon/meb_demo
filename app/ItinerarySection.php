<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItinerarySection extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'itinerary_sections';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'section1',
        'section2',
        'section3',
        'section4',
        'section5',
        'section6',
    ];
}
