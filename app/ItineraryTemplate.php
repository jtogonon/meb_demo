<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpWord\Shared\Converter;

class ItineraryTemplate extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'itinerary_templates';

    /**
     * Fillable fields for model.
     *
     * @var array
     */
    protected $fillable = ['name', 'operator_id', 'type', 'layout', 'filename'];

    /**
     * Get all templates by operator id.
     *
     * @param $operator_id
     * @return mixed
     */
    public static function getTemplatesByOperator($operator_id)
    {
        $templates = self::where('operator_id', $operator_id)->get();
        return $templates;
    }

    /**
     * Write documents.
     *
     * @param \PhpOffice\PhpWord\PhpWord $phpWord
     * @param string $filename
     * @param array $writers
     *
     * @param $resultPath
     * @return string
     */
    public static function write($phpWord, $filename, $writers, $resultPath)
    {
        $result = '';

        // Write documents
        foreach ($writers as $format => $extension) {
            $result .= date('H:i:s') . " Write to {$format} format";
            if (null !== $extension) {
                $targetFile = $resultPath . $filename . '.' . $extension;
                $phpWord->save($targetFile, $format);
            } else {
                $result .= ' ... NOT DONE!';
            }
            $result .= '';
        }

        return $result;
    }

    /**
     * Foreach product, depending on the category, layout can be different.
     *
     * @param $template_id
     * @param $items
     * @param $section
     * @param $ItemNameStyle
     * @param $ItemDescriptionStyle
     * @param $ItemDayStyle
     * @return mixed
     */
    public static function layout_product_items($template_id, $items, $section, $ItemNameStyle, $ItemDescriptionStyle, $ItemDayStyle)
    {
        $pictures = [];
        $itemsByDay = $items->groupBy(function ($item) {
            if ($item->getItemDate() == "") {
                return strtotime('2000-01-01');
            }
            return strtotime($item->getItemDate());
        })->all();

        ksort($itemsByDay);

        foreach ($itemsByDay as $key => $items) {

            $day = date_to_human_friendly($key);

            $table = $section->addTable(['cellMargin' => 0, 'borderTop' => 0]);

            $table->addRow(50);

            $cell1 = $table->addCell(1700, ['valign' => 'top', 'borderTop' => 0]);
            $cell1->addText(htmlspecialchars($day, ENT_COMPAT, 'UTF-8'), $ItemDayStyle);

            foreach ($items as $item) {
                $outputFile = public_path() . '/images/products/' . str_random(20);

                $item_name = $item->product->product_name;

                $item_description = $item->getSectionContent(1);
                $item_description = $item->cleanRenderSection($item_description);

                $content = $item->getSectionContent(2);

                $item_picture = get_picture_from_wysiwyg($content, $outputFile);
                $pictures[] = $item_picture;

                $itemDay = '';
                if ($item->getItemDate()) {
                    $itemDay = date_to_human_friendly($item->getItemDate());
                };

                self::generateItemSection(
                    $itemDay,
                    $ItemNameStyle,
                    $ItemDescriptionStyle,
                    $table,
                    $item_name,
                    $item_description,
                    $item_picture
                );

                if ($item->itemDetail && $template_id == 1) {

                    $table->addRow(50);

                    $table->addCell(250, ['valign' => 'top', 'borderTop' => 0]);
                    $table->addCell(100, ['valign' => 'top', 'border' => 0]);
                    $cell4 = $table->addCell(750, ['valign' => 'top', 'borderTop' => 0]);

                    switch ($item->product->category_id) {
                        case 1: //accommodation
                            $cell4->addText(htmlspecialchars($item->itemDetail->act_hotelresort, ENT_COMPAT, 'UTF-8'));
                            $cell4->addText(htmlspecialchars($item->itemDetail->notes, ENT_COMPAT, 'UTF-8'));
                            break;
                        case 2: //vacation
                            $cell4->addText(htmlspecialchars($item->notes, ENT_COMPAT, 'UTF-8'));
                            break;
                        case 3: //activity
                            $cell4->addText(htmlspecialchars($item->itemDetail->act_hotelresort, ENT_COMPAT, 'UTF-8'));
                            $cell4->addText(htmlspecialchars($item->itemDetail->act_name, ENT_COMPAT, 'UTF-8'));
                            $cell4->addText(htmlspecialchars($item->itemDetail->notes, ENT_COMPAT, 'UTF-8'));
                            break;
                        case 4: //car rental
                            $cell4->addText(htmlspecialchars('Pickup: ' . $item->itemDetail->carrent_location_pickup, ENT_COMPAT, 'UTF-8'));
                            $cell4->addText(htmlspecialchars('Return: ' . $item->itemDetail->carrent_location_return, ENT_COMPAT, 'UTF-8'));
                            $cell4->addText(htmlspecialchars($item->itemDetail->notes, ENT_COMPAT, 'UTF-8'));
                            break;
                        case 5: //flight
                            $cell4->addText(htmlspecialchars('From ' . $item->itemDetail->flight_from_city, ENT_COMPAT, 'UTF-8'));
                            $cell4->addText(htmlspecialchars('To ' . $item->itemDetail->flight_to_city, ENT_COMPAT, 'UTF-8'));
                            $cell4->addText(htmlspecialchars($item->itemDetail->notes, ENT_COMPAT, 'UTF-8'));
                            break;
                        case 6: //cruise
                            $cell4->addText(htmlspecialchars($item->itemDetail->notes, ENT_COMPAT, 'UTF-8'));
                            break;
                    }
                }
            }
        }

        return ['section' => $section, 'pictures' => $pictures];
    }

    /**
     * Generate Item section depending on type.
     *
     * @param $itemTime
     * @param $ItemNameStyle
     * @param $ItemDescriptionStyle
     * @param $table
     * @param $itemName
     * @param $itemDescription
     * @param $itemPicture
     * @param int $rowSize
     */
    private static function generateItemSection(
        $itemTime,
        $ItemNameStyle,
        $ItemDescriptionStyle,
        $table,
        $itemName,
        $itemDescription,
        $itemPicture,
        $rowSize = 50
    )
    {
        $table->addRow($rowSize);
        $cell1 = $table->addCell(1700, ['borderTop' => 0, 'gridSpan' => 3]);
        $cell1->addText(htmlspecialchars($itemName, ENT_COMPAT, 'UTF-8'), $ItemNameStyle);

        $table->addRow($rowSize);

        $cell2 = $table->addCell(12000, ['borderTop' => 0]);
        $cell2->addText($itemDescription, $ItemDescriptionStyle);

        $table->addCell(100, ['valign' => 'top', 'border' => 0]);

        $cell3 = $table->addCell(2000, ['valign' => 'top', 'border' => 0]);

        if ($itemPicture) {
            $cell3->addImage($itemPicture, ['width' => Converter::cmToPixel(5), 'height' => Converter::cmToPixel(3.5)]);
        }
    }
}