<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Nationality extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nationalities';
}