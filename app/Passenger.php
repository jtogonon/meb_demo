<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Passenger extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'passengers';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'prefix',
        'address1',
        'address2',
        'country',
        'city',
        'state',
        'zip_code',
        'prefix',
        'dob',
        'home_phone',
        'mobile_phone',
        'email',
        'notes',
        'gender',
        'age',
        'user_defined1',
        'user_defined2',
        'user_defined3',
        'user_defined4',
        'user_defined5',
        'customer_id',
        'user_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'dob'
    ];

    /**
     * Passenger can be User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Create row for table @TODO find out what is this for @Jakob
     *
     * @return string
     */
    public static function createRow()
    {//@TODO change to view->make @Jakob
        $element = '
        <input type="hidden" name="user_id[]" value="<?=$pasngr->user_id?>">
            <h4>Passenger 1</h4>
            <div class="col-lg-10 ">
                <div class="form-group">
                    <input type="hidden" name="passenger[]">
                    <div class="col-sm-2">
                        Prefix<br/>
                        <input type="text" name="prefix_" value="" class="form-control input-sm">
                    </div>
                    <div class="col-sm-3">
                        FirstName<br/>
                        <input type="text" name="firstname_" value="" class="form-control input-sm"></div>
                    <div class="col-sm-2">MI<br/>
                        <input type="text" name="middle_" value="" class="form-control input-sm"></div>
                    <div class="col-sm-3">LastName<br/>
                        <input type="text" name="lastname_" value="" class="form-control input-sm"></div>
                    <div class="col-sm-2">Suffix<br/>
                        <input type="text" name="suffix_" value="" class="form-control input-sm">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3">
                        Date of Birth<br/>
                        <input type="text" name="dob_" value="" class="form-control input-sm datepicker">
                    </div>
                    <div class="col-sm-3">
                        Country of Citizenship<br/>
                        {!! Form::select(\'country_\'.$countr, $countries,  $userCountryId,[\'class\'=>\'select2 form-control input-sm\',\'style\'=>\'width:120px;\']) !!}
                    </div>
                    <div class="col-sm-3">
                        Gender<br/>
                        {!! Form::select(\'gender_\'.$countr, [\'Male\'=>\'Male\',\'Female\'=>\'Female\'],  $pasngr->user->gender,[\'class\'=>\'select2 form-control input-sm\',\'style\'=>\'width:120px;\']) !!}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <hr/>
        ';

        return $element;
    }

    /**
     * Passenger belongs to booking.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function bookings()
    {
        return $this->belongsToMany(Booking::class);
    }

    /**
     * Get Users all laed travelers.
     *
     * @return array
     */
    public static function leadTravelers()
    {
        $lead_travelers = [];
        $user_booking_ids = [];
        $bookings =  Auth::user()->bookings;

        foreach($bookings as $booking){
            $user_booking_ids[] = $booking->id;
        }

        $booking_lead_travelers = DB::table('booking_passenger')
            ->where('lead_traveler', '=', '1')
            ->whereIn('booking_id', $user_booking_ids)->get();

        foreach($booking_lead_travelers as $booking_lead_traveler){
            $lead_travelers[] =  Passenger::find($booking_lead_traveler->passenger_id);
        }

        return $lead_travelers;
    }

    /**
     * Passenger belongs to Country.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
