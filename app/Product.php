<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'code',
        'website',
        'zip',
        'category_id',
        'price_id',
        'itinerary_id',
        'type_id',
        'user_id',
        'country_id'
    ];

    /**
     * Get users all Products by category.
     *
     * @param $category_id
     * @return bool
     */
    public static function getProductsByCategory($category_id)
    {
        $category_id = (integer)$category_id;

        if ($category_id > 6 || $category_id < 1) {
            return false;
        }

        $products = self::where(['category_id' => $category_id, 'user_id' => Auth::user()->id])
            ->orderBy('name', 'ASC')
            ->get();

        return $products;
    }

    /**
     * Products total price.
     *
     * @return int
     */
    public function getPrice()
    {
        return 264; //@TODO get price @Jakob
    }

    /*-------------------------------------OLD---------------------------*/

    public function get_productfees()
    {
        if ($this->productfees) {
            return $this->productfees->fees;
        }

        return '';
    }


    public function promos()
    {
        return $this->hasOne(ProductPromo::class, 'product_id');
    }


    public static function getProductsByOperatorId($operator_id)
    {
        $products = self::where('operator_id', ($operator_id))
            ->orderBy('product_name', 'ASC')
            ->get();

        return $products;
    }

    public static function getProducts($operator_id, $category_id = null)
    {
        if ($category_id != null) {
            $products = self::where('operator_id', ($operator_id))
                ->where('category_id', ($category_id))
                ->orderBy('product_name', 'ASC')
                ->get();
        } else {
            $products = self::getProductsByOperatorId($operator_id);
        }

        return $products;
    }

    public static function getProductsBySupplierId($supplier_id)
    {
        $products = self::select('*')
            ->where('supplier_id', $supplier_id)
            ->orderBy('product_name', 'ASC')
            ->get();

        return $products;
    }

    public static function getFee($product_id)
    {
        $product = self::find($product_id);

        return $product->fee;
    }

    /**
     * Product price per day.
     *
     * @return float
     */
    public function perDayPrice()
    {
        $jsonFee = ProductFee::getFeesByProductId($this->id);

        $total = ProductFee::extract_fee($jsonFee, 'total');

        return round($total, 2);
    }

    /*******************************************************************/

    /**
     * Product belongs to Supplier.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    /**
     * Product has many Hotels.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hotels()
    {
        return $this->hasOne(Hotel::class);
    }

    /**
     * Product has many Flights.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function flights()
    {
        return $this->hasOne(Flight::class);
    }

    /**
     * Product has many Cars.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cars()
    {
        return $this->hasOne(Car::class);
    }

    /**
     * Product has many Activities.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activities()
    {
        return $this->hasOne(Activity::class);
    }

    /**
     * Product has many Cruises.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cruises()
    {
        return $this->hasOne(Cruise::class);
    }

    /**
     * Product belongs to Country.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Product belongs to Type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(ProductType::class);
    }

    /**
     * Product has one Itinerary.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function itinerary()
    {
        return $this->belongsTo(ItinerarySection::class);
    }

    /**
     * Product has one Price.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function price()
    {
        return $this->belongsTo(ProductFee::class);
    }

    /**
     * Product belongs to Category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }
}