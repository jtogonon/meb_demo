<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_categories';

    public function supplier()
    {
        //return $this->belongsTo('App\Supplier');
    }
}