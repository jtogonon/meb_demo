<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFee extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_fees';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'price',
        'taxes',
        'user_defined1',
        'user_defined2',
        'total_price',
        'commission',
    ];

    public static function getFee($product_id)
    {
        $product = self::find($product_id);

        return $product->fees;
    }

    public static function getFeesByProductId($product_id)
    {
        $fees = self::where('product_id', $product_id)
            ->get();
        if (count($fees) > 0) {
            return $fees[0]->fees;
        } else {
            return false;
        }
    }

    /**
     * To avoid duplicate items in a booking
     * single instance of a product only
     *
     * @param $product_id
     * @param $operator_id
     * @return mixed
     */
    public static function checkProduct($product_id, $operator_id)
    {
        $products = self::where('product_id', $product_id)
            ->where('operator_id', $operator_id)
            ->get();

        return $products;
    }

    public static function extract_fee($json_str, $keyname)
    {
        if (!empty($json_str)) {
            $json = json_decode($json_str);
            foreach ($json as $key => $val) {
                if ($key == $keyname) {
                    return $val;
                    break;
                }
            }
        }

        return '';
    }
}