<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductItinerarySection extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products_itinerary_sections';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public static function getSection1($operator_id, $product_id)
    {
        $section = self::where('operator_id', ($operator_id))
            ->where('product_id', ($product_id))->get();

        if (count($section) > 0) {
            return $section[0];
        }

        return false;
    }

    public static function getSectionContent($operator_id, $product_id, $section)
    {
        $section = self::where('operator_id', $operator_id)
            ->where('product_id', $product_id)
            ->where('section', $section)
            ->get();
        if (count($section) > 0) {
            return $section[0]->content;
        }

        return '';
    }

    public static function saveSectionContent($operator_id, $product_id, $sectionNum, $content)
    {
        $section = self::where('operator_id', $operator_id)
            ->where('product_id', $product_id)
            ->where('section', $sectionNum)
            ->get();

        if (count($section) > 0) {
            $sectContent = self::find($section[0]->id);
            $sectContent->content = $content;
            $sectContent->save();
        } else {
            $sectContent = new self;
            $sectContent->operator_id = $operator_id;
            $sectContent->product_id = $product_id;
            $sectContent->section = $sectionNum;
            $sectContent->content = $content;
            $sectContent->save();
        }

        return true;
    }
}