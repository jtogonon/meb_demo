<?php namespace App\Providers;

use App\Repositories\DocumentWriter\Invoice;
use App\Repositories\DocumentWriter\ItineraryWordRepository;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;


class ItineraryWordServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('App\Repositories\DocumentWriter\ItineraryWordRepository', function()
		{
			$uri = explode('/', Request::path()); //@TODO refactor Request::segment(2) @Jakob
			$booking_id = $uri[2];
			return new ItineraryWordRepository($booking_id);
		});

		$this->app->bind('App\Repositories\DocumentWriter\Invoice', function()
		{
			$uri = explode('/', Request::path());
			$booking_id = $uri[2];
			return new Invoice($booking_id);
		});
	}

}
