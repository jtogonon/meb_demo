<?php

namespace App\Providers;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeTopPanelNavigation();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Every time elements.top_panel used
     * pass name and breadCrumb for top panel navigation
     */
    public function composeTopPanelNavigation()
    {
        view()->composer('elements.top_panel', function($view){
            $view->with([
                'breadCrumb' => [
                    'Dashboard' => '/',
                    'Customers' => '/customers',
                    'Products' => '/products/index'
                ],
                'name' => Request::segment(1)
            ]);
        });
    }
}
