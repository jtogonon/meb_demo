<?php

namespace App\Repositories;


class FileMakerRepository
{
    public $config = [];
    public $headers = [];
    public $headers_exists = [];
    public $files = [];
    public $boundary;
    public $dir_base;
    public $page_first;

    /**
     * @param $header
     */
    public function SetHeader($header)
    {
        $this->headers[] = $header;
        $key = strtolower(substr($header, 0, strpos($header, ':')));
        $this->headers_exists[$key] = TRUE;
    }

    /**
     * @param $from
     */
    public function SetFrom($from)
    {
        $this->SetHeader("From: $from");
    }

    /**
     * @param $subject
     */
    public function SetSubject($subject)
    {
        $this->SetHeader("Subject: $subject");
    }

    /**
     * @param null $date
     * @param bool $istimestamp
     */
    public function SetDate($date = NULL, $istimestamp = FALSE)
    {
        if ($date == NULL) {
            $date = time();
        }
        if ($istimestamp == TRUE) {
            $date = date('D, d M Y H:i:s O', $date);
        }
        $this->SetHeader("Date: $date");
    }

    /**
     * @param null $boundary
     */
    public function SetBoundary($boundary = NULL)
    {
        if ($boundary == NULL) {
            $this->boundary = '--' . strtoupper(md5(mt_rand())) . '_MULTIPART_MIXED';
        } else {
            $this->boundary = $boundary;
        }
    }

    /**
     * @param $dir
     */
    public function SetBaseDir($dir)
    {
        $this->dir_base = str_replace("\\", "/", realpath($dir));
    }

    /**
     * @param $filename
     */
    public function SetFirstPage($filename)
    {
        $this->page_first = str_replace("\\", "/", realpath("{$this->dir_base}/$filename"));
    }

    /**
     *
     */
    public function AutoAddFiles()
    {
        if (!isset($this->page_first)) {
            exit ('Not set the first page.');
        }
        $filepath = str_replace($this->dir_base, '', $this->page_first);
        $filepath = 'http://mhtfile' . $filepath;
        $this->AddFile($this->page_first, $filepath, NULL);
        $this->AddDir($this->dir_base);
    }

    /**
     * @param $dir
     */
    public function AddDir($dir)
    {
        $handle_dir = opendir($dir);
        while ($filename = readdir($handle_dir)) {
            if (($filename != '.') && ($filename != '..') && ("$dir/$filename" != $this->page_first)) {
                if (is_dir("$dir/$filename")) {
                    $this->AddDir("$dir/$filename");
                } elseif (is_file("$dir/$filename")) {
                    $filepath = str_replace($this->dir_base, '', "$dir/$filename");
                    $filepath = 'http://mhtfile' . $filepath;
                    $this->AddFile("$dir/$filename", $filepath, NULL);
                }
            }
        }
        closedir($handle_dir);
    }

    /**
     * @param $filename
     * @param null $filepath
     * @param null $encoding
     */
    public function AddFile($filename, $filepath = NULL, $encoding = NULL)
    {
        if ($filepath == NULL) {
            $filepath = $filename;
        }
        $mimetype = $this->GetMimeType($filename);
        $filecont = file_get_contents($filename);
        $this->AddContents($filepath, $mimetype, $filecont, $encoding);
    }

    /**
     * @param $filepath
     * @param $mimetype
     * @param $filecont
     * @param null $encoding
     */
    public function AddContents($filepath, $mimetype, $filecont, $encoding = NULL)
    {
        if ($encoding == NULL) {
            $filecont = chunk_split(base64_encode($filecont), 76);
            $encoding = 'base64';
        }
        $this->files[] = ['filepath' => $filepath,
            'mimetype' => $mimetype,
            'filecont' => $filecont,
            'encoding' => $encoding
        ];
    }

    /**
     *
     */
    public function CheckHeaders()
    {
        if (!array_key_exists('date', $this->headers_exists)) {
            $this->SetDate(NULL, TRUE);
        }
        if ($this->boundary == NULL) {
            $this->SetBoundary();
        }
    }

    /**
     * @return bool
     */
    public function CheckFiles()
    {
        if (count($this->files) == 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * @return string
     */
    public function GetFile()
    {
        $this->CheckHeaders();
        if (!$this->CheckFiles()) {
            exit ('No file was added.');
        }
        $contents = implode("\r\n", $this->headers);
        $contents .= "\r\n";
        $contents .= "MIME-Version: 1.0\r\n";
        $contents .= "Content-Type: multipart/related;\r\n";
        $contents .= "\tboundary=\"{$this->boundary}\";\r\n";
        $contents .= "\ttype=\"" . $this->files[0]['mimetype'] . "\"\r\n";
        $contents .= "X-MimeOLE: Produced By Mht File Maker v1.0 beta\r\n";
        $contents .= "\r\n";
        $contents .= "This is a multi-part message in MIME format.\r\n";
        $contents .= "\r\n";
        foreach ($this->files as $file) {
            $contents .= "--{$this->boundary}\r\n";
            $contents .= "Content-Type: $file[mimetype]\r\n";
            $contents .= "Content-Transfer-Encoding: $file[encoding]\r\n";
            $contents .= "Content-Location: $file[filepath]\r\n";
            $contents .= "\r\n";
            $contents .= $file['filecont'];
            $contents .= "\r\n";
        }
        $contents .= "--{$this->boundary}--\r\n";
        return $contents;
    }

    /**
     * @param $filename
     */
    public function MakeFile($filename)
    {
        $contents = $this->GetFile();
        $fp = fopen($filename, 'w');
        fwrite($fp, $contents);
        fclose($fp);
    }

    /**
     * @param $filename
     * @return string
     */
    public function GetMimeType($filename)
    {
        $pathinfo = pathinfo($filename);
        switch ($pathinfo['extension']) {
            case 'htm':
                $mimetype = 'text/html';
                break;
            case 'html':
                $mimetype = 'text/html';
                break;
            case 'txt':
                $mimetype = 'text/plain';
                break;
            case 'cgi':
                $mimetype = 'text/plain';
                break;
            case 'php':
                $mimetype = 'text/plain';
                break;
            case 'css':
                $mimetype = 'text/css';
                break;
            case 'jpg':
                $mimetype = 'image/jpeg';
                break;
            case 'jpeg':
                $mimetype = 'image/jpeg';
                break;
            case 'jpe':
                $mimetype = 'image/jpeg';
                break;
            case 'gif':
                $mimetype = 'image/gif';
                break;
            case 'png':
                $mimetype = 'image/png';
                break;
            case 'pdf':
                $mimetype = 'application/pdf';
                break;
            default:
                $mimetype = 'application/octet-stream';
                break;
        }
        return $mimetype;
    }

    /**
     * @param $html
     * @return mixed
     */
    public function replaceImagePath($html)
    {
        preg_match_all('@<img(.*)?src="([^"]+)"@ui', $html, $matches);

        foreach ($matches[2] as $img) {
            $img_tmp = $img;
            $img_tmp_old = $img;

            if (strpos($img_tmp, "http") === FALSE)
                $img_tmp = url() . $img_tmp;


            //path without domain
            $img_array = explode("//", $img_tmp);
            $img_name_only = $img_array[1];
            $img_name_only = explode("/", $img_name_only);
            unset($img_name_only[0]);
            $img_name_only = implode("/", $img_name_only);
            $img_name_only = rtrim($img_name_only, '"');

            //replace img absolute path with relative path
            $html = str_replace($img_tmp_old, $img_name_only, $html);

            //add img to file
            $this->AddFile($img_tmp, $img_name_only, NULL);
        };

        return $html;

    }

    /**
     * Generate Doc file and save.
     *
     * @param $html
     * @param $name
     */
    public function CreateDOC($html, $name)
    {
        $html = $this->replaceImagePath($html);

        $this->AddContents("index.html", "text/html", $html);

        $this->MakeFile($name);
    }


    /*********************************/


    /**
     *
     *
     * @param $filename
     * @param string $charset
     */
    public function send_download($filename, $charset = 'cp1251')
    {

        header($_SERVER["SERVER_PROTOCOL"] . ' 200 OK');

        if (ereg('Opera(/| )([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT'])) {
            $UserBrowser = "Opera";
        } elseif (ereg('MSIE ([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT'])) {
            $UserBrowser = "IE";
        } else {
            $UserBrowser = '';
        }

        $mime_type = ($UserBrowser == 'IE' || $UserBrowser == 'Opera') ? 'application/octetstream' : 'application/octet-stream';

        header("Content-Type: application/msword; charset=" . $charset);

        $ua = (isset($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : '';

        $isMSIE = preg_match('@MSIE ([0-9].[0-9]{1,2})@', $ua);
        if ($isMSIE) {
            header('Content-Disposition: attachment; filename="' . $filename . '"');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
        } else {
            header('Content-Disposition: attachment; filename="' . $filename . '"');
            header('Pragma: no-cache');
        }
    }
}
