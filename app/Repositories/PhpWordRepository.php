<?php namespace App\Repositories;

use Illuminate\Support\Facades\View;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\Media;
use PhpOffice\PhpWord\Shared\Html;
use PhpOffice\PhpWord\Element\Image;


class PhpWordRepository
{
    public $phpWord;

    public $fileMaker;

    private $section;

    public function __construct()
    {
        $this->phpWord = new PhpWord();
        $this->fileMaker = new FileMakerRepository();
    }

    public function createHTML($html)
    {
        $this->createSection();

        return Html::addHtml($this->section, $html, true);
    }

    public function createSection()
    {
        return $this->section = $this->phpWord->addSection();
    }

    public function addTextToSection($text, $style = [])
    {
        if (!$style) {
            return $this->section->addText(htmlspecialchars($text));
        }

        return $this->section->addText(htmlspecialchars($text), []);
    }

    private function saveWord($name, $version = 'Word2007')
    {
        $objWriter = IOFactory::createWriter($this->phpWord, $version);
        return $objWriter->save($name . '.docx');
    }

    public function getTemplate1()
    {
//        $this->createSection();
//
//        $src = 'documents/templates/template1/images/image.jpg';
//        $type = 'image';
//        $image = new Image($src);

        $section = $this->phpWord->addSection();
        $header = $section->addHeader();
        $header->addWatermark('documents/templates/template1/images/image.jpg',
            [
                'width' => 400,
                'height' => 50,
                'marginTop' => -1000,
                'marginLeft' => -1000
            ]
        );

//        $section = $this->phpWord->addSection();


        Html::addHtml($section, '<div class="header">
            <div class="day">Day 1</div>
            <div class="date">Thursday, 15 January 2015</div>
            <div class="clear"></div>
        </div>');

        $section->addLine(['weight' => 1, 'width' => 570, 'height' => 0, 'color' => 'blue']);

        Html::addHtml($section, '<div class="block">
            <div class="hour">09:30</div>
            <div class="info">
                Intermediate 4WD, Toyota Rav4 or similar (IFBV/IFAR), NIL Accident
                <br/>Damage Excess, 7-13 Days
                <br/>Pick up: Thursday, 15 January 2015
                <br/>Drop off: Thursday, 22 January 2015
            </div>
            <div class="pic"></div>
            <div class="clear"></div>
        </div>');


//        $section = $this->phpWord->addSection();
//        $section->addImage(
//            'documents/templates/template1/images/image.jpg',
//            [
//                'width' => 400,
//                'height' => 50,
//                'marginTop' => 1000,
//                'marginLeft' => 1000,
//                'wrappingStyle' => 'behind'
//            ]
//        );

//        Media::addSectionMediaElement($src, $type, $image);

        $this->saveWord('documents/results/template1');
    }
}