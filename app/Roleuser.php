<?php namespace App;

//use Zizaco\Entrust\EntrustRole;
use Illuminate\Database\Eloquent\Model;

class Roleuser extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'role_user';

    public $timestamps = false;

    public function setUpdatedAt($value)
    {
        $this->{static::UPDATED_AT} = $value;
    }
}