<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomCategory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'room_categories';

    protected $primaryKey = 'id';
}