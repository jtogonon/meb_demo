<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subscriptions';

    /**
     * Fillable fields for model.
     *
     * @var array
     */
    protected $fillable = [
        'operator_id',
        'monthly_plan',
        'payment_plan',
        'payment_method',
        'last_payment_received',
        'next_due_date'
    ];

    public static $rules = [
        'monthly_plan' => 'required',
        'payment_plan' => 'required',
        'payment_method' => 'required',
        'last_payment_received' => '',
        'next_due_date' => '',
    ];
}