<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'suppliers';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'website',
        'tax_scheme',
        'logo',
        'contact_name',
        'phone',
        'email_sales',
        'email_invoice',
        'street',
        'city',
        'state',
        'company_description',
        'user_id',
        'country_id'
    ];

    /**
     * Supplier has many products.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * Supplier belongs to user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param $operator_id
     * @return mixed
     */
    public static function getSuppliersByOperatorId($operator_id)
    {
        $suppliers = self::select('*')
            ->where('user_id', $operator_id)
            ->orderBy('name', 'ASC')
            ->get();

        return $suppliers;
    }
}
