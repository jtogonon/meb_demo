<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'operator_id',
        'first_name',
        'last_name',
        'email',
        'password',
        'gender',
        'date_of_birth',
        'nationality_id',
        'phone_home',
        'phone_mobile',
        'lead_customer',
        'notes',
        'prefix',
        'middle_name',
        'suffix'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $rules = [
        'first_name' => 'required|regex:/^[a-z][a-z ]*$/i',
        'last_name' => 'required|regex:/^[a-z][a-z ]*$/i',
        'password' => 'required',
        'email' => 'email',
        'date_of_birth' => 'required',
        'phone_home' => 'required',
        'phone_mobile' => 'required',
        'notes' => 'required',
    ];

    /**
     * Get owners(operator owner, operator, agent owner, agent) customers.
     *
     * @param $user_id
     * @return array
     */
    public static function getCustomersByOwnerId($user_id)
    {
        $customer_ids = DB::table('client_owner')
            ->select('client_id')
            ->where('owner_id', $user_id)
            ->where('relation_id', 4)
            ->orWhere('relation_id', 7)
            ->orWhere('relation_id', 9)
            ->orWhere('relation_id', 10)
            ->get();

        $customers = [];
        foreach ($customer_ids as $customer_id) {
            $customers[] = User::find($customer_id->client_id);
        }

        return $customers;
    }

    public function getStreetAddress()
    {
        if ($this->address) {
            return $this->address->street;
        }

        return '';
    }

    public function getState()
    {
        if ($this->address) {
            return $this->address->state;

        }

        return '';
    }

    public function getCountry()
    {
        if ($this->address) {
            return $this->address->getCountry(true); // will return shortname
        }

        return '';
    }

    public function getCountryId()
    {
        if ($this->address) {
            return $this->address->getCountryId(); // will return shortname
        }

        return '0';
    }

    public static function getCustomersByOperatorId($user_id)
    {//@TODO replace this in all files with getCustomersByOwnerId @Jakob
        $customers = User::select(
            'users.id',
            'users.first_name',
            'users.last_name',
            'users.gender',
            'users.email',
            'users.mobile_phone',
            'users.home_phone'
        )
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.role_id', 3)
            ->where('users.id', $user_id)
            ->orderBy('first_name', 'ASC')
            ->get();

        return $customers;
    }

    public static function getEmailNameById($user_id)
    {
        $user = self::find($user_id);

        return ['email' => $user->email, 'name' => $user->first_name . ' ' . $user->last_name];
    }

    public static function getEmailsId($user_id)
    {
        $user = self::find($user_id);

        return $user->email;
    }

    /**
     * User has many products.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * User has many bookings.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    /**
     * User has many Passengers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function passengers()
    {
        return $this->hasMany(Passenger::class);
    }

    /**
     * User has many Supplier.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function suppliers()
    {
        return $this->hasMany(Supplier::class);
    }

    /**
     * User has many Roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userroles()
    {
        return $this->hasMany(Roleuser::class);
    }

    /**
     * User has one Address.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function address()
    {
        return $this->hasOne(Address::class);
    }

    /**
     * User has many custom fields.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function customFields()
    {
        return $this->hasMany(CustomField::class);
    }

}
