<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VacationPackage extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'vacation_packages';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_date',
        'end_date',
        'supplier_id',
        'product_id',
    ];

}
