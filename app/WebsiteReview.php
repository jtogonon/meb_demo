<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteReview extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'website_reviews';

    protected $fillable = [
        'user_id',
        'company',
        'website',
        'rating',
        'comment',
        'created_at',
        'updated_at'
    ];
}