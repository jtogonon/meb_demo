<?php

/**
 * Convert base64 to jpeg.
 *
 * @param $base64_string
 * @param $output_file
 * @return mixed
 */
function base64_to_jpeg($base64_string, $output_file)
{
    $ifp = fopen($output_file, "wb");
    fwrite($ifp, base64_decode($base64_string));
    fclose($ifp);

    return $output_file;
}

/**
 * Convert date to human friendly.
 *
 * @param $date
 * @param null $short
 * @return bool|string
 */
function date_to_human_friendly($date, $short = null)
{
    if (is_string($date)) {
        $date = strtotime($date);
    }

    if($short){
        return date('l, d F', $date);
    }

    return date('l, d F Y', $date);
}

/**
 * Get picture url from WYISWYG editor.
 *
 * @param $content
 * @param $outputFile
 * @return mixed|string
 */
function get_picture_from_wysiwyg($content, $outputFile)
{
    $pattern_img_src = '/src="([^"]*)"/i';
    $pattern_base64 = '/data:(?<mime>[\w\/\-\.]+);(?<encoding>\w+),(?<data>.*)"\sdata/';
    $picture = '';
    if (preg_match_all($pattern_base64, $content, $matches)) {
        $base64 = $matches['data'][0];
        $picture = base64_to_jpeg($base64, $outputFile . '.jpg');
    } elseif (preg_match_all($pattern_img_src, $content, $matches)) {
        $picture = $matches[1][0];
    }

    return $picture;
}

/**
 * Get number of days between two dates.
 *
 * @param $start_date
 * @param $end_date
 * @return float
 */
function get_days_between_dates($start_date, $end_date)
{
    $start_date = strtotime($start_date);
    $end_date = strtotime($end_date);
    $date_diff = $end_date - $start_date;
    return intval(floor($date_diff/(60*60*24)));
}

