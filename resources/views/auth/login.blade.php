@extends('layout_login')

@section('content')
    <form action="#" id="loginForm" action="{{ url('/login') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label class="control-label" for="username">Email</label>
            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
            <span class="help-block small">Your email address</span>
        </div>
        <div class="form-group">
            <label class="control-label" for="password">Password</label>
            <input type="password" class="form-control" name="password">
            <span class="help-block small">Your strong password</span>
        </div>
        <div class="checkbox">
            <input type="checkbox" class="i-checks" checked>
            Remember login
            <p class="help-block small">(if this is a private computer)</p>
        </div>
        <button class="btn btn-success btn-block">Login</button>
        <a class="btn btn-default btn-block" href="/register">Register</a>
    </form>
@stop
