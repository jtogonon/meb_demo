@extends('landing2')

@section('content-home')
    <header id="home">
        <div class="container">
            <div class="heading">
                <h1>
                    Booking System and Back Office for your Travel Business
                </h1>
                <span>$25US /month</span>
                <p>
                    <b><a href="#" style="color: #edf0f5">Free Trial</a></b>
                    <span style="margin-left: 10px;">No Payment info required</span>
                </p>
                <a href="#" class="btn btn-success btn-sm">Learn more</a>
            </div>
            <div class="heading-image animate-panel" data-child="img-animate" data-effect="fadeInRight">
                <p class="small"></p>
                <img class="img-animate" src="images/landing/c1.jpg">
                <img class="img-animate" src="images/landing/c2.jpg">
                <img class="img-animate" src="images/landing/c3.jpg">
                <img class="img-animate" src="images/landing/c4.jpg">
                <br/>
                <img class="img-animate" src="images/landing/c5.jpg">
                <img class="img-animate" src="images/landing/c6.jpg">
                <img class="img-animate" src="images/landing/c7.jpg">
                <img class="img-animate" src="images/landing/c8.jpg">
            </div>
        </div>
    </header>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h4>HTML5 & CSS3</h4>
                    <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh
                        ultricies vehicula ut id elit. Morbi leo risus.</p>
                    <p><a class="navy-link btn btn-sm" href="#" role="button">Learn more</a></p>
                </div>
                <div class="col-md-4">
                    <h4>Staggering Animations</h4>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page
                        when looking at its layout. The point of.</p>
                    <p><a class="navy-link btn btn-sm" href="#" role="button">Learn more</a></p>
                </div>
                <div class="col-md-4">
                    <h4>Unique Dashboard</h4>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered
                        alteration in some form, by injected humour.</p>
                    <p><a class="navy-link btn btn-sm" href="#" role="button">Learn more</a></p>
                </div>
            </div>
        </div>
    </section>
@stop