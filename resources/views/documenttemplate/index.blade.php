@extends('layout')

@section('content')
    <div class="content animate-panel">
        <div class="row">
            <table id="tbl_templates" class="table table-striped table-bordered table-hover dataTable no-footer"
                   role="grid" aria-describedby="example2_info">
                <thead>
                <tr role="row">
                    <th width="10%">ID</th>
                    <th>Name</th>
                    <th>Filename</th>
                    <th>Last Updated</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($templates as $dtpl)
                    <tr role="row" class="odd">
                        <td><a href="/documenttemplate/view/{{ $dtpl->id }}">{{ $dtpl->id }}</a></td>
                        <td><a href="/documenttemplate/view/{{ $dtpl->id }}">{{ $dtpl->name }}</a></td>
                        <td><a href="/documenttemplate/view/{{ $dtpl->id }}">{{ $dtpl->filename }}</a></td>
                        <td>{{ $dtpl->updated_at }}</td>
                        <td><a href="/documenttemplate/delete/{{ $dtpl->id }}" title="Delete"
                               onclick="return confirm('Are you sure you want to delete?');">
                                <i class="pe-7s-close-circle"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop