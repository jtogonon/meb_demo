<ul class="sidebar-nav" style="margin-bottom: 50px;">
    <li class="active">
        <a href="/"> <span class="nav-label">Home</span></a>
    </li>
    <li>
        <label>MEB Booking System</label>
        <ul class="nav nav-second-level">
            <li><a href="/key-features">Key Features</a></li>
            <li><a href="/pricing">Pricing</a></li>
            <li><a href="/modules">Modules</a></li>
            <li><a href="/web-integration">Website Integration</a></li>
            <li><a href="/accounting-system-integration">Account System Integration</a></li>
            <li><a href="/reviews">Reviews</a></li>
        </ul>
    </li>
    <li>
        <label>Web Design</label>
        <ul class="nav nav-second-level">
            <li><a href="#">Pricing</a></li>
            <li><a href="#">Web Design Templates</a></li>
            <li><a href="#">Reviews</a></li>
        </ul>
    </li>
    <li>
        <label>Support</label>
        <ul class="nav nav-second-level">
            <li><a href="#">Forum</a></li>
            <li><a href="#">Blog</a></li>
        </ul>
    </li>
    <li>
        <a href="/"> <span class="nav-label">Contact Us</span></a>
    </li>
    <li>
        <label>Online Chat</label>
        <ul class="nav nav-second-level">
            <li><a href="#">Talk to one of our Agents</a></li>
        </ul>
    </li>
    <li>
        <a href="/login"> <span class="nav-label">Login</span></a>
    </li>
    <li>
        <a href="/"> <span class="nav-label">Free Trial</span></a>
    </li>
</ul>