<div id="navigation">
    <div class="profile-picture">
        <a href="index.html">
            <img src="/images/profile.jpg" class="img-circle m-b" alt="logo">
        </a>

        <div class="stats-label text-color">
            <span class="font-extra-bold font-uppercase">Robert Razer</span>

            <div class="dropdown">
                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                    <small class="text-muted">Founder of App <b class="caret"></b></small>
                </a>
                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                    <li><a href="contacts.html">Contacts</a></li>
                    <li><a href="profile.html">Profile</a></li>
                    <li><a href="analytics.html">Analytics</a></li>
                    <li class="divider"></li>
                    <li><a href="login.html">Logout</a></li>
                </ul>
            </div>


            <div id="sparkline1" class="small-chart m-t-sm"></div>
            <div>
                <h4 class="font-extra-bold m-b-xs">
                    $260 104,200
                </h4>
                <small class="text-muted">Your income from the last year in sales product X.</small>
            </div>
        </div>
    </div>

    <ul class="nav" id="side-menu">
        <li class="active">
            <a href="{{ url('dashboard') }}"> <span class="nav-label">Dashboard</span> <span class="label label-success pull-right">v.1</span>
            </a>
        </li>

        <li>
            <a href="{{ url('booking/create') }}"> <span class="nav-label">Bookings</span><span
                        class="label label-warning pull-right">NEW</span> </a>
        </li>

        <li>
            <a href="/customers/index"> <span class="nav-label">Customers</span> </a>
        </li>


        <li>
            <a href="{{ url('/supplier') }}"> <span class="nav-label">Suppliers</span></a>

        </li>
        <li>
            <a href="/inquiries/index"> <span class="nav-label">Inquiries</span><span
                        class="label label-warning pull-right">NEW</span> </a>
        </li>
        {{--<li>
            <a href="/emails/index"> <span class="nav-label">Emails</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li><a href="/emailtemplates/index">Email Templates</a></li>
            </ul>
        </li>--}}
        <li>
            <a href="/reports/index"><span class="nav-label">Reports</span><span class="fa arrow"></span> </a>
            <ul class="nav nav-second-level">
                <li><a href="contacts.html">Contacts</a></li>
                <li><a href="projects.html">Projects</a></li>
                <li><a href="project.html">Project detail</a></li>
                <li><a href="social_board.html">Social board</a></li>
                <li><a href="timeline.html">Timeline</a></li>
                <li><a href="profile.html">Profile</a></li>
                <li><a href="mailbox.html">Mailbox</a></li>
            </ul>
        </li>
        <li>
            <a href="/administrator"><span class="nav-label">Administrator</span></a>
            {{--<ul class="nav nav-second-level">
                <li><a href="/settings/emails">Email Settings</a></li>
                <li><a href="/settings/customfields">Custom Fields</a></li>
            </ul>--}}
        </li>

        <li>
            <a href="/webintegrations/index"> <span class="nav-label">Website Intregration</span><span
                        class="label label-warning pull-right">NEW</span> </a>
        </li>
    </ul>
</div>