<div class="normalheader transition animated fadeIn small-header">
    <div class="hpanel">
        <div class="panel-body">
            <div class="pull-right">
                <a class="btn btn-default btn-sm" href="{{ url($name . '/create') }}">Add {{ ucfirst($name) }}</a>
            </div>
            <h2 class="font-light m-b-xs">
                {{ ucfirst($name) }}
            </h2>
            <div id="hbreadcrumb">
                <ol class="hbreadcrumb breadcrumb">
                    @foreach ($breadCrumb as $menu => $link)
                        @if ($link == '')
                            <li>{{ $menu }}</li>
                        @else
                            <li><a href="{{ $link }}">{{ $menu }}</a></li>
                        @endif
                    @endforeach
                </ol>
            </div>
        </div>
    </div>
</div>