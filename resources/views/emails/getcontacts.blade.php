<div class="modal-content">
    <div class="color-line"></div>
    <div class="modal-header">
        <h2 class="modal-title">Contacts</h2>
        <small class="font-bold">Email</small>
    </div>
    <div class="modal-body" style="min-height:100px;" id="email_popup_modalbody">
        contact list
    </div>
    <div class="clear"></div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="btn_send_email">Send Email</button>
    </div>

</div>