{!! Form::model(new App\Email, ['url' => '/emails/send', 'method' => 'post', 'class'=>'form-horizontal', 'id'=>'form_popup_email']) !!}
<div class="modal-content">
    <div class="modal-header col-lg-12 col-md-12 col-xs-12">
        <div class="pull-left padding-left-30">
            <h2 class="modal-title">Email - Booking ID {{ $booking_id }}</h2>
            <small class="font-bold">Email</small>
        </div>
        <button type="button" class="pull-right close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body" style="min-height:500px;" id="email_popup_modalbody">

        <input type="hidden" name="booking_id" value="{{ $booking_id }}">
        <div class="col-lg-12">
            @if(isset($email_from))
                <div class="form-group">
                    {!! Form::label('email_from', 'From:', ['class'=>'col-sm-2 control-label disabled']) !!}
                    <div class="col-sm-8">
                        <div class="col-md-12">
                            {!! Form::text('email_from', $email_from, ['id'=>'email_from', 'class'=>'form-control input-sm']) !!}
                        </div>
                    </div>
                    <div class="col-sm-2">

                    </div>
                </div>
            @endif
            <div class="form-group">
                {!! Form::label('email_to', 'To:', ['class'=>'col-sm-2 control-label']) !!}
                <div class="col-sm-8">
                    <div class="col-md-12">
                        {!! Form::select('email_to[]', $email_to, '', ['id'=>'email_to', 'class'=>'js-source-states-2', "multiple"=>"multiple", "style"=>"width: 100%"]) !!}
                    </div>
                </div>
                <div class="col-sm-2">

                </div>
            </div>
            <div class="form-group" id="add_cc_label">
                <label class='col-sm-2 control-label'>&nbsp;</label>
                <div class="col-sm-8">
                    <div class="col-md-12"><a href="" class="btn btn-default btn-xs">Add CC</a></div>
                </div>
            </div>
            <div class="form-group" id="add_cc_field" style="display:none;">
                {!! Form::label('from', 'CC:', ['class'=>'col-sm-2 control-label']) !!}
                <div class="col-sm-8">
                    <div class="col-md-12">
                        {!! Form::select('email_cc[]', $email_to, '', ['id'=>'email_cc', 'class'=>'js-source-states-2', "multiple"=>"multiple", "style"=>"width: 100%"]) !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <a href="" class="btn btn-default btn-xs" id="add_cc_remove">x</a>
                </div>
            </div>
            <div class="form-group" id="add_bcc_label">
                <label class='col-sm-2 control-label'>&nbsp;</label>
                <div class="col-sm-8">
                    <div class="col-md-12"><a href="" class="btn btn-default btn-xs">Add BCC</a></div>
                </div>
            </div>
            <div class="form-group" id="add_bcc_field" style="display:none;">
                {!! Form::label('from', 'Bcc:', ['class'=>'col-sm-2 control-label']) !!}
                <div class="col-sm-8">
                    <div class="col-md-12">
                        {!! Form::select('email_bcc[]', $email_to, '', ['class'=>'js-source-states-2', "multiple"=>"multiple", "style"=>"width: 100%"]) !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <a href="" class="btn btn-default btn-xs" id="add_bcc_remove">x</a>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('template_id', 'Email Template:', ['class'=>'col-sm-2 control-label']) !!}
                <div class="col-sm-8">
                    <div class="col-md-12">
                        {!! Form::select('template_id', $templates, '', ['class'=>'form-control input-sm']) !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    &nbsp;
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('email_subject', 'Subject:', ['class'=>'col-sm-2 control-label']) !!}
                <div class="col-sm-8">
                    <div class="col-md-12">
                        {!! Form::text('email_subject', 'subject here', ['class'=>'form-control input-sm', 'rows'=>'4']) !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    &nbsp;
                </div>
            </div>

        </div>
        <hr>
        <div class="col-lg-12 ">
            {!! Form::label('email_body', 'Message:', ['class'=>'col-sm-2 control-label']) !!}
            <br/><br/>
                <textarea class="summernote" id="email_body" name="email_body" style="min-height:400px;">
                </textarea>

        </div><!-- end col-lg-12 -->

        <div class="clearfix"></div>

    </div>
    <div class="clear"></div>
    <div class="modal-footer navbar-fixed-bottom">
        <button type="button" class="btn btn-white pull-left close" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-white pull-right" id="btn_send_email">Save</button>
    </div>

</div>
{!! Form::close() !!}

<style>
    .select2-container-multi .select2-choices {
        border-color: #e4e5e7 !important;
        border-radius: 3px;
    }
</style>