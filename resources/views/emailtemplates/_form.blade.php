@if(isset($template))
    {!! Form::hidden('id', $template->id) !!}
@endif

<div class="form-group">
    {!! Form::label('short_name', 'Short Name:',['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-8">
        <div class="col-md-4">
            {!! Form::text('short_name',  old('short_name'),['id'=>'short_name','class'=>'form-control',]) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('subject', 'Subject:',['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-8">
        <div class="col-md-4">
            {!! Form::text('subject',  old('subject'), ['id'=>'short_name', 'class'=>'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('html_body', 'HTML Body:', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-8">
        <div class="col-md-12">
            {!! Form::textarea('html_body', old('html_body'), ['class'=>'form-control txtDropTarget', 'rows'=>5, 'style'=>"min-height:400px;"]) !!}
        </div>
    </div>
    <div class="col-sm-2">
        <p>Drag & Drop Variables in the Email editor</p>
        <ul id="emailVars">
            @foreach ($emailVars as $v)
                <li><code>{ {{ $v }} }</code></li>
            @endforeach
        </ul>
    </div>
</div>

<div class="form-group">
    {!! Form::label('text_body', 'Text Body:', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-8">
        <div class="col-md-12">
            {!! Form::textarea('text_body', old('text_body'), ['class'=>'form-control', 'rows'=>5]) !!}
        </div>
    </div>
</div>


<div class="form-group">
    <label class="control-label col-sm-2"></label>
    <div class="col-sm-10">
        {!! Form::submit($submit_text, ['class'=>'btn w-xs btn-primary']) !!}
        {!! link_to_action('EmailTemplatesController@index', $title = '[Cancel]') !!}
    </div>
</div>

<style>
    UL#emailVars li {
        cursor: pointer;
    }
</style>