@extends('layout')

@section('content')
    <div class="content animate-panel">
        <div class="row">
            <table id="tbl_templates" class="table table-striped table-bordered table-hover dataTable no-footer"
                   role="grid" aria-describedby="example2_info">
                <thead>
                <tr role="row">
                    <th width="10%">ID</th>
                    <th>Short Name</th>
                    <th>Subject</th>
                    <th>Last Updated</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($templates as $etpl)
                    <tr role="row" class="odd">
                        <td><a href="/emailtemplates/view/{{ $etpl->id }}">{{ $etpl->id }}</a></td>
                        <td><a href="/emailtemplates/view/{{ $etpl->id }}">{{ $etpl->short_name }}</a>
                        </td>
                        <td>{{ $etpl->subject }}</td>
                        <td>{{ $etpl->updated_at }}</td>
                        <td><a href="/emailtemplates/delete/{{ $etpl->id }}" title="Delete"
                               onclick="return confirm('Are you sure you want to delete?');">
                                <i class="pe-7s-close-circle"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('footerJS')
    <script>
        $('#tbl_risks').dataTable();
    </script>
@stop