@extends('layout')

@section('content')
    <div class="content animate-panel">
        <div class="hpanel">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    <a class="closebox"><i class="fa fa-times"></i></a>
                </div>
                View Email Template
            </div>
            <div class="panel-body">
                {!! Form::model($template, ['url' => 'emailtemplates/save', 'method' => 'post', 'class'=>'form-horizontal']) !!}
                @include('emailtemplates/_form', ['submit_text' => 'Save Email Template'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop