<div class="col-lg-12">
    <div class="hpanel hblue">
        <div class="panel-heading">
            <div class="panel-tools">
                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
            </div>
            Email Notification
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    {!! Form::label('email', 'Main', ['style' => 'margin-right:20px;']) !!}
                    {!! Form::text('email', '', ['class' => 'form-control input-sm', 'style' => 'width: 78%; display: inline;']) !!}
                </div>
                <div class="col-md-6">
                    <span>All enquiries will be sent to this email address</span>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="col-md-6">
                    {!! Form::label('copy', 'Copy', ['style' => 'margin-right:20px;']) !!}
                    {!! Form::text('copy', '', ['class' => 'form-control input-sm', 'style' => 'width: 78%; display: inline;']) !!}
                </div>
                <div class="col-md-6">
                    <span>These email addresses will be copied on enquiry notifications. Multiple email addresses are separated by a semi-colon (;)</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12">
    <div class="hpanel hblue">
        <div class="panel-heading">
            <div class="panel-tools">
                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
            </div>
            Auto Respond
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-1" style="margin-right: 15px;"></div>
                <div class="col-md-4">
                    {!! Form::radio('auto', '0', true, ['style' => 'margin-top: -1px; vertical-align: middle', 'id' => 'rad_no']) !!}
                    {!! Form::label('email', 'No', ['style' => 'margin-right:20px;']) !!}
                    {!! Form::radio('auto', '1', false, ['style' => 'margin-top: -1px; vertical-align: middle','id' => 'rad_yes']) !!}
                    {!! Form::label('email', 'Yes', ['style' => 'margin-right:20px;']) !!}
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="col-md-7">
                    {!! Form::label('sent_from', 'Sent from', ['style' => 'margin-right:20px;']) !!}
                    {!! Form::text('sent_from', '', ['class' => 'form-control input-sm disabled_form', 'style' => 'width: 78%; display: inline;','disabled']) !!}
                </div>
                <div class="col-md-5">
                    <span>Auto respond will be send from this email</span>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="col-md-7">
                    {!! Form::label('subject', 'Subject line', ['style' => 'margin-right:9px;']) !!}
                    {!! Form::text('subject', '', ['class' => 'form-control input-sm disabled_form', 'style' => 'width: 78%; display: inline;','disabled']) !!}
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="form-group">
                    {!! Form::label('body', 'Body', ['class' => 'col-sm-1', 'style' => 'margin-right:6px;']) !!}
                    <div class="col-sm-8">
                        {!! Form::textarea('body', '', ['class' => 'form-control input-sm disabled_form', 'disabled']) !!}
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="col-md-6">
                    {!! Form::label('attachment', 'Attach a file', ['style' => 'margin-right:7px;']) !!}
                    {!! Form::file('attachment', ['class' => 'form-control input-sm disabled_form', 'style'=>'width:50%;display:inline', 'disabled']) !!}
                </div>
            </div>
        </div>

    </div>
</div>

<div class="text-right m-t-xs">
    <a class="btn btn-primary btn-save-booking" href="#">Save Changes</a>
</div>

<script>
    $('#rad_yes').click(function(){
        $('.disabled_form').removeAttr('disabled');
    });
    $('#rad_no').click(function(){
        for(var i = 0; i < $('.disabled_form').length; i++) {
            document.getElementsByClassName('disabled_form')[i].setAttribute('disabled',true);
        }
    });
</script>
