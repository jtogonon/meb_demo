<div class="row" style="padding-left: 35px;">
    <input type="radio" name="choice" id="choice_no" style="margin-top: -1px; vertical-align: middle" value="0"
           checked/>
    <label style="margin-right:20px;">No</label>

    <input type="radio" name="choice" id="choice_yes" style="margin-top: -1px; vertical-align: middle" value="1"/>
    <label style="margin-right:20px;">Yes</label><br/>
</div>
<div class="col-lg-3">
    <div class="hpanel hblue">
        <div class="panel-heading">
            <div class="panel-tools">
                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
            </div>
            Customize your form
        </div>
        <div class="panel-body">
            <input type="checkbox" id="welcome_note_check" class="options_toggle"
                   style="margin-top: -1px; vertical-align: middle"/>
            <label style="margin-right:20px;">Welcome Note</label><br/>

            <input type="checkbox" id="name_check" class="options_toggle"
                   style="margin-top: -1px; vertical-align: middle" checked/>
            <label style="margin-right:20px;">Name</label><br/>

            <input type="checkbox" id="phone_number_check" class="options_toggle"
                   style="margin-top: -1px; vertical-align: middle" checked/>
            <label style="margin-right:20px;">Call back number</label><br/>

            <input type="checkbox" id="email_check" class="options_toggle"
                   style="margin-top: -1px; vertical-align: middle" checked/>
            <label style="margin-right:20px;">Email Address</label><br/>

            <input type="checkbox" id="message_check" class="options_toggle"
                   style="margin-top: -1px; vertical-align: middle" checked/>
            <label style="margin-right:20px;">Message</label><br/>

            <input type="checkbox" id="address1_check" class="options_toggle"
                   style="margin-top: -1px; vertical-align: middle"/>
            <label style="margin-right:20px;">Address1</label><br/>

            <input type="checkbox" id="address2_check" class="options_toggle"
                   style="margin-top: -1px; vertical-align: middle"/>
            <label style="margin-right:20px;">Address2</label><br/>

            <input type="checkbox" id="city_check" class="options_toggle"
                   style="margin-top: -1px; vertical-align: middle"/>
            <label style="margin-right:20px;">City</label><br/>

            <input type="checkbox" id="state_check" class="options_toggle"
                   style="margin-top: -1px; vertical-align: middle"/>
            <label style="margin-right:20px;">State</label><br/>

            <input type="checkbox" id="zip_code_check" class="options_toggle"
                   style="margin-top: -1px; vertical-align: middle"/>
            <label style="margin-right:20px;">Zip code</label><br/>

            <input type="checkbox" id="country_check" class="options_toggle"
                   style="margin-top: -1px; vertical-align: middle"/>
            <label style="margin-right:20px;">Country</label><br/>

            <input type="checkbox" id="custom_check" class="options_toggle"
                   style="margin-top: -1px; vertical-align: middle"/>
            <label style="margin-right:20px;">Custom Field</label><br/>

        </div>
    </div>
</div>

<div class="col-lg-9">
    <div class="hpanel hblue">
        <div class="panel-heading">
            <div class="panel-tools">
                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
            </div>
            Preview
        </div>
        <div class="panel-body" id="preview_body">

            <div class="row">
                <div class="form-group" id="name_container">
                    <label for="name" class="col-md-2 control-label input-sm">Name</label>
                    <div class="col-md-6">
                        {!! Form::text('name', '', ['class' => 'form-control input-sm disabled_preview', 'style' => 'width: 78%; display: inline;','disabled']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group" id="phone_number_container">
                    <label for="phone_number" class="col-md-2 control-label input-sm">Phone Number</label>
                    <div class="col-md-6">
                        {!! Form::text('phone_number', '', ['class' => 'form-control input-sm disabled_preview', 'style' => 'width: 78%; display: inline;','disabled']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group" id="email_container">
                    <label for="email" class="col-md-2 control-label input-sm">Email Address</label>
                    <div class="col-md-6">
                        {!! Form::text('email', '', ['class' => 'form-control input-sm disabled_preview', 'style' => 'width: 78%; display: inline;','disabled']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group" id="message_container">
                    <label for="message" class="col-md-2 control-label input-sm">Message</label>
                    <div class="col-md-10">
                        {!! Form::textarea('message', '', ['class' => 'form-control input-sm disabled_preview', 'style' => 'width: 78%; display: inline;','disabled']) !!}
                    </div>
                </div>
            </div>

            <div class="text-right m-t-xs" style="margin-top: 20px;">
                <a class="btn btn-primary btn-save-inquiry" href="#" disabled>Save Changes</a>
            </div>
        </div>
    </div>
</div>