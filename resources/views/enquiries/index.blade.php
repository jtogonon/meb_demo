@extends('layout')

@section('content')

    <div class="content animate-panel">
        <div class="hpanel">

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#create-form">Enquiry Form Creation</a></li>
                <li class=""><a data-toggle="tab" href="#notification" id="tab-quotes">Notification</a></li>
            </ul>

            <div class="tab-content">
                <div id="create-form" class="tab-pane active">
                    <div class="panel-body">
                        {!! Form::open(['url' => 'enquiries/create', 'method' => 'post', 'id' => 'create_inquiry']) !!}
                        @include('enquiries/_form_create')
                        {!! Form::close() !!}
                    </div>
                </div>

                <div id="notification" class="tab-pane">
                    <div class="panel-body">
                        {!! Form::open(['url' => 'enquiries/save', 'method' => 'post']) !!}
                        @include('enquiries/_form')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div><!-- end hpanel -->
    </div>
@stop
