<div class="gridster" style="height: 761px; width: 608px; position: relative;">
    <ul>
        <li class="gridster_element" data-row="1" data-col="1" data-sizex="1" data-sizey="1"></li>
        <li class="gridster_element" data-row="2" data-col="1" data-sizex="1" data-sizey="1"></li>
        <li class="gridster_element" data-row="3" data-col="1" data-sizex="1" data-sizey="1"></li>
        <li class="gridster_element" data-row="4" data-col="1" data-sizex="1" data-sizey="1"></li>
        <li class="gridster_element" data-row="5" data-col="1" data-sizex="1" data-sizey="1"></li>

        <li class="gridster_element" data-row="1" data-col="2" data-sizex="1" data-sizey="1"></li>
        <li class="gridster_element" data-row="2" data-col="2" data-sizex="1" data-sizey="1"></li>
        <li class="gridster_element" data-row="3" data-col="2" data-sizex="1" data-sizey="1"></li>
        <li class="gridster_element" data-row="4" data-col="2" data-sizex="1" data-sizey="1"></li>
        <li class="gridster_element" data-row="5" data-col="2" data-sizex="1" data-sizey="1"></li>

        <li class="gridster_element" data-row="1" data-col="3" data-sizex="1" data-sizey="1"></li>
        <li class="gridster_element" data-row="2" data-col="3" data-sizex="1" data-sizey="1"></li>
        <li class="gridster_element" data-row="3" data-col="3" data-sizex="1" data-sizey="1"></li>
        <li class="gridster_element" data-row="4" data-col="3" data-sizex="1" data-sizey="1"></li>
        <li class="gridster_element" data-row="5" data-col="3" data-sizex="1" data-sizey="1"></li>

        <li class="gridster_element" data-row="1" data-col="4" data-sizex="1" data-sizey="1"></li>
        <li class="gridster_element" data-row="2" data-col="4" data-sizex="1" data-sizey="1"></li>
        <li class="gridster_element" data-row="3" data-col="4" data-sizex="1" data-sizey="1"></li>
        <li class="gridster_element" data-row="4" data-col="4" data-sizex="1" data-sizey="1"></li>
        <li class="gridster_element" data-row="5" data-col="4" data-sizex="1" data-sizey="1"></li>
    </ul>
</div>