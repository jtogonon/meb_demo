@extends('layouts.master')

@section('content')

    <h1>Operator</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $operator->id }}</td> <td> {{ $operator->name }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection