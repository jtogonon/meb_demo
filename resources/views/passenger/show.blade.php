@extends('layouts.master')

@section('content')

    <h1>Passenger</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>First Name</th><th>Last Name</th><th>Middle Name</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $passenger->id }}</td> <td> {{ $passenger->first_name }} </td><td> {{ $passenger->last_name }} </td><td> {{ $passenger->middle_name }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection