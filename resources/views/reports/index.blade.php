@extends('layout')

@section('content')

    <div class="content animate-panel">

        <div class="hpanel">

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#report-accomodations">Hotel Manifest</a></li>
                <li class=""><a data-toggle="tab" href="#report-flights">Flight Manifest</a></li>
                <li class=""><a data-toggle="tab" href="#report-activities">Activity Manifest</a></li>
                <li class=""><a data-toggle="tab" href="#report-arrivals">Arrivals</a></li>
            </ul>

            <div class="tab-content">
                <div id="report-accomodations" class="tab-pane active">
                    <div class="panel-body">
                        <div class="row">
                            <table id="tbl_1st"
                                   class="table table-striped table-bordered table-hover dataTable no-footer"
                                   role="grid" aria-describedby="example2_info">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1"
                                        colspan="1" aria-label="Name: activate to sort column descending"
                                        aria-sort="ascending"
                                        style="width: 178px; font-weight: 500; text-align: center;">Hotel Manifest
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                        aria-label="Office: activate to sort column ascending"
                                        style="width: 131px; font-weight: 500; text-align: center;">Flight Manifest
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                        aria-label="Start date: activate to sort column ascending"
                                        style="width: 126px; font-weight: 500; text-align: center;">Group Manifest
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                        aria-label="Start date: activate to sort column ascending"
                                        style="width: 126px; font-weight: 500; text-align: center;">Activity Manifest
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                                        aria-label="Salary: activate to sort column ascending"
                                        style="width: 97px; font-weight: 500; text-align: center;">Arrivals
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr role="row" class="odd">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <div class="text-right m-t-xs">
                            <a class="btn btn-primary btn-save-booking" href="#">Generate</a>
                        </div>
                    </div>
                </div><!-- end report-accomodations -->

                <div id="report-flights" class="tab-pane">
                    <div class="panel-body">
                        flights report
                    </div>
                </div> <!-- end report-accomodations -->

                <div id="report-activities" class="tab-pane">
                    <div class="panel-body">
                        activities report
                    </div>
                </div> <!-- end report-accomodations -->

            </div><!-- end tab-content -->

        </div><!-- end panel -->
    </div>
@stop