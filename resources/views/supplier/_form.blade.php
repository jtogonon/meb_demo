<div class="panel blank-panel">
    <div class="panel-heading">
        <div class="panel-title m-b-md"><h4>Suppliers</h4></div>
        <div class="panel-options">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-1">Info </a></li>
                <li class=""><a data-toggle="tab" href="#tab-address">Address</a></li>
                @if(isset($supplier))
                    <li class=""><a data-toggle="tab" href="#tab-contact">Contact</a></li>
                @endif
            </ul>
        </div>
    </div>

    <div class="tab-content">
        <div id="tab-1" class="tab-pane active">

            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Supplier Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control ', 'placeholder' => 'Name', 'id' => 'supplier_name']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
                {!! Form::label('code', 'Supplier DBA: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('code', null, ['class' => 'form-control', 'placeholder' => 'DBA']) !!}
                    {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            {{-- @TODO Legal Name --}}

            <div class="form-group {{ $errors->has('logo') ? 'has-error' : ''}}">
                {!! Form::label('logo', 'Logo: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('logo', null, ['class' => 'form-control', 'placeholder' => 'Logo', 'id' => 'logo_input']) !!}
                    {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            {{-- @TODO Itinerary Sections --}}

            @if($show_products)
                @include('supplier._supplier_products')
            @endif

        </div><!-- end tab-1 -->

        <div id="tab-address" class="tab-pane">
            <div class="form-group {{ $errors->has('website') ? 'has-error' : ''}}">
                {!! Form::label('website', 'Website: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('website', null, ['class' => 'form-control', 'placeholder' => 'Website']) !!}
                    {!! $errors->first('website', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('tax_scheme') ? 'has-error' : ''}}">
                {!! Form::label('tax_scheme', 'Tax Scheme: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('tax_scheme', null, ['class' => 'form-control', 'placeholder' => 'Tax Scheme']) !!}
                    {!! $errors->first('tax_scheme', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('street') ? 'has-error' : ''}}">
                {!! Form::label('street', 'Street: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('street', null, ['class' => 'form-control', 'placeholder' => 'Street']) !!}
                    {!! $errors->first('street', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
                {!! Form::label('city', 'City: ', ['class' => 'col-sm-3 control-label']) !!} {{--@TODO add City array--}}
                <div class="col-sm-6">
                    {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'City']) !!}
                    {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('state') ? 'has-error' : ''}}">
                {!! Form::label('state', 'State: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('state', null, ['class' => 'form-control', 'placeholder' => 'Choose State']) !!} {{--@TODO add States array--}}
                    {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('company_description') ? 'has-error' : ''}}">
                {!! Form::label('company_description', 'Company Description: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('company_description', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('company_description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('country_id') ? 'has-error' : ''}}">
                {!! Form::label('country_id', 'Country: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('country_id', $countries, 226, ['class' => 'form-control', 'placeholder' => 'Choose Country']) !!}
                    {!! $errors->first('country_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>

        <div id="tab-contact" class="tab-pane">
            <div class="form-group">
                <label class="col-sm-2 control-label">Executive Admin</label>
            </div>

            <div class="form-group {{ $errors->has('contact_name') ? 'has-error' : ''}}">
                {!! Form::label('contact_name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('contact_name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
                {!! Form::label('phone', 'Phone: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone']) !!}
                    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('email_sales') ? 'has-error' : ''}}">
                {!! Form::label('email_sales', 'Email (sales): ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('email_sales', null, ['class' => 'form-control', 'placeholder' => 'Email Sales']) !!}
                    {!! $errors->first('email_sales', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('email_invoice') ? 'has-error' : ''}}">
                {!! Form::label('email_invoice', 'Email (invoice): ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('email_invoice', null, ['class' => 'form-control', 'placeholder' => 'Email Invoice']) !!}
                    {!! $errors->first('email_invoice', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

        </div>

        <div class="form-group">
            <label class="control-label col-sm-2"></label>

            <div class="col-sm-10">
                {!! Form::submit($submit_text, ['class' => 'btn w-xs btn-primary']) !!}
                {!! link_to_action('Supplier\SupplierController@index', $title = '[Cancel]') !!}
            </div>
        </div>
    </div><!-- end tab-content -->
</div>

@include('layouts.partials.error_list')