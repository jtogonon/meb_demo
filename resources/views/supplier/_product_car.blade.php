<div class="modal-content">
    <div class="modal-header col-lg-12 col-md-12 col-xs-12">
        <div class="pull-left padding-left-30">
            <h2 class="modal-title">New Product - Car Rental</h2>
            <small class="font-bold">Car Rental Details</small>
        </div>
        <button type="button" class="pull-right close" data-dismiss="modal">&times;</button>
    </div>
    @if(isset($product))
        {!! Form::model($product, ['url' => '/supplier/product/save', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'product_details_popup']) !!}
        {!! Form::hidden('id', $product->id) !!}
    @else
        {!! Form::open(['url' => '/supplier/product/save', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'product_details_popup']) !!}
    @endif

    <div class="modal-body" style="min-height:750px;">
        <input type="hidden" name="product_id" value="{{ $product_id }}">
        <input type="hidden" name="category_id" value="4">
        <input type="hidden" name="supplier_id" value="{{ $supplier_id }}">

        <div class="col-lg-6 border-right">
            <div class="form-group">
                {!! Form::label('vehicle_class_id', 'Vehicle Class', ['class'=>'col-sm-4 control-label']) !!}
                <div class="col-sm-8">
                    <div class="col-md-12">
                        {!! Form::select('vehicle_class_id', $vehicle_classes, null, ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                {!! Form::label('product_id', 'Product ID', ['class'=>'col-sm-4 control-label ']) !!}
                <div class="col-sm-8">
                    <div class="col-md-12">
                        {!! Form::text('acco_checkout', null, ['class'=>'form-control', 'placeholder'=>'', 'disabled']) !!}
                    </div>
                </div>
            </div>
        </div>


        <div class="clear clearfix"></div>
        <div class="col-lg-12">
            <div class="form-group">
                {!! Form::label('product_name', 'Product Name', ['class' => 'col-sm-2 control-label ']) !!}
                <div class="col-sm-10">
                    <div class="col-md-8">
                        {!! Form::text('product_name', null, ['class' => 'form-control', 'placeholder'=>'']) !!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('acc_desc', 'Description', ['class' => 'col-sm-2 control-label ']) !!}
                <div class="col-sm-10">
                    <div class="col-md-8">
                        {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'check_in', 'rows' => '3']) !!}
                    </div>
                </div>
            </div>

            <table class="table table-striped" width="50%" id="tbl_carrental_fees">
                <tr>
                    <td>Per Day</td>
                    <td>
                        <div class="col-md-4"><input type="text" class="input-sm form-control fees" name="car_per_day"
                                                     id="car_perday"
                                                     value="{{ (isset($productfees)) ? \App\ProductFee::extract_fee($productfees, 'per_day') : '0' }}">
                        </div>
                    </td>
                </tr>
                @if (count($custom_fields) > 0)
                    @foreach ($custom_fields as $field)
                        <tr>
                            <td>{{ $field->name }}</td>
                            <td>
                                <div class="col-md-4"><input type="text" class="input-sm form-control fees customfee"
                                                             name="custom_{{ $field->id }}?>"
                                                             value="{{ (isset($productfees)) ? \App\ProductFee::extract_fee($productfees, 'custom_' . $field->id) : $field->default_value }}">
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                <tr>
                    <td>Taxes</td>
                    <td>
                        <div class="col-md-4"><input type="text" class="input-sm form-control fees" name="car_tax"
                                                     id="car_tax"
                                                     value="{{ (isset($productfees)) ? \App\ProductFee::extract_fee($productfees, 'tax') : '0' }}">
                        </div>
                        <div class="col-sm-1 text-left">%</div>
                    </td>
                </tr>
                <tr>
                    <td>Total Cost</td>
                    <td>
                        <div class="col-md-4"><input type="text" class="input-sm form-control fees" name="car_total"
                                                     id="car_total"></div>
                    </td>
                </tr>
            </table>
        </div>

        <div class="col-lg-6">
            <div class="form-group">
                {!! Form::label('product_id', 'Commission', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-8">
                    <div class="col-md-8">
                        {!! Form::text('acco_checkout', '20%', ['class' => 'form-control', 'placeholder' => '']) !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="clear clearfix"></div>

        <div class="col-lg-12">
            {!! Form::label('section', 'Section ', ['class'=>'col-sm-2 control-label ']) !!}
            <div class="col-sm-10">
                <div class="col-md-8">
                    {!! Form::select('section_select', $itinerary_sections, '', ['class' => 'form-control sectionDp', 'rows' => '8']) !!}
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            {!! Form::label('product_id', 'Section Content ', ['class'=>'col-sm-2 control-label ']) !!}
            <div class="col-sm-10">
                <div class="col-md-12">
                    {!! Form::textarea('section_content', null, ['class'=>'sectionContent summernote', 'style' => 'min-height:100px;']) !!}
                </div>
            </div>
        </div>

    </div><!-- end modal-body -->

    <div class="modal-footer navbar-fixed-bottom">
        <button type="button" class="btn btn-white pull-left close" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-white pull-right">Save Product</button>
    </div>
    {!! Form::close() !!}
</div>