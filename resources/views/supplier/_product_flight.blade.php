<div class="modal-content">
    <div class="modal-header col-lg-12 col-md-12 col-xs-12">
        <div class="pull-left padding-left-30">
            @if(isset($product))
                <h2 class="modal-title">Flight - {{ $product->product_name }}</h2>
            @else
                <h2 class="modal-title">New Product - Flight</h2>
            @endif
            <small class="font-bold">Flight Details</small>
        </div>
        <button type="button" class="pull-right close" data-dismiss="modal">&times;</button>
    </div>
    @if(isset($product))
        {!! Form::model($product, ['url' => '/supplier/product/save', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'product_details_popup']) !!}
        {!! Form::hidden('id', $product->id) !!}
    @else
        {!! Form::open(['url' => '/supplier/product/save', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'product_details_popup']) !!}
    @endif

    <div class="modal-body" style="min-height:750px;">
        <input type="hidden" name="product_id" value="{{ $product_id }}">
        <input type="hidden" name="category_id" value="5">
        <input type="hidden" name="supplier_id" value="{{ $supplier_id }}">

        <div class="col-lg-6 border-right">
            <div class="form-group">
                {!! Form::label('flight_depart', 'Departing', ['class'=>'col-sm-4 control-label']) !!}
                <div class="col-sm-8">
                    <div class="col-md-12">
                        {!! Form::text('flight_departing', null, ['class'=>'form-control datepicker']) !!}
                    </div>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('flight_arrive', 'Arriving', ['class'=>'col-sm-4 control-label disabled']) !!}
                <div class="col-sm-8">
                    <div class="col-md-12">
                        {!! Form::text('flight_arriving', null, ['class'=>'form-control datepicker']) !!}
                    </div>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('country_id', 'Country', ['class'=>'col-sm-4 control-label']) !!}
                <div class="col-sm-8">
                    <div class="col-md-8">
                        {!! Form::select('country_id', $countries, null, ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>


        </div>
        <div class="col-lg-6">
            <div class="form-group">
                {!! Form::label('product_id', 'Product ID', ['class'=>'col-sm-4 control-label ']) !!}
                <div class="col-sm-8">
                    <div class="col-md-12">
                        {!! Form::text('id', null, ['class'=>'form-control', 'placeholder'=>'', 'disabled']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="clear clearfix"></div>

        <div class="col-lg-12">
            <div class="form-group">
                {!! Form::label('product_name', 'Product Name', ['class'=>'col-sm-2 control-label ']) !!}
                <div class="col-sm-10">
                    <div class="col-md-8">
                        {!! Form::text('product_name', null, ['class'=>'form-control', 'placeholder'=>'']) !!}
                    </div>
                </div>
            </div>

            <table class="table table-striped" width="50%" id="tbl_product_flight_fees">
                <tr>
                    <td>Flight</td>
                    <td>
                        <div class="col-md-4"><input type="text" class="input-sm form-control fees" id="flight_fee"
                                                     name="fee[flight]"
                                                     value="{{ (isset($productfees)) ? \App\ProductFee::extract_fee($productfees, 'flight') : '0' }}">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Taxes</td>
                    <td>
                        <div class="col-md-4"><input type="text" class="input-sm form-control fees" id="flight_tax"
                                                     name="fee[tax]"
                                                     value="{{ (isset($productfees)) ? \App\ProductFee::extract_fee($productfees, 'tax') : '0' }}">
                        </div>
                        <div class="col-sm-1 text-left">%</div>
                    </td>
                </tr>
                @if (count($custom_fields) > 0)
                    @foreach ($custom_fields as $field)
                        <tr>
                            <td>{{ $field->name }}</td>
                            <td>
                                <div class="col-md-4"><input type="text" class="input-sm form-control fees customfee"
                                                             name="custom_{{ $field->id }}"
                                                             value="{{ (isset($productfees)) ? \App\ProductFee::extract_fee($productfees, 'custom_' . $field->id) : $field->default_value }}">
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                <tr>
                    <td>Total Flight</td>
                    <td>
                        <div class="col-md-4"><input type="text" class="input-sm form-control fees" id="flight_total"
                                                     name="fee[total]"
                                                     value="{{ (isset($productfees)) ? \App\ProductFee::extract_fee($productfees, 'total') : '0' }}">
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <div class="col-lg-6">
            <div class="form-group">
                {!! Form::label('product_id', 'Commission %',['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-8">
                    <div class="col-md-6">
                        {!! Form::text('commision', old('commision'), ['class' => 'form-control','placeholder' => '']) !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="clear clearfix"></div>

        <div class="col-lg-12">
            {!! Form::label('section', 'Section ', ['class'=>'col-sm-2 control-label ']) !!}
            <div class="col-sm-10">
                <div class="col-md-8">
                    {!! Form::select('section_select', $itinerary_sections, '', ['class' => 'form-control sectionDp', 'rows' => '8']) !!}
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            {!! Form::label('product_id', 'Section Content ', ['class' => 'col-sm-2 control-label ']) !!}
            <div class="col-sm-10">
                <div class="col-md-12">
                    {!! Form::textarea('section_content', null, ['class' => 'sectionContent summernote', 'style' => 'min-height:100px;']) !!}
                </div>
            </div>
        </div>

    </div><!-- end modal-body -->

    <div class="modal-footer navbar-fixed-bottom">
        <button type="button" class="btn btn-white pull-left close" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-white pull-right">Save Product</button>
    </div>
    {!! Form::close() !!}
</div>