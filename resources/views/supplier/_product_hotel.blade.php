<div class="modal-content">
    <div class="modal-header col-lg-12 col-md-12 col-xs-12">
        <div class="pull-left padding-left-30">
            <h2 class="modal-title">New Product - Hotel Accomodation</h2>
            <small class="font-bold">Hotel Accomodation Details</small>
        </div>
        <button type="button" class="pull-right close" data-dismiss="modal">&times;</button>
    </div>
    @if(isset($product))
        {!! Form::model($product, ['url' => '/supplier/product/save', 'method' => 'post', 'class'=>'form-horizontal', 'id' => 'product_details_popup']) !!}
        {!! Form::hidden('id', $product->id) !!}
    @else
        {!! Form::open(['url' => '/supplier/product/save', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'product_details_popup']) !!}
    @endif

    <div class="modal-body" style="min-height:1100px;">
        <input type="hidden" name="product_id" value="{{ $product_id }}">
        <input type="hidden" name="category_id" value="1">
        <input type="hidden" name="supplier_id" value="{{ $supplier_id }}">

        <div class="col-lg-6 border-right">

            <div class="form-group">
                {!! Form::label('room_category_id', 'Room Category',['class'=>'col-sm-4 control-label']) !!}
                <div class="col-sm-8">
                    <div class="col-md-8">
                        {!! Form::select('room_category_id', ['Elegant', 'Luxury', 'Super Luxury'], old('room_category_id'), ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('product_code', 'Product Code',['class'=>'col-sm-4 control-label']) !!}
                <div class="col-sm-8">
                    <div class="col-md-8">
                        {!! Form::text('product_code',old('product_code'), ['class'=>'form-control','id'=>'product_code']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                {!! Form::label('product_id', 'Product ID', ['class'=>'col-sm-4 control-label ']) !!}
                <div class="col-sm-8">
                    <div class="col-md-12">
                        {!! Form::text('id', old('id'), ['class'=>'form-control', 'placeholder'=>'', 'disabled']) !!}
                    </div>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('country_id', 'Country', ['class'=>'col-sm-4 control-label']) !!}
                <div class="col-sm-8">
                    <div class="col-md-8">
                        {!! Form::select('country_id', $countries, old('country_id'), ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>

        </div>

        <div class="clear clearfix"></div>

        <div class="col-lg-12">
            <div class="form-group">
                {!! Form::label('acc_product_name', 'Product Name',['class'=>'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <div class="col-md-8">
                        {!! Form::text('product_name', old('product_name'), ['class'=>'form-control', 'id'=>'product_name']) !!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('acc_desc', 'Description', ['class'=>'col-sm-2 control-label ']) !!}
                <div class="col-sm-10">
                    <div class="col-md-8">
                        {!! Form::textarea('description', old('description'), ['class'=>'form-control', 'id'=>'check_in', 'rows'=>'3']) !!}
                    </div>
                </div>
            </div>

            <table class="table table-striped" width="50%" id="tbl_product_acc_fees">
                <tr>
                    <td>Per Night</td>
                    <td>
                        <div class="col-md-4"><input type="text" id="acc_per_night" class="input-sm form-control fees"
                                                     name="fee[per_night]"
                                                     value="{{ (isset($productfees)) ? \App\ProductFee::extract_fee($productfees, 'per_night') : '0' }}">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Resort Fee</td>
                    <td>
                        <div class="col-md-4"><input type="text" id="acc_resort" class="input-sm form-control fees"
                                                     name="fee[resort_fee]"
                                                     value="{{ (isset($productfees)) ? \App\ProductFee::extract_fee($productfees, 'resort_fee') : '0' }}">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Room Tax</td>
                    <td>
                        <div class="col-md-4"><input type="text" id="acc_room" class="input-sm form-control fees"
                                                     name="fee[room_tax]"
                                                     value="{{ (isset($productfees)) ? \App\ProductFee::extract_fee($productfees, 'room_tax') : '0' }}">
                        </div>
                        <div class="col-sm-1 text-left">%</div>
                    </td>
                </tr>
                @if (count($custom_fields) > 0)
                    @foreach ($custom_fields as $field)
                        <tr>
                            <td>{{ $field->name }}</td>
                            <td>
                                <div class="col-md-4"><input type="text" class="input-sm form-control fees customfee"
                                                             name="custom_{{ $field->id }}"
                                                             value="{{ (isset($productfees)) ? \App\ProductFee::extract_fee($productfees, 'custom_' . $field->id) : $field->default_value }}">
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                <tr>
                    <td>Other Charges</td>
                    <td>
                        <div class="col-md-4"><input type="text" id="acc_other_charge"
                                                     class="input-sm form-control fees" name="fee[other_charge]"
                                                     value="{{ (isset($productfees)) ? \App\ProductFee::extract_fee($productfees, 'other_charge') : '0' }}">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Total Per Night</td>
                    <td>
                        <div class="col-md-4"><input type="text" id="acc_totalfees" class="input-sm form-control fees"
                                                     name="fee[total]"
                                                     value="{{ (isset($productfees)) ? \App\ProductFee::extract_fee($productfees, 'total') : '0' }}">
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <div class="col-lg-6">
            <div class="form-group">
                {!! Form::label('product_id', 'Commission',['class'=>'col-sm-4 control-label']) !!}
                <div class="col-sm-8">
                    <div class="col-md-8">
                        {!! Form::text('commision', old('commision'), ['class'=>'form-control','placeholder'=>'']) !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="clear clearfix"></div>

        <div class="col-lg-12">
            {!! Form::label('section', 'Section ',['class'=>'col-sm-2 control-label ']) !!}
            <div class="col-sm-10">
                <div class="col-md-8">
                    {!! Form::select('section_select', $itinerary_sections, '', ['class'=>'form-control sectionDp','rows'=>'8']) !!}
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            {!! Form::label('product_id', 'Section Content ',['class'=>'col-sm-2 control-label ']) !!}
            <div class="col-sm-10">
                <div class="col-md-12">
                    {!! Form::textarea('section_content', null, ['class' => 'sectionContent summernote', 'style' => 'min-height:100px;']) !!}
                </div>
            </div>
        </div>

    </div><!-- end modal-body -->

    <div class="modal-footer navbar-fixed-bottom">
        <button type="button" class="btn btn-white pull-left close" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-white pull-right">Save Product</button>
    </div>
    {!! Form::close() !!}
</div>