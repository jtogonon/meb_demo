<div class="modal-content">
    <div class="modal-header col-lg-12 col-md-12 col-xs-12">
        <div class="pull-left padding-left-30">
            <h2 class="modal-title">New Product - Vacation</h2>
            <small class="font-bold">Vacation Details</small>
        </div>
        <button type="button" class="pull-right close" data-dismiss="modal">&times;</button>
    </div>
    @if(isset($product))
        {!! Form::model($product, ['url' => '/supplier/product/save', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'product_details_popup']) !!}
        {!! Form::hidden('id', $product->id) !!}
    @else
        {!! Form::open(['url' => '/supplier/product/save', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'product_details_popup']) !!}
    @endif

    <div class="modal-body" style="min-height:550px;">

        <input type="hidden" name="product_id" value="{{ $product_id }}">
        <input type="hidden" name="category_id" value="2">
        <input type="hidden" name="supplier_id" value="{{ $supplier_id }}">

        <div class="col-lg-6 border-right">
            <div class="form-group">
                {!! Form::label('act_name', 'Package Name', ['class'=>'col-sm-4 control-label']) !!}
                <div class="col-sm-8">
                    <div class="col-md-12">
                        {!! Form::text('acco_hotel', null, ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                {!! Form::label('product_id', 'Product ID', ['class'=>'col-sm-4 control-label ']) !!}
                <div class="col-sm-8">
                    <div class="col-md-12">
                        {!! Form::text('acco_checkout', null, ['class'=>'form-control', 'placeholder'=>'', 'disabled']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>

        <br/><br/>
        <a class="btn btn-outline btn-primary btn-category btm-sm" href="" cat_id="1">Hotel</a>
        <a class="btn btn-outline btn-primary2 btn-category btm-sm" href="" cat_id="5">Flights</a>
        <a class="btn btn-outline btn-info btn-category btm-sm" href="" cat_id="4">Cars</a>
        <a class="btn btn-outline btn-success btn-category btm-sm" href="" cat_id="3">Activities</a>
        <a class="btn btn-outline btn-danger btn-category btm-sm" href="" cat_id="6">Cruises</a>


        <div class="clear"></div>
        <br/>
        <table class="table table-striped table-bordered" id="tbl_products">
            <thead>
            <tr role="row">
                <th>Supplier</th>
                <th>Country</th>
                <th>Product & Services</th>
                <th>Price</th>
                <th>Product Type</th>
                <th>Promotion</th>

            </thead>

            <tbody>

            <tr role="row" class="odd">
                <td>
                    <div class="col-md-12">{!! Form::text('acco_hotel', null, ['class' => 'form-control']) !!}</div>
                </td>
                <td>
                    <div class="col-md-12">{!! Form::text('acco_hotel', null, ['class' => 'form-control']) !!}</div>
                </td>
                <td>
                    <div class="col-md-12">{!! Form::text('acco_hotel', null, ['class' => 'form-control']) !!}</div>
                </td>
                <td>
                    <div class="col-md-12">{!! Form::text('acco_hotel', null, ['class' => 'form-control']) !!}</div>
                </td>
                <td>
                    <div class="col-md-12">{!! Form::text('acco_hotel', null, ['class' => 'form-control']) !!}</div>
                </td>
                <td>
                    <div class="col-md-12">{!! Form::text('acco_hotel', null, ['class' => 'form-control']) !!}</div>
                </td>
            </tr>

            </tbody>
        </table>

        <br/>
        Booking List
        <div class="clear"></div>
        <div id="supplier_products_wrapper" style="overflow: scroll;">
            <table class="table table-striped" style="width: 200%;max-width: 200%;">
                <thead>
                <tr role="row">
                    <th>Date</th>
                    <th>Category</th>
                    <th>Supplier</th>
                    <th>Country</th>
                    <th>Product & Services</th>
                    <th>Location1</th>
                    <th>Location2</th>
                    <th>Duration</th>
                    <th>Per per Unit</th>
                    <th>Total Price</th>
                    <th>Confirmed</th>
                </tr>
                </thead>
                <tbody>
                <tr role="row" class="odd">
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                    <td>6</td>
                    <td>7</td>
                    <td>8</td>
                    <td>9</td>
                    <td>10</td>
                    <td>11</td>
                </tr>
                </tbody>
            </table>
        </div>

    </div><!-- end modal-body -->

    <div class="modal-footer navbar-fixed-bottom">
        <button type="button" class="btn btn-white pull-left close" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-white pull-right">Save Product</button>
    </div>
    {!! Form::close() !!}
</div>