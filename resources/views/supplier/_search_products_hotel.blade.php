<div id="search_products_wrapper">
    <div class="pull-right" style="padding-right:10px; margin-bottom:20px;"><a
                href="{{ url('/supplier/product/create/1/' . $supplier_id) }}" class="btn_new_product btn btn-primary btn-sm">New
            Product</a></div>
    <table class="table table-striped table-bordered" id="tbl_products">
        <thead>
        <tr role="row">
            <th width="20%">Product Id</th>
            <th>Country</th>
            <th>Product Name</th>
            <th>Price</th>
            <th>Promotion</th>
            <th>Action</th>
        </thead>
        <tbody>
        @foreach ($products as $product)
            <tr role="row" class="odd">
                <td>{{ 'P' . $product->id }}</td>
                <td>{{ $product->country->name }}</td>
                <td><a href="" class="item_details" data="{{ $product->id }}:1">{{ $product->name }}</a></td>
                <td>{{ round($product->getPrice(), 2) }}</td>
                <td>x</td>
                <td><a href="{{ url('/products/delete/' . $product->id) }}" class="btn btn-default btndelete">Delete</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>