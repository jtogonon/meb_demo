<link rel="stylesheet" href="{{ url('/assets/fontawesome/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ url('/assets/metisMenu/dist/metisMenu.css') }}">
<link rel="stylesheet" href="{{ url('/assets/animate.css/animate.css') }}">
<link rel="stylesheet" href="{{ url('/assets/bootstrap/dist/css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ url('/assets/sweetalert/lib/sweet-alert.css') }}">