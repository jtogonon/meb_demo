<hr/>
<div class="clear"></div>
Products
<br/><br/>
<a class="btn btn-outline btn-primary btn-category btm-sm"
   href="" category_id="1" supplier_id="{{ $supplier->id }}">Hotels</a>

<a class="btn btn-outline btn-primary2 btn-category btm-sm"
   href="" category_id="5" supplier_id="{{ $supplier->id }}">Flights</a>

<a class="btn btn-outline btn-info btn-category btm-sm"
   href="" category_id="4" supplier_id="{{ $supplier->id }}">Cars</a>

<a class="btn btn-outline btn-success btn-category btm-sm"
   href="" category_id="3" supplier_id="{{ $supplier->id }}">Activities</a>

<a class="btn btn-outline btn-danger btn-category btm-sm"
   href="" category_id="6" supplier_id="{{ $supplier->id }}">Cruises</a>

<a class="btn btn-outline btn-success btn-category btm-sm"
   href="" category_id="2" supplier_id="{{ $supplier->id }}">Vacation Packages</a>

<div id="supplier_products_wrapper">
    <table class="table table-striped">
        @unless(empty($products))
            <thead>
            <tr role="row">
                <th>Product ID</th>
                <th>Product Name</th>
                <th>Product Code</th>
                <th>Country</th>
            </thead>
            @foreach ($products as $product)
                <tr role="row" class="odd">
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->code }}</td>
                    <td>{{ $product->country->name }}</td>
                    <td></td>
                </tr>
            @endforeach
        @endunless
    </table>
</div>