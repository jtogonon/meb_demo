@extends('layout2')

@include('supplier._styles')

@section('content')
    <div class="content animate-panel">
        <div class="hpanel">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    <a class="closebox"><i class="fa fa-times"></i></a>
                </div>
                Add New Supplier
            </div>
            <div class="panel-body">
                {!! Form::open(['url' => 'supplier', 'method' => 'post', 'class' => 'form-horizontal']) !!}
                @include('supplier._form', ['submit_text' => 'Create', 'show_products' => false])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@include('supplier._scripts')

@section('footerJS')
    <script src="{{ url('/scripts/suppliers.js') }}"></script>
@stop