@extends('layout2')

@section('page_title')
    Suppliers
@stop

@include('supplier._styles')

@section('headerCSS')
    <link rel="stylesheet" href="{{ url('/assets/select2-3.5.2/select2.css') }}">
    <link rel="stylesheet"
          href="{{ url('/assets/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/toastr/build/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/summernote/dist/summernote.css') }}">
@stop

@section('content')
    <div class="content animate-panel">
        <div class="hpanel">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    <a class="closebox"><i class="fa fa-times"></i></a>
                </div>
                Supplier Details
            </div>
            <div class="panel-body">
                {!! Form::model($supplier, [ 'url' => ['supplier', $supplier->id], 'method' => 'PATCH', 'class' => 'form-horizontal' ]) !!}

                @include('supplier._form', [
                    'submit_text' => 'Update',
                    'supplier_id' => $supplier->id,
                    'show_products' => true
                ])
                {!! Form::close() !!}
            </div>

            <div class="modal fade hmodal-info" id="modal_details" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@include('supplier._scripts')

@section('footerJS')
    <script src="{{ url('/assets/select2-3.5.2/select2.min.js') }}"></script>
    <script src="{{ url('/assets/steps/jquery.steps.min.js') }}"></script>
    <script src="{{ url('/assets/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ url('/assets/datatables/jquery.DataTable.1.10.9.min.js') }}"></script>
    <script src="{{ url('/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ url('/assets/toastr/build/toastr.min.js') }}"></script>
    <script src="{{ url('/scripts/suppliers.js') }}"></script>
    <script src="{{ url('/assets/summernote/dist/summernote.min.js') }}"></script>
    <script>
        $('#tbl_suppliers').dataTable();
    </script>
@stop