@extends('layout2')

@section('page_title')
    Supplier
@stop

@include('supplier._styles')

@section('headerCSS')
    <link rel="stylesheet"
          href="{{ url('/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}">
@stop

@section('top_panel')
    @include('elements.top_panel')
@stop

@section('content')
    <div class="content animate-panel">
        <div class="row">
            <table id="tbl_suppliers" class="table table-striped table-bordered table-hover dataTable no-footer"
                   role="grid" aria-describedby="example2_info">
                <thead>
                <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                        aria-label="Name: activate to sort column descending" aria-sort="ascending"
                        style="width: 178px;">Supplier Name
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                        aria-label="Office: activate to sort column ascending" style="width: 131px;">Email
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                        aria-label="Start date: activate to sort column ascending" style="width: 126px;">Contact Person
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                        aria-label="Start date: activate to sort column ascending" style="width: 126px;">Website
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                        aria-label="Salary: activate to sort column ascending" style="width: 97px;">Actions
                    </th>
                </tr>
                </thead>
                <tbody>
                @if($product_type)
                    {{ $product_type }}
                @endif

                @foreach ($suppliers as $supplier)
                    <tr role="row" class="odd">
                        <td><a href="{{ url('/supplier/' . $supplier->id  . '/edit')}}">{{ $supplier->name }}</a></td>
                        <td>{{ $supplier->email_sales }}</td>
                        <td>{{ $supplier->contact_name }}</td>
                        <td><a href="{{ $supplier->website }}" target="_blank">{{ $supplier->website }}</a></td>
                        <td><a href="/bookings/new/{{ $supplier->id }}" title="New Booking"><i
                                        class="fa fa-book"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@include('supplier._scripts')

@section('footerJS')
    <script src="{{ url('/assets/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('/assets/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $('#tbl_suppliers').dataTable();
    </script>
@stop