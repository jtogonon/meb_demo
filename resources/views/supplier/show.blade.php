@extends('layout2')

@section('content')

    <h1>Supplier</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID</th> <th>Name</th><th>Code</th><th>Website</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $supplier->id }}</td> <td> {{ $supplier->name }} </td><td> {{ $supplier->code }} </td><td> {{ $supplier->website }} </td>
                </tr>
            </tbody>
        </table>
    </div>

@endsection