
<div class="modal-content">
    <div class="modal-header col-lg-12 col-md-12 col-xs-12">
        <div class="pull-left ">
            <h2 class="modal-title padding-left-30">
                @if(isset($customer))
                    {{ $customer->first_name . ' ' . $customer->last_name }}
                @else
                    New Customer
                @endif
            </h2>
            <small class="font-bold padding-left-30">Bookings</small>
        </div>
        <button type="button" class="pull-right close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body bookings_table_wrapper">
        @include('booking._latest_bookings_search_tab', $bookings)
    </div>

    <div class="modal-footer navbar-fixed-bottom">
        <button type="button" class="btn btn-white pull-left close" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-white pull-right" id="btn_save_customer_details">Save</button>
    </div>
</div>
{!! Form::close() !!}

