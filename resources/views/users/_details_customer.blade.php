@if(isset($customer))
    {!! Form::model($customer, ['url' => '', 'method' => 'post', 'class'=>'form-horizontal', 'id'=>'form_customer_details']) !!}
    {!! Form::hidden('user_id', $customer->id) !!}
@else
    {!! Form::model(new App\User, ['url' => '', 'method' => 'post', 'class'=>'form-horizontal', 'id'=>'form_customer_details']) !!}
@endif

<div class="modal-content">
    <div class="modal-header col-lg-12 col-md-12 col-xs-12">
        <div class="pull-left ">    {{--<div class="col-lg-11 ">--}}
            <h2 class="modal-title padding-left-30">
                @if(isset($customer))
                    {{ $customer->first_name . ' ' . $customer->last_name }}
                @else
                    New Customer
                @endif
            </h2>
            <small class="font-bold padding-left-30">Customer Details</small>
        </div>
        <button type="button" class="pull-right close" data-dismiss="modal">&times;</button>

        {{--</div>--}}
        {{--<div class="col-lg-1">--}}
        {{--</div>--}}
    </div>
    <div class="modal-body">
        <div class="col-lg-12 padding-bottom-20 padding-top-20">
            <div class="col-lg-6">
                <div class="form-group">
                    {!! Form::label('title', 'Title',['class'=>'col-sm-3 control-label disabled']) !!}
                    <div class="col-sm-9">
                        <div class="col-md-12">
                            {!! Form::select('title', ['Mr', 'Mrs', 'Miss'], ['class'=>'form-control select2 dropdown']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('first_name', 'First Name', ['class'=>'col-sm-3 control-label disabled']) !!}
                    <div class="col-sm-9">
                        <div class="col-md-12">
                            {!! Form::text('first_name', old('first_name'), ['class'=>'form-control input-sm']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('last_name', 'Last Name', ['class'=>'col-sm-3 control-label disabled']) !!}
                    <div class="col-sm-9">
                        <div class="col-md-12">
                            {!! Form::text('last_name', old('last_name'), ['class'=>'form-control input-sm']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('dob', 'DOB',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-9">
                        <div class="col-md-12">
                            {!! Form::text('dob', old('dateofbirth'), ['class'=>'form-control datepicker input-sm', 'id'=>'dob']) !!}
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-6 ">
                <div class="form-group">
                    {!! Form::label('user_id', 'Customer ID', ['class'=>'col-sm-3 control-label disabled']) !!}
                    <div class="col-sm-9">
                        <div class="col-md-12">
                            {!! Form::text('id', old('id'), ['class'=>'form-control input-sm', 'disabled']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('gender', 'Gender', ['class'=>'col-sm-3 control-label disabled']) !!}
                    <div class="col-sm-9">
                        <div class="col-md-12">
                            {!! Form::select('gender', ['Male'=>'Male', 'Female'=>'Female'], old('gender'), ['class'=>'form-control input-sm']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('nationality', 'Nationality', ['class'=>'col-sm-3 control-label disabled']) !!}
                    <div class="col-sm-9">
                        <div class="col-md-12">
                            {!! Form::select('nationality', $nationalities, old('nationality'), ['class'=>'form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end col-lg-12 -->


        @if(!is_null($customer->address)) <!-- this check is for development/ might be removed -->
        <div class="col-lg-12 border-top padding-bottom-20 padding-top-20">
            <h3>Address:</h3>
            <div class="col-lg-6 ">
                <div class="form-group">
                    {!! Form::label('address', 'Address', ['class'=>'col-sm-3 control-label disabled']) !!}
                    <div class="col-sm-9">
                        <div class="col-md-12">
                            @if(isset($customer))
                                {!! Form::text('address', $customer->address->street, ['class'=>'form-control input-sm']) !!}
                            @else
                                {!! Form::text('address', '', ['class'=>'form-control input-sm']) !!}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('city', 'City/Town',['class'=>'col-sm-3 control-label disabled']) !!}
                    <div class="col-sm-9">
                        <div class="col-md-12">
                            @if(isset($customer))
                                {!! Form::text('city', $customer->address->city, ['class'=>'form-control input-sm']) !!}
                            @else
                                {!! Form::text('city', '' , ['class'=>'form-control input-sm']) !!}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('state', 'State',['class'=>'col-sm-3 control-label disabled']) !!}
                    <div class="col-sm-9">
                        <div class="col-md-12">
                            @if(isset($customer))
                                @if ($show_states_default)
                                    {!! Form::text('state2', $customer->state, ['class'=>'form-control input-sm hidden', 'id'=>'state2']) !!}
                                    {!! Form::select('state', $us_states, $customer->address->state, ['class'=>'form-control', 'style'=>'width:240px;', 'id'=>'state']) !!}
                                @else
                                    {!! Form::text('state2', $customer->state, ['class'=>'form-control input-sm ', 'id'=>'state2']) !!}
                                    {!! Form::select('state', $us_states, $customer->getState(), ['class'=>'form-control hidden', 'style'=>'width:240px;', 'id'=>'state']) !!}
                                @endif
                            @else
                                {!! Form::text('state2', '', ['class'=>'form-control input-sm hidden', 'id'=>'state2']) !!}
                                {!! Form::select('state', $us_states, '', ['class'=>'form-control', 'style'=>'width:240px;', 'id'=>'state']) !!}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('zip', 'Zip Code',['class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-9">
                        <div class="col-md-12">
                            @if(isset($customer))
                                {!! Form::text('zip', $customer->address->postal, ['class'=>'form-control input-sm']) !!}
                            @else
                                {!! Form::text('zip', '', ['class'=>'form-control input-sm']) !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 ">
                <div class="form-group">
                    {!! Form::label('country', 'Country', ['class'=>'col-sm-3 control-label ']) !!}
                    <div class="col-sm-9">
                        <div class="col-md-12">
                            @if(isset($customer))
                                {!! Form::select('country_id', $countries, $customer->getCountryId(), ['id'=>'country_id', 'class'=>'form-control']) !!}
                            @else
                                {!! Form::select('country_id', $countries, '226', ['id'=>'country_id', 'class'=>'form-control']) !!}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('phone_home', 'Home Phone', ['class'=>'col-sm-3 control-label ']) !!}
                    <div class="col-sm-9">
                        <div class="col-md-12">
                            {!! Form::text('phone_home', old('phone_home'), ['class'=>'form-control input-sm']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('phone_mobile', 'Cell Phone', ['class'=>'col-sm-3 control-label disabled']) !!}
                    <div class="col-sm-9">
                        <div class="col-md-12">
                            {!! Form::text('phone_mobile', old('phone_mobile'), ['class'=>'form-control input-sm']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Email', ['class'=>'col-sm-3 control-label disabled']) !!}
                    <div class="col-sm-9">
                        <div class="col-md-12">
                            {!! Form::text('email', old('email'), ['class'=>'form-control input-sm']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif <!-- for development/ might be removed -->
        <div class="clear clearfix"></div>

        <div class="col-lg-12 border-top padding-bottom-20 padding-top-20">
            <h3>Additional Info:</h3>
            <div class="col-lg-6">
                @if (count($custom_fields) > 0 )
                    @foreach ($custom_fields as $field)
                        <div class="form-group">
                            {!! Form::label($field['name'], $field['label'],['class'=>'col-sm-3 control-label input-sm']) !!}
                            <div class="col-sm-9">
                                <div class="col-md-12">
                                    {!! Form::text($field['name'], $field['value'], ['class'=>'form-control input-sm',]) !!}
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    {!! Form::label('notes', 'Notes', ['class'=>'col-sm-3 control-label disabled']) !!}
                    <div class="col-sm-9">
                        <div class="col-md-12">
                            {!! Form::textarea('notes', old('notes'), ['class'=>'form-control', 'rows'=>4]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer navbar-fixed-bottom">
        <button type="button" class="btn btn-white pull-left close" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-white pull-right" id="btn_save_customer_details">Save</button>
    </div>
</div>
{!! Form::close() !!}

<script>
    $(document).ready(function () {
        /*todo, check on ajax reload*/
        $("#dob").datepicker();

        $('#country_id').change(function () {

            if ($(this).val() != '226') {
                $("#state").addClass('hidden');
                $('#state2').removeClass('hidden');
            }
            if ($(this).val() == '226') {
                $("#state2").addClass('hidden');
                $('#state').removeClass('hidden');
            }
        });

        $("#form_customer_details").submit(function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name=_token]').val()
                }
            });

            $.ajax({
                        url: '/customers/savedetails/',
                        dataType: 'JSON',
                        data: $('form#form_customer_details').serialize(),
                        success: function (response) {

                        }
                    })
                    .done(function (resp) {
                        swal({
                            title: "Saved!",
                            text: "Successfully saved customer info.",
                            type: "success"
                        });
                        var curUrl = window.location.href;
                        var urlRegExp = new RegExp('customers/index');
                        if (urlRegExp.test(curUrl)) {
                            window.location.href = '/customers/index';
                        }
                    });
        });
    });
    /*end on ready*/
</script>