<!-- /resources/views/projects/partials/_form.blade.php -->

<div class="form-group">
    {!! Form::label('name', 'Name:', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <div class="col-md-2">
            {!! Form::text('title', '', ['class'=>'form-control ', 'placeholder'=>'Title']) !!}
        </div>
        <div class="col-md-4">
            {!! Form::text('first_name', '', ['class'=>'form-control ', 'placeholder'=>'First Name']) !!}
        </div>
        <div class="col-md-4">
            {!! Form::text('last_name', '', ['class'=>'form-control ', 'placeholder'=>'Last Name']) !!}
        </div>

    </div>
</div>

<div class="form-group">
    {!! Form::label('address', 'Street Address:', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
        <div class="col-md-10">
            {!! Form::text('street', '', ['class'=>'form-control ', 'placeholder'=>'']) !!}
        </div>
    </div>


</div>
<div class="form-group">
    {!! Form::label('city', 'City:', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <div class="col-md-4">
            {!! Form::text('city', '', ['class'=>'form-control ', 'placeholder'=>'']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('state', 'State:', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <div class="col-md-4">
            {!! Form::text('state', '', ['class'=>'form-control ', 'placeholder'=>'']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('country_id', 'Country:', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <div class="col-md-4">
            {!! Form::select('country_id', $countries, 226, ['class'=>'form-control m-b']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('email', 'Email:', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <div class="col-md-4">
            {!! Form::text('email', '', ['class'=>'form-control ', 'placeholder'=>'']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('telephon', 'Telephone:', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <div class="col-md-4">
            {!! Form::text('tel_home', '', ['class'=>'form-control ', 'placeholder'=>'Home / Evening']) !!}
        </div>
        <div class="col-md-4">
            {!! Form::text('tel_work', '', ['class'=>'form-control ', 'placeholder'=>'First Name']) !!}
        </div>

    </div>
</div>
<div class="form-group">
    <label class="control-label col-sm-2"></label>
    <div class="col-sm-10">
        <div class="col-md-4">
            {!! Form::text('tel_mobile', '', ['class'=>'form-control ', 'placeholder'=>'Mobile']) !!}
        </div>
        <div class="col-md-4">
            {!! Form::text('tel_sms', '', ['class'=>'form-control ', 'placeholder'=>'SMS (Text message)']) !!}
        </div>

    </div>
</div>

<div class="form-group">
    {!! Form::label('travel_agent', 'Travel Agent:', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <div class="col-md-4">
            {!! Form::select('agent_id', $agents, 226, ['class'=>'form-control m-b']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-sm-2"></label>
    <div class="col-sm-10">
        {!! Form::submit($submit_text, ['class'=>'btn w-xs btn-primary']) !!}
        {!! link_to_action('UsersController@index', $title = '[Cancel]') !!}
    </div>
</div>