<div class="col-lg-1"></div>
<div class="col-lg-10" style="margin-top: 20px;">
    <div class="hpanel hblue">
        <div class="panel-heading hbuilt">
            <div class="panel-tools">
                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
            </div>
            New User
        </div>
        <div class="panel-body">
            {!! Form::open(['url' => 'users/store', 'method' => 'post']) !!}
            <div class="row">
                <div class="form-group">
                    <label for="first_name" class="col-sm-2 control-label">First name</label>
                    <div class="col-sm-4">
                        {!! Form::text('first_name', old('first_name'), ['id'=>'first_name', 'class'=>'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="form-group">
                    <label for="middle_name" class="col-sm-2 control-label">Middle name</label>
                    <div class="col-sm-4">
                        {!! Form::text('middle_name', old('last_name'), ['id'=>'first_name', 'class'=>'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="form-group">
                    <label for="last_name" class="col-sm-2 control-label">Last name</label>
                    <div class="col-sm-4">
                        {!! Form::text('last_name', old('last_name'), ['id'=>'first_name', 'class'=>'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="form-group">
                    <label for="suffix" class="col-sm-2 control-label">Suffix</label>
                    <div class="col-sm-4">
                        {!! Form::text('suffix', old('suffix'), ['id'=>'first_name', 'class'=>'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-4">
                        {!! Form::text('email', old('email'), ['id'=>'first_name', 'class'=>'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-4">
                        {!! Form::password('password', ['id'=>'first_name', 'class'=>'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="form-group">
                    <label for="gender" class="col-sm-2 control-label">Gender</label>
                    <div class="col-sm-4">
                        {!! Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], '', ['class'=>'form-control', 'style'=>'width:180px;']) !!}
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="form-group">
                    <label for="dateofbirth" class="col-sm-2 control-label">Date of birth</label>
                    <div class="col-sm-4">
                        {!! Form::text('dateofbirth', old('password'), ['id'=>'dateofbirth', 'class'=>'form-control datepicker']) !!}
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="form-group">
                    <label for="nationality" class="col-sm-2 control-label">Nationality</label>
                    <div class="col-sm-4">
                        {!! Form::select('nationality', $nationality, '', ['class'=>'form-control', 'style'=>'width:180px;']) !!}
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="form-group">
                    <label for="phone_home" class="col-sm-2 control-label">Phone number</label>
                    <div class="col-sm-4">
                        {!! Form::text('phone_home', old('phone_home'), ['id'=>'phone_home','class'=>'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="form-group">
                    <label for="phone_mobile" class="col-sm-2 control-label">Mobile Number</label>
                    <div class="col-sm-4">
                        {!! Form::text('phone_mobile', old('phone_mobile'), ['id'=>'phone_mobile','class'=>'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="form-group">
                    <label for="notes" class="col-sm-2 control-label">Notes</label>
                    <div class="col-sm-4">
                        {!! Form::text('notes', old('notes'), ['id'=>'phone_mobile', 'class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
            {!! Form::submit('Save', ['class' => 'btn btn-primary btn-save-booking']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="col-lg-1"></div>