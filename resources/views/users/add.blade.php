@extends('layout')

@section('content')

    <div class="content animate-panel">
        <div class="hpanel">
            <div class="panel-heading">
                <div class="panel-tools">
                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    <a class="closebox"><i class="fa fa-times"></i></a>
                </div>
                Add New Customer
            </div>
            <div class="panel-body">
                {!! Form::model(new App\User, ['url' => 'users/save', 'method' => 'post', 'class'=>'form-horizontal']) !!}
                @include('users/_form', ['submit_text' => 'Save User'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop