@extends('layout')

@section('content')
    <div class="content animate-panel">
        <div class="row" style="background-color: #fff">

            <div class="row" style="margin-top: 10px;">
                <div class="col-lg-1"></div>
                <div class="col-lg-10">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <span>Whoops! There's an error in the form.</span>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if(Session::has('success_flash'))
                        <div class="alert alert-success">{{Session::get('success_flash')}}</div>
                    @endif
                </div>
                <div class="col-lg-1"></div>
            </div>
            <div class="row">
                @include('users/_form_create_user')
            </div>
        </div>
    </div>
@stop

@section('footerJS')
    <script>
        // Initialize customers table
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });

        setTimeout(function(){
            $('.alert-success').fadeOut(5000);
        });
    </script>
@stop