@extends('layout')

@section('headerCSS')
    <link rel="stylesheet" href="{{ url('/assets/select2-3.5.2/select2.css') }}">
    <style>
        .select-all-checkbox-th {
            width: 15px !important;
            padding-left: 20px !important;
            padding-right: 17px !important;
        }
    </style>
@stop

@section('content')
    <div class="content animate-panel">
        <div class="row">
            <table id="tbl_customers" class="table table-striped table-bordered table-hover dataTable no-footer">
                <thead>
                <tr role="row">
                    {{--<th class="no-sort" style="width:13px; padding-right:5px; padding-left:20px;"><input type="checkbox" id="check-all" name="user_id[]" ></th>--}}
                    <th class="no-sort select-all-checkbox-th"><input name="select_all" value="1" id="select-all"
                                                                      type="checkbox"/></th>
                    <th>Customer ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($lead_travelers as $lead_traveler)
                    <tr role="row" class="odd tbl_row" data="{{ $lead_traveler->id }}">
                        <td style="width:13px; padding-right:5px; padding-left:20px;"><input type="checkbox"
                                                                                             name="user_id[]"></td>
                        <td> {{ $lead_traveler->id }}</td>
                        <td>{{ $lead_traveler->first_name }}</td>
                        <td>{{ $lead_traveler->last_name }}</td>
                        <td>
                            <div class="col-md-8">
                                {!! Form::select('action',
                                ['/customers/details/' . $lead_traveler->id=>'Details', '/customers/bookings/'. $lead_traveler->id => 'Booking'],
                                '',
                                ['class' => 'action select2'])
                                 !!}
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <div class="modal fade hmodal-info" id="modal_details" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                </div>
            </div>
        </div>
        <div class="modal fade hmodal-info" id="modal_bookings" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                </div>
            </div>
        </div>
    </div>
@stop
