@extends('layout')

@section('content')

    <div class="content animate-panel">
        <div class="row">
            <table id="tbl_users" class="table table-striped table-bordered table-hover dataTable no-footer" role="grid"
                   aria-describedby="example2_info">
                <thead>
                <tr role="row">
                    <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1"
                        aria-label="Name: activate to sort column descending" aria-sort="ascending"
                        style="width: 178px;">Name
                    </th>
                    <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1"
                        aria-label="Email: activate to sort column descending" aria-sort="ascending"
                        style="width: 178px;">Email Address
                    </th>
                    <th class="sorting" tabindex="0" rowspan="1" colspan="1"
                        aria-label="Active: activate to sort column ascending" style="width: 131px;">Active
                    </th>
                    <th class="sorting" tabindex="0" rowspan="1" colspan="1"
                        aria-label="Role: activate to sort column ascending" style="width: 126px;">Role
                    </th>
                    <th class="sorting" tabindex="0" rowspan="1" colspan="1"
                        aria-label="Date Created: activate to sort column ascending" style="width: 97px;">Date Created
                    </th>
                    <th class="sorting" tabindex="0" rowspan="1" colspan="1"
                        aria-label="Logged In: activate to sort column ascending" style="width: 97px;">Logged In
                    </th>
                    <th class="sorting" tabindex="0" rowspan="1" colspan="1"
                        aria-label="Last Logged In: activate to sort column ascending" style="width: 97px;">Last Logged
                        In
                    </th>
                    <th class="sorting" tabindex="0" rowspan="1" colspan="1"
                        aria-label="Duration: activate to sort column ascending" style="width: 97px;">Duration
                    </th>
                    <th tabindex="0" rowspan="1" colspan="1" style="width: 97px;">Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($users as $user)
                    <tr role="row" class="odd" id="{{$user->id}}">
                        <td>{{$user->first_name}} {{$user->id}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            {{
                                $lastLoggedIn = \Carbon\Carbon::createFromFormat('Y-m-d G:i:s', $user->last_loggedin);
                                $lastLoggedOut = \Carbon\Carbon::createFromFormat('Y-m-d G:i:s', $user->last_loggedout);
                                $now = \Carbon\Carbon::now();
                            }}

                            @if ($user->last_loggedin == '0000-00-00 00:00:00')
                                {{ 'No' }}
                            @else
                                {{ $lastLoggedIn->diffInDays($lastLoggedOut) <= 10 ? 'Yes' : 'No' }}
                            @endif
                        </td>
                        <td>{{$user->role_name}}</td>
                        <td>{{ date('M d, Y', strtotime($user->created_at)) }}</td>
                        <td>
                            @if ($user->last_loggedin == '0000-00-00 00:00:00')
                                {{ 'No' }}
                            @else
                                {{ $lastLoggedIn->diffInHours($now) <= 1 ? 'Yes' : 'No' }}
                            @endif
                        </td>
                        <td>{{ date('M d, Y', strtotime($user->last_loggedin)) }}</td>
                        <td>
                            {{ gmdate("H:i:s", $lastLoggedIn->diffInSeconds($lastLoggedOut)) }}
                        </td>
                        <td>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <a href="javascript:void(0)" id="deleteUser-{{$user->id}}"
                               class="btn btn-danger btn-xs deleteUser">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('footerJS')
    <script>
        // Initialize bookings table
        $('#tbl_users').dataTable();

        $('.deleteUser').click(function(){
            var confirmDel = confirm('Delete this record?');
            if(confirmDel) {
                var id = $(this).prop('id').split('-')[1];
                var token = $('input[name=_token]').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('input[name=_token]').val()
                    }
                });

                $.ajax({
                    url: '/user/delete/'+id,
                    type: 'DELETE',
                    dataType: 'JSON',
                    success: function(resp) {
                        var title = 'Deleted';
                        var text = 'Record successfully deleted.';
                        var type = 'success';
                        if(resp.success == 0) {
                            title = 'Error!';
                            text = 'No record to delete.';
                            type = 'error';
                        }

                        swal({
                            title: title,
                            text: text,
                            type: type
                        });

                        $('#tbl_users').DataTable().row('#'+resp.id).remove().draw( false );
                    }
                });
            }
        });
    </script>
@stop